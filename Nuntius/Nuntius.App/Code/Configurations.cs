﻿using Nuntius.Data;
using Nuntius.Globals;
using Nuntius.Messenger;
using Nuntius.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nuntius.App.Code
{
    public static class Configurations
    {
        /// <summary>
        /// This method should only be called for first load or after updating configurations. This will update all Configurations.
        /// </summary>
        public async static void LoadConfigurations()
        {
            var preferredBroadcaster = await ConfigurationsService.Instance.GetConfiguration((int)ConfigurationEnums.PrefferedBroadcaster);
            _PrefferredBroadcaster = preferredBroadcaster != null ? int.Parse(preferredBroadcaster.Value) : Defaults.PrefferredBroadcaster;

            var singlePageRecordCount = await ConfigurationsService.Instance.GetConfiguration((int)ConfigurationEnums.SinglePageRecordsCount);
            _SinglePageRecordsCount = singlePageRecordCount != null ? int.Parse(singlePageRecordCount.Value) : Defaults.SinglePageRecordsCount;

            //var gsmComPort = await ConfigurationsService.Instance.GetConfiguration((int)ConfigurationEnums.GSMComPort);
            //_GSMComPortConfig = gsmComPort != null ? int.Parse(gsmComPort.Value) : 0;

            var gsmTimeOut = await ConfigurationsService.Instance.GetConfiguration((int)ConfigurationEnums.GSMTimeoutConfig);
            _GSMTimeoutConfig = gsmTimeOut != null ? int.Parse(gsmTimeOut.Value) : Defaults.GSMTimeout;

            var gsmBaudRate = await ConfigurationsService.Instance.GetConfiguration((int)ConfigurationEnums.GSMBaudRateConfig);
            _GSMBaudRateConfig = gsmTimeOut != null ? int.Parse(gsmBaudRate.Value) : Defaults.GSMBaudRate;

            var twilioAccountSid = await ConfigurationsService.Instance.GetConfiguration((int)ConfigurationEnums.TwilioAccountSid);
            _TwilioAccountSid = twilioAccountSid != null ? twilioAccountSid.Value : Defaults.TwilioAccountSid;

            var twilioAuthToken = await ConfigurationsService.Instance.GetConfiguration((int)ConfigurationEnums.TwilioAuthToken);
            _TwilioAuthToken = twilioAuthToken != null ? twilioAuthToken.Value : Defaults.TwilioAuthToken;

            var twilioFromNumber = await ConfigurationsService.Instance.GetConfiguration((int)ConfigurationEnums.TwilioFromNumber);
            _TwilioFromNumber = twilioFromNumber != null ? twilioFromNumber.Value : Defaults.TwilioFromNumber;

            var smsTime = await ConfigurationsService.Instance.GetConfiguration((int)ConfigurationEnums.SMSTimeConfig);
            _SMSTime = smsTime != null ? int.Parse(smsTime.Value) : Defaults.SMSTime;

            var smsMessageCountConfig = await ConfigurationsService.Instance.GetConfiguration((int)ConfigurationEnums.SMSMessageCountConfig);
            _SMSMessageCount = smsMessageCountConfig != null ? int.Parse(smsMessageCountConfig.Value) : Defaults.SMSMessageCount;

            var aboutDetails = await ConfigurationsService.Instance.GetConfiguration((int)ConfigurationEnums.AboutDetails);
            _AboutDetails = aboutDetails != null ? aboutDetails.Value : string.Empty;
            
            var messageSignature = await ConfigurationsService.Instance.GetConfiguration((int)ConfigurationEnums.MessageSignature);
            _MessageSignature = messageSignature != null ? messageSignature.Value : string.Empty;

            var includeMessageSignature = await ConfigurationsService.Instance.GetConfiguration((int)ConfigurationEnums.IncludeMessageSignature);
            _IncludeMessageSignature = includeMessageSignature != null ? bool.Parse(includeMessageSignature.Value) : false;

            var contactEmail = await ConfigurationsService.Instance.GetConfiguration((int)ConfigurationEnums.ContactEmail);
            _ContactEmail = contactEmail != null ? contactEmail.Value : string.Empty;

            var contactPhone = await ConfigurationsService.Instance.GetConfiguration((int)ConfigurationEnums.ContactPhone);
            _ContactPhone = contactPhone != null ? contactPhone.Value : string.Empty;

            var contactDetails = await ConfigurationsService.Instance.GetConfiguration((int)ConfigurationEnums.ContactDetails);
            _ContactDetails = contactDetails != null ? contactDetails.Value : string.Empty;

            //reset these broadcasters when configurations are updated
            Postman.PreferredBroadcaster = (BroadcasterEnums)PrefferredBroadcaster;
            GSMMessenger.Setup(GSMComPortConfig, GSMBaudRateConfig, GSMTimeoutConfig);
            TwilioMessenger.Setup(TwilioAccountSid, TwilioAuthToken, TwilioFromNumber);
        }

        public static int SingleMessageCharactersCount { get { return 160; } }

        public static int PrefferredBroadcaster
        {
            get
            {
                return _PrefferredBroadcaster;
            }
        }
        private static int _PrefferredBroadcaster { get; set; }
        
        public static int SinglePageRecordsCount
        {
            get
            {
                return _SinglePageRecordsCount;
            }
        }
        private static int _SinglePageRecordsCount { get; set; }

        public static string AboutDetails
        {
            get
            {
                return _AboutDetails;
            }
        }
        private static string _AboutDetails { get; set; }

        public static bool IncludeMessageSignature
        {
            get
            {
                return _IncludeMessageSignature;
            }
        }
        private static bool _IncludeMessageSignature { get; set; }

        public static string MessageSignature
        {
            get
            {
                return _MessageSignature;
            }
        }
        private static string _MessageSignature { get; set; }

        public static string ContactEmail
        {
            get
            {
                return _ContactEmail;
            }
        }
        private static string _ContactEmail { get; set; }

        public static string ContactPhone
        {
            get
            {
                return _ContactPhone;
            }
        }
        private static string _ContactPhone { get; set; }

        public static string ContactDetails
        {
            get
            {
                return _ContactDetails;
            }
        }
        private static string _ContactDetails { get; set; }

        //This can be taken from Hardware so I think we dont need to add as config
        /// <summary>
        /// The Port at which GSM Modem is connected. 
        /// </summary>
        public static int GSMComPortConfig
        {
            get
            {
                if (_GSMComPortConfig == 0)
                {
                    var gsmComPort = SystemHelper.GetActiveComPort();

                    if (gsmComPort <= 0) return 0;
                    else _GSMComPortConfig = gsmComPort;
                }

                return _GSMComPortConfig;
            }
        }
        private static int _GSMComPortConfig { get; set; }

        public static void UpdateComPort(int comPort)
        {
            _GSMComPortConfig = comPort;

            GSMMessenger.Setup(GSMComPortConfig, GSMBaudRateConfig, GSMTimeoutConfig);
        }

        /// <summary>
        /// The Timeout is the time, in seconds, that a tcp/ip operation (connect/send/recv) will retry before failing the operation.
        /// </summary>
        public static int GSMTimeoutConfig
        {
            get
            {
                return _GSMTimeoutConfig;
            }
        }
        private static int _GSMTimeoutConfig { get; set; }
       
        /// <summary>
        /// The baud rate is the time, in seconds at which information is transferred through Port.
        /// More: https://www.mathworks.com/help/matlab/matlab_external/baudrate.html
        /// </summary>
        public static int GSMBaudRateConfig
        {
            get
            {
                return _GSMBaudRateConfig;
            }
        }
        private static int _GSMBaudRateConfig { get; set; }
        
        /// <summary>
        /// An Account SID from twilio.com/console. 
        /// </summary>
        public static string TwilioAccountSid
        {
            get
            {
                return _TwilioAccountSid;
            }
        }
        private static string _TwilioAccountSid { get; set; }
        
        /// <summary>
        /// Authentication token for Twilio project.
        /// </summary>
        public static string TwilioAuthToken
        {
            get
            {
                return _TwilioAuthToken;
            }
        }
        private static string _TwilioAuthToken { get; set; }
        
        /// <summary>
        /// Twilio assigned number to send message from
        /// </summary>
        public static string TwilioFromNumber
        {
            get
            {
                return _TwilioFromNumber;
            }
        }
        private static string _TwilioFromNumber { get; set; }

        /// <summary>
        /// Will return true if twilio configurations has value
        /// </summary>
        public static bool IsTwilioSetupped
        {
            get
            {
                return (!string.IsNullOrEmpty(_TwilioAccountSid.Trim()) && !string.IsNullOrEmpty(_TwilioAuthToken.Trim()) && !string.IsNullOrEmpty(_TwilioFromNumber.Trim()));
            }
        }

        public static int SMSTime
        {
            get
            {
                return _SMSTime;
            }
        }
        private static int _SMSTime { get; set; }
        public static int SMSTimeInMiliSeconds
        {
            get
            {
                return SMSTime * 60 * 1000;
            }
        }

        public static int SMSMessageCount
        {
            get
            {
                return _SMSMessageCount;
            }
        }
        private static int _SMSMessageCount { get; set; }
        
        public static string TwilioAccountSidURL
        {
            get
            {
                return "Twilio_AccountSid_URL";
            }
        }

        public static string TwilioAuthTokenURL
        {
            get
            {
                return "Twilio_AuthToken_URL";
            }
        }

        public static string TwilioFromNumberURL
        {
            get
            {
                return "Twilio_FromNumber_URL";
            }
        }

        public static string GSMBaudRateURL
        {
            get
            {
                return "Twilio_AccountSid_URL";
            }
        }

        public static string GSMComPortURL
        {
            get
            {
                return "Twilio_AuthToken_URL";
            }
        }

        public static string GSMTimeoutURL
        {
            get
            {
                return "Twilio_FromNumber_URL";
            }
        }

        public static string WebsiteURL
        {
            get
            {
                return "http://www.sajjadgul.com/nuntius";
            }
        }

        public static string EmailURL
        {
            get
            {
                return "sajjadarifgul@gmail.com";
            }
        }
        
        public static string CustomFAQURL
        {
            get
            {
                return "http://www.sajjadgul.com/nuntius/faqs";
            }
        }
    }
}
