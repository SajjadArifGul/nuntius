﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management;
using Nuntius.Globals;
using System.ServiceProcess;

namespace Nuntius.App.Code
{
    public static class SystemHelper
    {
        public static int GetActiveComPort()
        {
            int PortNumber = -1;
            using (var searcher = new ManagementObjectSearcher("SELECT * FROM Win32_PnPEntity WHERE Caption like '%(COM%'"))
            {
                var HuaweiPorts = searcher.Get().Cast<ManagementBaseObject>().ToList().Select(p => p["Caption"].ToString()).Where(p => p.Contains("HUAWEI Mobile Connect - 3G PC UI Interface")).Select(n => n.GetComPortNumber());

                if (HuaweiPorts != null)
                {
                    PortNumber = HuaweiPorts.FirstOrDefault();
                }
            }

            return PortNumber;
        }
        
        static ServiceController service;
        public static void ProcessMessengerBGS()
        {
            var serviceName = "MessengerBGS";
            
            if(service == null)
                service = new ServiceController(serviceName);

            try
            {
                if (!service.Status.Equals(ServiceControllerStatus.Running))
                {
                    service.Start();
                    service.WaitForStatus(ServiceControllerStatus.Running);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static void StopMessengerBGS()
        {
            var serviceName = "MessengerBGS";

            ServiceController service = new ServiceController(serviceName);

            try
            {                
                service.Stop();

                service.WaitForStatus(ServiceControllerStatus.Stopped);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
