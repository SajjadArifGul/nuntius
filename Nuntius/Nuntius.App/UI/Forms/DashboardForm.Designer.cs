﻿namespace Nuntius.App.UI.Forms
{
    partial class DashboardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LogoTitlePanel = new System.Windows.Forms.Panel();
            this.lblLogo = new System.Windows.Forms.Label();
            this.LeftMenuFlowPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.btnContacts = new System.Windows.Forms.Button();
            this.btnGroups = new System.Windows.Forms.Button();
            this.btnBroadcasts = new System.Windows.Forms.Button();
            this.btnSettings = new System.Windows.Forms.Button();
            this.btnAbout = new System.Windows.Forms.Button();
            this.btnHelp = new System.Windows.Forms.Button();
            this.ControlsPanel = new System.Windows.Forms.Panel();
            this.pbLoading = new System.Windows.Forms.PictureBox();
            this.LogoTitlePanel.SuspendLayout();
            this.LeftMenuFlowPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLoading)).BeginInit();
            this.SuspendLayout();
            // 
            // LogoTitlePanel
            // 
            this.LogoTitlePanel.BackColor = System.Drawing.Color.Transparent;
            this.LogoTitlePanel.Controls.Add(this.lblLogo);
            this.LogoTitlePanel.Location = new System.Drawing.Point(0, 0);
            this.LogoTitlePanel.Margin = new System.Windows.Forms.Padding(0);
            this.LogoTitlePanel.Name = "LogoTitlePanel";
            this.LogoTitlePanel.Size = new System.Drawing.Size(199, 80);
            this.LogoTitlePanel.TabIndex = 5;
            // 
            // lblLogo
            // 
            this.lblLogo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLogo.BackColor = System.Drawing.Color.Transparent;
            this.lblLogo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblLogo.Font = new System.Drawing.Font("Bahnschrift", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLogo.Image = global::Nuntius.App.Properties.Resources.nuntius_logo_white_76;
            this.lblLogo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblLogo.Location = new System.Drawing.Point(3, 8);
            this.lblLogo.Margin = new System.Windows.Forms.Padding(3);
            this.lblLogo.Name = "lblLogo";
            this.lblLogo.Size = new System.Drawing.Size(193, 65);
            this.lblLogo.TabIndex = 0;
            this.lblLogo.Text = "Nuntius";
            this.lblLogo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LeftMenuFlowPanel
            // 
            this.LeftMenuFlowPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.LeftMenuFlowPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.LeftMenuFlowPanel.Controls.Add(this.LogoTitlePanel);
            this.LeftMenuFlowPanel.Controls.Add(this.btnContacts);
            this.LeftMenuFlowPanel.Controls.Add(this.btnGroups);
            this.LeftMenuFlowPanel.Controls.Add(this.btnBroadcasts);
            this.LeftMenuFlowPanel.Controls.Add(this.btnSettings);
            this.LeftMenuFlowPanel.Controls.Add(this.btnHelp);
            this.LeftMenuFlowPanel.Controls.Add(this.btnAbout);
            this.LeftMenuFlowPanel.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LeftMenuFlowPanel.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.LeftMenuFlowPanel.Location = new System.Drawing.Point(0, 0);
            this.LeftMenuFlowPanel.Margin = new System.Windows.Forms.Padding(0);
            this.LeftMenuFlowPanel.Name = "LeftMenuFlowPanel";
            this.LeftMenuFlowPanel.Size = new System.Drawing.Size(200, 562);
            this.LeftMenuFlowPanel.TabIndex = 1;
            // 
            // btnContacts
            // 
            this.btnContacts.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnContacts.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.btnContacts.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnContacts.FlatAppearance.BorderSize = 0;
            this.btnContacts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnContacts.Font = new System.Drawing.Font("Bahnschrift SemiBold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnContacts.Image = global::Nuntius.App.Properties.Resources.contacts_white_45;
            this.btnContacts.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnContacts.Location = new System.Drawing.Point(0, 81);
            this.btnContacts.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.btnContacts.Name = "btnContacts";
            this.btnContacts.Padding = new System.Windows.Forms.Padding(1);
            this.btnContacts.Size = new System.Drawing.Size(199, 40);
            this.btnContacts.TabIndex = 0;
            this.btnContacts.Text = "Contacts";
            this.btnContacts.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnContacts.UseVisualStyleBackColor = false;
            this.btnContacts.Click += new System.EventHandler(this.btnContacts_Click);
            // 
            // btnGroups
            // 
            this.btnGroups.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGroups.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.btnGroups.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGroups.FlatAppearance.BorderSize = 0;
            this.btnGroups.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGroups.Font = new System.Drawing.Font("Bahnschrift SemiBold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGroups.Image = global::Nuntius.App.Properties.Resources.groups_white_45;
            this.btnGroups.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGroups.Location = new System.Drawing.Point(0, 123);
            this.btnGroups.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.btnGroups.Name = "btnGroups";
            this.btnGroups.Padding = new System.Windows.Forms.Padding(1);
            this.btnGroups.Size = new System.Drawing.Size(199, 40);
            this.btnGroups.TabIndex = 1;
            this.btnGroups.Text = "Groups";
            this.btnGroups.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGroups.UseVisualStyleBackColor = false;
            this.btnGroups.Click += new System.EventHandler(this.btnGroups_Click);
            // 
            // btnBroadcasts
            // 
            this.btnBroadcasts.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBroadcasts.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.btnBroadcasts.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBroadcasts.FlatAppearance.BorderSize = 0;
            this.btnBroadcasts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBroadcasts.Font = new System.Drawing.Font("Bahnschrift SemiBold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBroadcasts.Image = global::Nuntius.App.Properties.Resources.broadcasts_white_45;
            this.btnBroadcasts.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBroadcasts.Location = new System.Drawing.Point(0, 165);
            this.btnBroadcasts.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.btnBroadcasts.Name = "btnBroadcasts";
            this.btnBroadcasts.Padding = new System.Windows.Forms.Padding(1);
            this.btnBroadcasts.Size = new System.Drawing.Size(199, 40);
            this.btnBroadcasts.TabIndex = 2;
            this.btnBroadcasts.Text = "Broadcasts";
            this.btnBroadcasts.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBroadcasts.UseVisualStyleBackColor = false;
            this.btnBroadcasts.Click += new System.EventHandler(this.btnBroadcasts_Click);
            // 
            // btnSettings
            // 
            this.btnSettings.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSettings.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.btnSettings.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSettings.FlatAppearance.BorderSize = 0;
            this.btnSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSettings.Font = new System.Drawing.Font("Bahnschrift SemiBold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSettings.Image = global::Nuntius.App.Properties.Resources.settings_white_45;
            this.btnSettings.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSettings.Location = new System.Drawing.Point(0, 207);
            this.btnSettings.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Padding = new System.Windows.Forms.Padding(1);
            this.btnSettings.Size = new System.Drawing.Size(199, 40);
            this.btnSettings.TabIndex = 3;
            this.btnSettings.Text = "Settings";
            this.btnSettings.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSettings.UseVisualStyleBackColor = false;
            this.btnSettings.Click += new System.EventHandler(this.btnSettings_Click);
            // 
            // btnAbout
            // 
            this.btnAbout.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAbout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.btnAbout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAbout.FlatAppearance.BorderSize = 0;
            this.btnAbout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAbout.Font = new System.Drawing.Font("Bahnschrift SemiBold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAbout.Image = global::Nuntius.App.Properties.Resources.info_white_45;
            this.btnAbout.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAbout.Location = new System.Drawing.Point(0, 291);
            this.btnAbout.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.btnAbout.Name = "btnAbout";
            this.btnAbout.Padding = new System.Windows.Forms.Padding(1);
            this.btnAbout.Size = new System.Drawing.Size(199, 40);
            this.btnAbout.TabIndex = 4;
            this.btnAbout.Text = "About";
            this.btnAbout.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAbout.UseVisualStyleBackColor = false;
            this.btnAbout.Click += new System.EventHandler(this.btnAbout_Click);
            // 
            // btnHelp
            // 
            this.btnHelp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnHelp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.btnHelp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnHelp.FlatAppearance.BorderSize = 0;
            this.btnHelp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHelp.Font = new System.Drawing.Font("Bahnschrift SemiBold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHelp.Image = global::Nuntius.App.Properties.Resources.help_white_45;
            this.btnHelp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHelp.Location = new System.Drawing.Point(0, 249);
            this.btnHelp.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Padding = new System.Windows.Forms.Padding(1);
            this.btnHelp.Size = new System.Drawing.Size(199, 40);
            this.btnHelp.TabIndex = 5;
            this.btnHelp.Text = "Help";
            this.btnHelp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnHelp.UseVisualStyleBackColor = false;
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // ControlsPanel
            // 
            this.ControlsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ControlsPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(155)))), ((int)(((byte)(229)))));
            this.ControlsPanel.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.ControlsPanel.Location = new System.Drawing.Point(200, 0);
            this.ControlsPanel.Margin = new System.Windows.Forms.Padding(0);
            this.ControlsPanel.Name = "ControlsPanel";
            this.ControlsPanel.Size = new System.Drawing.Size(772, 618);
            this.ControlsPanel.TabIndex = 2;
            // 
            // pbLoading
            // 
            this.pbLoading.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pbLoading.Image = global::Nuntius.App.Properties.Resources.circle_loading_animation;
            this.pbLoading.Location = new System.Drawing.Point(8, 565);
            this.pbLoading.Name = "pbLoading";
            this.pbLoading.Size = new System.Drawing.Size(54, 50);
            this.pbLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbLoading.TabIndex = 0;
            this.pbLoading.TabStop = false;
            this.pbLoading.Visible = false;
            // 
            // DashboardForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(972, 618);
            this.Controls.Add(this.ControlsPanel);
            this.Controls.Add(this.LeftMenuFlowPanel);
            this.Controls.Add(this.pbLoading);
            this.Name = "DashboardForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dashboard";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.DashboardForm_FormClosed);
            this.Load += new System.EventHandler(this.DashboardForm_Load);
            this.LogoTitlePanel.ResumeLayout(false);
            this.LeftMenuFlowPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbLoading)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel LogoTitlePanel;
        private System.Windows.Forms.FlowLayoutPanel LeftMenuFlowPanel;
        private System.Windows.Forms.Label lblLogo;
        private System.Windows.Forms.Panel ControlsPanel;
        private System.Windows.Forms.Button btnContacts;
        private System.Windows.Forms.Button btnGroups;
        private System.Windows.Forms.Button btnBroadcasts;
        private System.Windows.Forms.Button btnSettings;
        private System.Windows.Forms.Button btnAbout;
        private System.Windows.Forms.Button btnHelp;
        private System.Windows.Forms.PictureBox pbLoading;
    }
}