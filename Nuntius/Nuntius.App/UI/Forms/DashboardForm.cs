﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nuntius.App.UI.UserControls.Contacts;
using Nuntius.App.UI.UserControls.Groups;
using Nuntius.App.UI.UserControls.Broadcasts;
using Nuntius.App.UI.UserControls.Settings;
using Nuntius.App.UI.UserControls.About;
using Nuntius.App.UI.UserControls.Help;
using Nuntius.App.UI.UserControls.BaseControls;

namespace Nuntius.App.UI.Forms
{
    public partial class DashboardForm : Form
    {
        #region Define as Singleton
        private static DashboardForm _Instance;

        public static DashboardForm Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new DashboardForm();
                }

                return (_Instance);
            }
        }

        private DashboardForm()
        {
            InitializeComponent();
        }
        #endregion
        
        private void DashboardForm_Load(object sender, EventArgs e)
        {
            ContactsMainUserControl.Instance.Load();
        }

        #region LTH Menu
        private void btnContacts_Click(object sender, EventArgs e)
        {
            ContactsMainUserControl.Instance.Load();
        }

        private void btnGroups_Click(object sender, EventArgs e)
        {
            GroupsMainUserControl.Instance.Load();
        }

        private void btnBroadcasts_Click(object sender, EventArgs e)
        {
            BroadcastsMainUserControl.Instance.Load();
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            SettingsMainUserControl.Instance.Load();
        }

        private void btnAbout_Click(object sender, EventArgs e)
        {
            AboutMainUserControl.Instance.Load();
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            HelpMainUserControl.Instance.Load();
        }

        #endregion

        #region Controls and Message Displays

        /// <summary>
        /// Pass a Nuntius Control to this and It will be displayed on Dashboard
        /// </summary>
        /// <param name="control">Nuntius Control</param>
        public void ShowControl(NuntiusControl control)
        {
            if (!ControlsPanel.Controls.Contains(control))
            {
                ControlsPanel.Controls.Add(control);
                control.Dock = DockStyle.Fill;
                control.BringToFront();

                foreach (var existingControl in ControlsPanel.Controls.OfType<UserControl>().Where(x => x != control))
                {
                    ControlsPanel.Controls.Remove(existingControl);
                }
            }
            else control.BringToFront();

            control.Focus();            
        }

        /// <summary>
        /// Displays Sucess Message
        /// </summary>
        /// <param name="Message">Message to be displayed</param>
        public void ShowSuccessMessage(string Message)
        {
            MessageBox.Show(Message, "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// Display Information Message
        /// </summary>
        /// <param name="Message">Message to be displayed</param>
        public void ShowInformationMessage(string Message)
        {
            MessageBox.Show(Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        }

        /// <summary>
        /// Display Warning Message
        /// </summary>
        /// <param name="Message">Message to be displayed</param>
        public void ShowWarningMessage(string Message)
        {
            MessageBox.Show(Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        /// <summary>
        /// Display Error Message
        /// </summary>
        /// <param name="Message">Message to be displayed</param>
        public void ShowErrorMessage(string Message)
        {
            MessageBox.Show(Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        /// <summary>
        /// Ask User Confirmation in a Message Box
        /// </summary>
        /// <param name="Message">Message to be confirmed</param>
        /// <returns>Dialog Result with either OK or Cancel</returns>
        public DialogResult ShowConfirmationMessage(string Message)
        {
            return MessageBox.Show(Message, "Confirmation Message", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
        }
        
        #endregion

        public void DisplayLoader(bool? Show = true)
        {
            if (Show.HasValue && Show.Value)
            {
                pbLoading.Show();
            }
            else if (Show.HasValue && !Show.Value)
            {
                pbLoading.Hide();
            }
            else if (!Show.HasValue)
            {
                pbLoading.Show();
            }
        }
        
        public void MenuNavigation(bool? Enabled = true)
        {
            LeftMenuFlowPanel.Enabled = Enabled.Value;
        }

        private void DashboardForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
