﻿using Nuntius.App.Code;
using Nuntius.Data;
using Nuntius.Globals;
using Nuntius.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nuntius.App.UI.Forms
{
    public partial class SplashForm : Form
    {
        public SplashForm()
        {
            InitializeComponent();
        }

        private async void SplashForm_Load(object sender, EventArgs e)
        {
            try
            {
                lblStatus.Text = Messages.NuntiusLoading;

                pbLoading.Visible = true;

                var version = await ConfigurationsService.Instance.GetConfiguration((int)ConfigurationEnums.NuntiusVersion);
                if (version != null)
                {
                    lblStatus.Text = string.Format("Nuntius V{0}", version.Value);                    
                }

                //to seed initial data. //this should ideally be done by ef in db initilization
                //SeederService.Instance.Seed();

                //Load All Configurations in Memory
                Configurations.LoadConfigurations();

                pbLoading.Hide();

                this.Hide();
            }
            catch (Exception ex)
            {
                var dialogResult = MessageBox.Show(string.Format(Messages.ErrorOccurredMessage, ex.StackTrace, ex.Message), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                if (dialogResult == DialogResult.OK) Application.Exit();
            }

            DashboardForm.Instance.ShowDialog();
        }
    }
}
