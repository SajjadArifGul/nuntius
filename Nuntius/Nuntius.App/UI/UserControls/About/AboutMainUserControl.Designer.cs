﻿namespace Nuntius.App.UI.UserControls.About
{
    partial class AboutMainUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutMainUserControl));
            this.lblAboutDetails = new System.Windows.Forms.Label();
            this.lblDescriptionLogo = new System.Windows.Forms.Label();
            this.lblLogo = new System.Windows.Forms.Label();
            this.lblControlHeader = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblAboutDetails
            // 
            this.lblAboutDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblAboutDetails.Font = new System.Drawing.Font("Bahnschrift", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAboutDetails.Location = new System.Drawing.Point(55, 37);
            this.lblAboutDetails.Name = "lblAboutDetails";
            this.lblAboutDetails.Size = new System.Drawing.Size(681, 431);
            this.lblAboutDetails.TabIndex = 28;
            this.lblAboutDetails.Text = resources.GetString("lblAboutDetails.Text");
            // 
            // lblDescriptionLogo
            // 
            this.lblDescriptionLogo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDescriptionLogo.BackColor = System.Drawing.Color.Transparent;
            this.lblDescriptionLogo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblDescriptionLogo.Font = new System.Drawing.Font("Bahnschrift Light", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescriptionLogo.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblDescriptionLogo.Location = new System.Drawing.Point(213, 513);
            this.lblDescriptionLogo.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.lblDescriptionLogo.Name = "lblDescriptionLogo";
            this.lblDescriptionLogo.Size = new System.Drawing.Size(452, 65);
            this.lblDescriptionLogo.TabIndex = 30;
            this.lblDescriptionLogo.Text = "Message Broadcaster";
            this.lblDescriptionLogo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLogo
            // 
            this.lblLogo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblLogo.BackColor = System.Drawing.Color.Transparent;
            this.lblLogo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblLogo.Font = new System.Drawing.Font("Bahnschrift", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLogo.Image = global::Nuntius.App.Properties.Resources.nuntius_logo_white_76;
            this.lblLogo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblLogo.Location = new System.Drawing.Point(54, 512);
            this.lblLogo.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.lblLogo.Name = "lblLogo";
            this.lblLogo.Size = new System.Drawing.Size(159, 65);
            this.lblLogo.TabIndex = 29;
            this.lblLogo.Text = "Nuntius";
            this.lblLogo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblControlHeader
            // 
            this.lblControlHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblControlHeader.BackColor = System.Drawing.Color.Transparent;
            this.lblControlHeader.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblControlHeader.Font = new System.Drawing.Font("Bahnschrift SemiBold", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblControlHeader.Image = global::Nuntius.App.Properties.Resources.info_white_45;
            this.lblControlHeader.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblControlHeader.Location = new System.Drawing.Point(0, 0);
            this.lblControlHeader.Margin = new System.Windows.Forms.Padding(0);
            this.lblControlHeader.Name = "lblControlHeader";
            this.lblControlHeader.Size = new System.Drawing.Size(791, 37);
            this.lblControlHeader.TabIndex = 6;
            this.lblControlHeader.Text = "About";
            this.lblControlHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = global::Nuntius.App.Properties.Resources.nuntius_qr_code;
            this.pictureBox1.Location = new System.Drawing.Point(615, 448);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(150, 150);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 31;
            this.pictureBox1.TabStop = false;
            // 
            // AboutMainUserControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(155)))), ((int)(((byte)(229)))));
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblDescriptionLogo);
            this.Controls.Add(this.lblLogo);
            this.Controls.Add(this.lblAboutDetails);
            this.Controls.Add(this.lblControlHeader);
            this.Font = new System.Drawing.Font("Bahnschrift", 9F);
            this.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.Name = "AboutMainUserControl";
            this.Size = new System.Drawing.Size(791, 623);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblControlHeader;
        private System.Windows.Forms.Label lblAboutDetails;
        private System.Windows.Forms.Label lblLogo;
        private System.Windows.Forms.Label lblDescriptionLogo;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}
