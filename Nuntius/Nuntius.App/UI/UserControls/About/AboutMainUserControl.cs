﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nuntius.App.UI.Forms;
using Nuntius.App.Code;
using Nuntius.App.UI.UserControls.BaseControls;
using System.Diagnostics;

namespace Nuntius.App.UI.UserControls.About
{
    public partial class AboutMainUserControl : NuntiusControl
    {
        #region Define as Singleton
        private static AboutMainUserControl _Instance;

        public static AboutMainUserControl Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new AboutMainUserControl();
                }

                return (_Instance);
            }
        }

        private AboutMainUserControl()
        {
            InitializeComponent();
        }
        #endregion        

        public new void Load()
        {
            base.Load();

            DashboardForm.Instance.DisplayLoader();
            this.Enabled = false;

            //lblAboutDetails.Text = Configurations.AboutDetails.ToString();

            this.Enabled = true;
            DashboardForm.Instance.DisplayLoader(false);
        }

        private void lblWebsiteURL_Click(object sender, EventArgs e)
        {
            Process.Start(Configurations.WebsiteURL);
        }

        private void lblEmailURL_Click(object sender, EventArgs e)
        {
            Process.Start(Configurations.EmailURL);
        }
    }
}
