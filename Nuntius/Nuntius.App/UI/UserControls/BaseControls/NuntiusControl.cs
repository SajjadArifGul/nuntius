﻿using Nuntius.App.UI.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nuntius.App.UI.UserControls.BaseControls
{
    public class NuntiusControl : UserControl
    {
        public new void Load()
        {
            DashboardForm.Instance.ShowControl(this);
        }
    }
}
