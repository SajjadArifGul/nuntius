﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nuntius.Entities;
using Nuntius.Data;
using Nuntius.App.UI.Forms;
using Nuntius.Globals;
using Nuntius.App.Code;
using Nuntius.App.UI.UserControls.BaseControls;
using Nuntius.Services;

namespace Nuntius.App.UI.UserControls.Broadcasts
{
    public partial class AllBroadcastsUserControl : NuntiusControl
    {
        #region Define as Singleton
        private static AllBroadcastsUserControl _Instance;

        public static AllBroadcastsUserControl Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new AllBroadcastsUserControl();
                }

                return (_Instance);
            }
        }

        private AllBroadcastsUserControl()
        {
            InitializeComponent();
        }
        #endregion

        int PageNumber = 1;
        int TotalRecordsCount = 0;
        
        public new async void Load(bool Clear = false)
        {
            base.Load();

            DashboardForm.Instance.DisplayLoader();
            this.Enabled = false;
            
            if(Clear)
            {
                ClearControls();
            }
            
            var broadcasts = await BroadcastsService.Instance.SearchBroadcasts(txtSearch.Text.Trim(), PageNumber, Configurations.SinglePageRecordsCount);
            TotalRecordsCount = await BroadcastsService.Instance.GetBroadcastsCount(txtSearch.Text.Trim());
            
            dgvBroadcasts.DataSource = broadcasts.Select(x=> new { ID = x.ID, Text = x.Text.IfNullOrEmptyShowAlternative("-"), Contacts = x.Messages.Count, Time = x.StartTime, Status = (BroadcastStatus)x.Status }).ToList();
            
            lblFoundBroadcastsCount.Text = string.Format(Messages.BroadcastsDisplayLabel, TotalRecordsCount == 0 ? TotalRecordsCount : ((PageNumber - 1) * Configurations.SinglePageRecordsCount) + 1, TotalRecordsCount > Configurations.SinglePageRecordsCount ? ((PageNumber - 1) * Configurations.SinglePageRecordsCount) + Configurations.SinglePageRecordsCount : TotalRecordsCount, TotalRecordsCount);

            this.Enabled = true;

            ValidatePagination();

            DashboardForm.Instance.DisplayLoader(false);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            PageNumber = PageNumber + 1;

            Load();
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            PageNumber = PageNumber - 1;

            Load();
        }

        private void ValidatePagination()
        {
            var TotalPages = Math.Ceiling((decimal)TotalRecordsCount / Configurations.SinglePageRecordsCount);

            if (PageNumber <= 1)
            {
                btnPrevious.Enabled = false;
            }
            else if(PageNumber <= TotalPages)
            {
                btnPrevious.Enabled = true;
            }

            if (PageNumber >= TotalPages)
            {
                btnNext.Enabled = false;
            }
            else
            {
                btnNext.Enabled = true;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            PageNumber = 1;

            Load();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            PageNumber = 1;

            Load(true);
        }

        private void ClearControls()
        {
            PageNumber = 1;
            txtSearch.Clear();
            lblFoundBroadcastsCount.Text = string.Empty;
        }
        
        private void btnNew_Click(object sender, EventArgs e)
        {
            NewBroadcastUserControl.Instance.Load(null);
        }

        private async void dgvBroadcasts_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && 
                (e.ColumnIndex == dgvBroadcasts.Columns["colDetails"].Index ||
                e.ColumnIndex == dgvBroadcasts.Columns["colResend"].Index ||
                e.ColumnIndex == dgvBroadcasts.Columns["colCancel"].Index ||
                e.ColumnIndex == dgvBroadcasts.Columns["colDelete"].Index))
            {
                DashboardForm.Instance.DisplayLoader();
                this.Enabled = false;

                int broadcastID = Convert.ToInt32(dgvBroadcasts.Rows[e.RowIndex].Cells["colID"].Value);
                var broadcast = await BroadcastsService.Instance.GetBroadcastByID(broadcastID);

                this.Enabled = true;
                DashboardForm.Instance.DisplayLoader(false);
                
                if (e.ColumnIndex == dgvBroadcasts.Columns["colDetails"].Index)
                {
                    BroadcastUserControl.Instance.Load(broadcastID);
                }
                else if (e.ColumnIndex == dgvBroadcasts.Columns["colResend"].Index)
                {
                    if (broadcast.Status != (int)BroadcastStatus.Processing)
                    {
                        if (DashboardForm.Instance.ShowConfirmationMessage(Messages.ConfirmBroadcastResend) == DialogResult.OK)
                        {
                            DashboardForm.Instance.DisplayLoader();

                            broadcast.Status = (int)BroadcastStatus.Processing;
                            broadcast.StartTime = DateTime.Now;
                            broadcast.CompletedTime = null;

                            foreach (var message in broadcast.Messages)
                            {
                                message.Status = (int)MessageStatus.Pending;
                                message.Result = string.Empty;
                                message.SentTime = null;
                            }

                            if (await BroadcastsService.Instance.UpdateBroadcast(broadcast))
                            {
                                DashboardForm.Instance.DisplayLoader(false);

                                BroadcastUserControl.Instance.Load(broadcastID, true);
                            }
                            else
                            {
                                DashboardForm.Instance.DisplayLoader(false);
                                DashboardForm.Instance.ShowErrorMessage(Messages.BroadcastNotResend);
                            }
                        }
                    }
                    else
                    {
                        DashboardForm.Instance.ShowWarningMessage(Messages.BroadcastAlreadyInProgress);
                    }
                }
                else if (e.ColumnIndex == dgvBroadcasts.Columns["colCancel"].Index)
                {
                    if (broadcast.Status == (int)BroadcastStatus.Processing || broadcast.Status == (int)BroadcastStatus.Pending)
                    {
                        if (DashboardForm.Instance.ShowConfirmationMessage(Messages.ConfirmBroadcastCancel) == DialogResult.OK)
                        {
                            DashboardForm.Instance.DisplayLoader();

                            broadcast.Status = (int)BroadcastStatus.Cancelled;
                            broadcast.CompletedTime = DateTime.Now;

                            foreach (var message in broadcast.Messages.Where(x=>x.Status == (int)MessageStatus.Pending))
                            {
                                message.Status = (int)MessageStatus.Cancelled;
                                message.SentTime = null;
                            }

                            if (await BroadcastsService.Instance.UpdateBroadcast(broadcast))
                            {
                                DashboardForm.Instance.DisplayLoader(false);

                                PageNumber = 1;
                                Load(true);
                            }
                            else
                            {
                                DashboardForm.Instance.DisplayLoader(false);
                                DashboardForm.Instance.ShowErrorMessage(Messages.BroadcastNotCancelled);
                            }
                        }
                    }
                    else
                    {
                        DashboardForm.Instance.ShowWarningMessage(Messages.BroadcastNotInProgress);
                    }
                }
                else if (e.ColumnIndex == dgvBroadcasts.Columns["colDelete"].Index)
                {
                    if(DashboardForm.Instance.ShowConfirmationMessage(Messages.BroadcastConfirmDelete) == DialogResult.OK)
                    {
                        DashboardForm.Instance.DisplayLoader();

                        if (await BroadcastsService.Instance.DeleteBroadcast(broadcast))
                        {
                            DashboardForm.Instance.DisplayLoader(false);

                            PageNumber = 1;
                            Load(true);
                        }
                        else
                        {
                            DashboardForm.Instance.DisplayLoader(false);
                            DashboardForm.Instance.ShowErrorMessage(Messages.BroadcastNotDeleted);
                        }
                    }
                }                           
            }
        }

        private void dgvBroadcasts_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                dgvBroadcasts_CellContentClick(dgvBroadcasts, new DataGridViewCellEventArgs(dgvBroadcasts.CurrentCell.ColumnIndex, dgvBroadcasts.CurrentCell.RowIndex));
            }
        }
    }
}
