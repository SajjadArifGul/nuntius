﻿using System;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Nuntius.Entities;
using Nuntius.Data;
using Nuntius.App.UI.Forms;
using Nuntius.Globals;
using Nuntius.Messenger;
using System.Threading;
using Nuntius.App.UI.UserControls.Settings;
using Nuntius.App.UI.UserControls.BaseControls;
using Nuntius.App.Code;
using Nuntius.Services;
using System.Collections.Generic;
using System.Drawing;

namespace Nuntius.App.UI.UserControls.Broadcasts
{
    public partial class BroadcastUserControl : NuntiusControl
    {
        #region Define as Singleton
        private static BroadcastUserControl _Instance;

        public static BroadcastUserControl Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new BroadcastUserControl();
                }

                return (_Instance);
            }
        }

        private BroadcastUserControl()
        {
            InitializeComponent();
        }
        #endregion

        #region Global Variables
        Broadcast Broadcast { get; set; }
        int PageNumber = 1;
        int TotalRecordsCount = 0;

        bool StartBroadcast = false;

        private delegate void UpdateBroadcastMessagesStatus();
        private UpdateBroadcastMessagesStatus BroadcastMessagesStatusUpdate = null;

        private delegate void UpdateBroadcastStatus();
        private UpdateBroadcastStatus BroadcastStatusUpdate = null;

        private delegate void UpdateMessageProcessStatus();
        private UpdateMessageProcessStatus MessageProcessStatusUpdate = null;
        #endregion

        public async new void Load(int broadcastID, bool startBroadcast = false)
        {
            base.Load();

            // Initialise the delegates
            this.BroadcastMessagesStatusUpdate = new UpdateBroadcastMessagesStatus(this.UpdateDataGridView);
            this.BroadcastStatusUpdate = new UpdateBroadcastStatus(this.LoadBroadcast);
            this.MessageProcessStatusUpdate = new UpdateMessageProcessStatus(this.MessageProcessStatus);

            DashboardForm.Instance.DisplayLoader();
            this.Enabled = false;

            ClearControls();

            this.StartBroadcast = startBroadcast;

            this.Broadcast = await BroadcastsService.Instance.GetBroadcastByID(broadcastID);

            if (this.Broadcast != null)
            {
                LoadBroadcast();            
            }

            this.Enabled = true;
            DashboardForm.Instance.DisplayLoader(false);

            ProcessBroadcast();
        }

        private async void btnCancel_Click(object sender, EventArgs e)
        {
            if (Broadcast.Messages.Any(x => x.Status == (int)MessageStatus.Pending))
            {
                if (DashboardForm.Instance.ShowConfirmationMessage(Messages.ConfirmBroadcastCancel) == DialogResult.OK)
                {
                    DashboardForm.Instance.DisplayLoader();

                    this.StopBroadcast = true;
                    this.BroadcastThread.Abort();

                    Broadcast.Status = (int)BroadcastStatus.Cancelled;
                    Broadcast.StartTime = DateTime.Now;

                    foreach (var message in Broadcast.Messages)
                    {
                        if (message.Status == (int)MessageStatus.Pending)
                            message.Status = (int)MessageStatus.Cancelled;
                    }

                    if (await BroadcastsService.Instance.UpdateBroadcast(Broadcast))
                    {
                        DashboardForm.Instance.DisplayLoader(false);

                        PageNumber = 1;
                        LoadBroadcast();
                    }
                    else
                    {
                        DashboardForm.Instance.DisplayLoader(false);
                        DashboardForm.Instance.ShowErrorMessage(Messages.BroadcastNotCancelled);
                    }
                }
            }
            else
            {
                DashboardForm.Instance.ShowWarningMessage(Messages.BroadcastNotInProgress);
            }
        }
                
        #region Broadcast Loader

        private void LoadBroadcast()
        {
            rtxtDetails.Text = Broadcast.Text;
            lblStatus.Text = ((BroadcastStatus)Broadcast.Status).ToString();
            lblStartTime.Text = Broadcast.StartTime.HasValue ? Broadcast.StartTime.Value.ToString() : "-";
            lblCompletedTime.Text = Broadcast.CompletedTime.HasValue ? Broadcast.CompletedTime.Value.ToString() : "-";

            TotalRecordsCount = Broadcast.Messages.Count;

            UpdateDataGridView();

            ValidatePagination();

            btnCancel.Enabled = Broadcast.Messages.Any(x => x.Status == (int)MessageStatus.Pending);
        }

        private void UpdateDataGridView()
        {
            var messages = Broadcast.Messages.Skip((PageNumber - 1) * Configurations.SinglePageRecordsCount)
                                            .Take(Configurations.SinglePageRecordsCount)
                                            .ToList();

            dgvMessages.DataSource = messages.Select(x => new {
                ID = x.ID,
                Name = x.Contact.Name,
                Contacts = x.Contact.Numbers.JoinNumbers(", "),
                Status = GetMessageStatusImage((MessageStatus)x.Status),
                Result = x.Result,
                Time = x.SentTime
            }).ToList();

            dgvMessages.Refresh();

            lblMessagingStatus.Text = string.Format(Messages.MessageProcessCount, Broadcast.Messages.Where(x=>x.Status == (int)MessageStatus.Sent).Count(), Broadcast.Messages.Count);
        }

        private Image GetMessageStatusImage(MessageStatus messageStatus)
        {
            if (messageStatus == MessageStatus.Pending)
            {
                return Properties.Resources.message_pending_white_32;
            }
            else if (messageStatus == MessageStatus.Sent)
            {
                return Properties.Resources.message_sent_white_32;
            }
            else if (messageStatus == MessageStatus.Cancelled)
            {
                return Properties.Resources.message_cancelled_white_32;
            }
            else //if (messageStatus == MessageStatus.Failed)
            {
                return Properties.Resources.message_failed_white_32;
            }
        }

        private void ClearControls()
        {
            PageNumber = 1;
            TotalRecordsCount = 0;

            rtxtDetails.Clear();
            lblStatus.Text = string.Empty;
            lblStartTime.Text = string.Empty;
            lblCompletedTime.Text = string.Empty;

            dgvMessages.DataSource = new List<Entities.Message>().Select(x => new { ID = "", Name = "", Contacts = "", Status = "", Result = "", Time = "" }).ToList();

            btnPrevious.Enabled = false;
            btnNext.Enabled = false;
            btnCancel.Enabled = false;
        }
        #endregion

        #region Pagination        
        private void ValidatePagination()
        {
            var TotalPages = Math.Ceiling((decimal)TotalRecordsCount / Configurations.SinglePageRecordsCount);

            if (PageNumber <= 1)
            {
                btnPrevious.Enabled = false;
            }
            else if (PageNumber <= TotalPages)
            {
                btnPrevious.Enabled = true;
            }

            if (PageNumber >= TotalPages)
            {
                btnNext.Enabled = false;
            }
            else
            {
                btnNext.Enabled = true;
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            PageNumber = PageNumber + 1;

            LoadBroadcast();
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            PageNumber = PageNumber - 1;

            LoadBroadcast();
        }
        #endregion

        #region Broadcast Thread and Processes

        // Declare our worker thread
        private Thread BroadcastThread = null;
        private bool StopBroadcast = false;

        private void ProcessBroadcast()
        {
            try
            {
                if (Broadcast != null && StartBroadcast /*&& Broadcast.Status == (int)BroadcastStatus.Processing*/)
                {
                    // Initialise and start worker thread
                    this.BroadcastThread = new Thread(new ThreadStart(this.SendMessages));
                    this.BroadcastThread.Start();

                    DashboardForm.Instance.DisplayLoader();
                    DashboardForm.Instance.MenuNavigation(false);
                }
            }
            catch (Exception ex)
            {
                DashboardForm.Instance.ShowErrorMessage(string.Format(Messages.BroadcastThreadStartErrorOccurredMessage, ex.StackTrace, ex.Message));
            }
        }

        private async void SendMessages()
        {
            int messageCount = 0;
            foreach (var message in Broadcast.Messages)
            {
                if (!this.StopBroadcast)
                {
                    var messageResults = string.Empty;

                    foreach (var number in message.Contact.Numbers)
                    {
                        messageResults += await Postman.Send(Broadcast.Text, number.Value);

                        messageCount++;

                        if (messageCount >= Configurations.SMSMessageCount)
                        {
                            this.Invoke(this.MessageProcessStatusUpdate);
                            Thread.Sleep(Configurations.SMSTimeInMiliSeconds);

                            messageCount = 0;
                        }
                    }

                    message.Status = (int)MessageStatus.Sent;
                    message.SentTime = DateTime.Now;
                    message.Result = messageResults;
                    
                    this.Invoke(this.BroadcastMessagesStatusUpdate);
                }
                else
                {
                    break;
                }
            }

            Broadcast.Status = (int)BroadcastStatus.Completed;
            Broadcast.CompletedTime = DateTime.Now;
            await BroadcastsService.Instance.UpdateBroadcast(Broadcast);

            this.Invoke(this.BroadcastStatusUpdate);
            this.BroadcastThread.Abort();
        }

        private void MessageProcessStatus()
        {
            lblMessagingStatus.Text = string.Format(Messages.MessageProcessWaitTime, Configurations.SMSTime);
        }
        #endregion
    }
}
