﻿namespace Nuntius.App.UI.UserControls.Broadcasts
{
    partial class BroadcastsMainUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.UserControlMenuFlowPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.btnBroadcastsHistory = new System.Windows.Forms.Button();
            this.btnNewBroadcast = new System.Windows.Forms.Button();
            this.lblControlHeader = new System.Windows.Forms.Label();
            this.btnWhatsAppBroadcast = new System.Windows.Forms.Button();
            this.UserControlMenuFlowPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // UserControlMenuFlowPanel
            // 
            this.UserControlMenuFlowPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.UserControlMenuFlowPanel.Controls.Add(this.btnBroadcastsHistory);
            this.UserControlMenuFlowPanel.Controls.Add(this.btnNewBroadcast);
            this.UserControlMenuFlowPanel.Controls.Add(this.btnWhatsAppBroadcast);
            this.UserControlMenuFlowPanel.Location = new System.Drawing.Point(0, 37);
            this.UserControlMenuFlowPanel.Margin = new System.Windows.Forms.Padding(0);
            this.UserControlMenuFlowPanel.Name = "UserControlMenuFlowPanel";
            this.UserControlMenuFlowPanel.Padding = new System.Windows.Forms.Padding(5);
            this.UserControlMenuFlowPanel.Size = new System.Drawing.Size(791, 586);
            this.UserControlMenuFlowPanel.TabIndex = 7;
            // 
            // btnBroadcastsHistory
            // 
            this.btnBroadcastsHistory.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.btnBroadcastsHistory.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBroadcastsHistory.FlatAppearance.BorderSize = 0;
            this.btnBroadcastsHistory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBroadcastsHistory.Font = new System.Drawing.Font("Bahnschrift SemiBold", 10F, System.Drawing.FontStyle.Bold);
            this.btnBroadcastsHistory.Image = global::Nuntius.App.Properties.Resources.broadcasts_white_45;
            this.btnBroadcastsHistory.Location = new System.Drawing.Point(10, 10);
            this.btnBroadcastsHistory.Margin = new System.Windows.Forms.Padding(5);
            this.btnBroadcastsHistory.Name = "btnBroadcastsHistory";
            this.btnBroadcastsHistory.Size = new System.Drawing.Size(192, 85);
            this.btnBroadcastsHistory.TabIndex = 0;
            this.btnBroadcastsHistory.Text = "Broadcasts History";
            this.btnBroadcastsHistory.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnBroadcastsHistory.UseVisualStyleBackColor = false;
            this.btnBroadcastsHistory.Click += new System.EventHandler(this.btnBroadcastsHistory_Click);
            // 
            // btnNewBroadcast
            // 
            this.btnNewBroadcast.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.btnNewBroadcast.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNewBroadcast.FlatAppearance.BorderSize = 0;
            this.btnNewBroadcast.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNewBroadcast.Font = new System.Drawing.Font("Bahnschrift SemiBold", 10F, System.Drawing.FontStyle.Bold);
            this.btnNewBroadcast.Image = global::Nuntius.App.Properties.Resources.new_broadcast_white_45;
            this.btnNewBroadcast.Location = new System.Drawing.Point(212, 10);
            this.btnNewBroadcast.Margin = new System.Windows.Forms.Padding(5);
            this.btnNewBroadcast.Name = "btnNewBroadcast";
            this.btnNewBroadcast.Size = new System.Drawing.Size(192, 85);
            this.btnNewBroadcast.TabIndex = 1;
            this.btnNewBroadcast.Text = "New Broadcast";
            this.btnNewBroadcast.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnNewBroadcast.UseVisualStyleBackColor = false;
            this.btnNewBroadcast.Click += new System.EventHandler(this.btnNewBroadcast_Click);
            // 
            // lblControlHeader
            // 
            this.lblControlHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblControlHeader.BackColor = System.Drawing.Color.Transparent;
            this.lblControlHeader.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblControlHeader.Font = new System.Drawing.Font("Bahnschrift SemiBold", 13F, System.Drawing.FontStyle.Bold);
            this.lblControlHeader.Image = global::Nuntius.App.Properties.Resources.broadcasts_white_45;
            this.lblControlHeader.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblControlHeader.Location = new System.Drawing.Point(0, 0);
            this.lblControlHeader.Margin = new System.Windows.Forms.Padding(0);
            this.lblControlHeader.Name = "lblControlHeader";
            this.lblControlHeader.Size = new System.Drawing.Size(791, 37);
            this.lblControlHeader.TabIndex = 6;
            this.lblControlHeader.Text = "Broadcasts";
            this.lblControlHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnWhatsAppBroadcast
            // 
            this.btnWhatsAppBroadcast.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.btnWhatsAppBroadcast.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnWhatsAppBroadcast.FlatAppearance.BorderSize = 0;
            this.btnWhatsAppBroadcast.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnWhatsAppBroadcast.Font = new System.Drawing.Font("Bahnschrift SemiBold", 10F, System.Drawing.FontStyle.Bold);
            this.btnWhatsAppBroadcast.Image = global::Nuntius.App.Properties.Resources.whatsapp_white_45;
            this.btnWhatsAppBroadcast.Location = new System.Drawing.Point(414, 10);
            this.btnWhatsAppBroadcast.Margin = new System.Windows.Forms.Padding(5);
            this.btnWhatsAppBroadcast.Name = "btnWhatsAppBroadcast";
            this.btnWhatsAppBroadcast.Size = new System.Drawing.Size(192, 85);
            this.btnWhatsAppBroadcast.TabIndex = 2;
            this.btnWhatsAppBroadcast.Text = "Whatsapp Broadcast";
            this.btnWhatsAppBroadcast.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnWhatsAppBroadcast.UseVisualStyleBackColor = false;
            // 
            // BroadcastsMainUserControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(155)))), ((int)(((byte)(229)))));
            this.Controls.Add(this.UserControlMenuFlowPanel);
            this.Controls.Add(this.lblControlHeader);
            this.Font = new System.Drawing.Font("Bahnschrift", 9F);
            this.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.Name = "BroadcastsMainUserControl";
            this.Size = new System.Drawing.Size(791, 623);
            this.UserControlMenuFlowPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblControlHeader;
        private System.Windows.Forms.FlowLayoutPanel UserControlMenuFlowPanel;
        private System.Windows.Forms.Button btnBroadcastsHistory;
        private System.Windows.Forms.Button btnNewBroadcast;
        private System.Windows.Forms.Button btnWhatsAppBroadcast;
    }
}
