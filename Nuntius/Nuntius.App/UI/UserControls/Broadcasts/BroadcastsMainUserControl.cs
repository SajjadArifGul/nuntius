﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nuntius.App.UI.Forms;
using Nuntius.App.UI.UserControls.BaseControls;

namespace Nuntius.App.UI.UserControls.Broadcasts
{
    public partial class BroadcastsMainUserControl : NuntiusControl
    {
        #region Define as Singleton
        private static BroadcastsMainUserControl _Instance;

        public static BroadcastsMainUserControl Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new BroadcastsMainUserControl();
                }

                return (_Instance);
            }
        }

        private BroadcastsMainUserControl()
        {
            InitializeComponent();
        }
        #endregion

        private void btnNewBroadcast_Click(object sender, EventArgs e)
        {
            NewBroadcastUserControl.Instance.Load(null);
        }

        private void btnBroadcastsHistory_Click(object sender, EventArgs e)
        {
            AllBroadcastsUserControl.Instance.Load(Clear: true);
        }
    }
}
