﻿namespace Nuntius.App.UI.UserControls.Broadcasts
{
    partial class NewBroadcastUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblControlHeader = new System.Windows.Forms.Label();
            this.ContainerPanel = new System.Windows.Forms.Panel();
            this.lblResultsCounts = new System.Windows.Forms.Label();
            this.btnBroadcasts = new System.Windows.Forms.Button();
            this.lblMessageCharactersCount = new System.Windows.Forms.Label();
            this.lbBroadcastContactsCount = new System.Windows.Forms.Label();
            this.rtxtMessageText = new System.Windows.Forms.RichTextBox();
            this.lbBroadcastContacts = new System.Windows.Forms.ListBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lbSearchContacts = new System.Windows.Forms.ListBox();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnSendBroadcast = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.ContainerPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblControlHeader
            // 
            this.lblControlHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblControlHeader.BackColor = System.Drawing.Color.Transparent;
            this.lblControlHeader.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblControlHeader.Font = new System.Drawing.Font("Bahnschrift SemiBold", 13F, System.Drawing.FontStyle.Bold);
            this.lblControlHeader.Image = global::Nuntius.App.Properties.Resources.new_broadcast_white_45;
            this.lblControlHeader.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblControlHeader.Location = new System.Drawing.Point(0, 0);
            this.lblControlHeader.Margin = new System.Windows.Forms.Padding(0);
            this.lblControlHeader.Name = "lblControlHeader";
            this.lblControlHeader.Size = new System.Drawing.Size(791, 37);
            this.lblControlHeader.TabIndex = 6;
            this.lblControlHeader.Text = "New Broadcast";
            this.lblControlHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ContainerPanel
            // 
            this.ContainerPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ContainerPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(155)))), ((int)(((byte)(229)))));
            this.ContainerPanel.Controls.Add(this.lblResultsCounts);
            this.ContainerPanel.Controls.Add(this.btnBroadcasts);
            this.ContainerPanel.Controls.Add(this.lblMessageCharactersCount);
            this.ContainerPanel.Controls.Add(this.lbBroadcastContactsCount);
            this.ContainerPanel.Controls.Add(this.rtxtMessageText);
            this.ContainerPanel.Controls.Add(this.lbBroadcastContacts);
            this.ContainerPanel.Controls.Add(this.label8);
            this.ContainerPanel.Controls.Add(this.lbSearchContacts);
            this.ContainerPanel.Controls.Add(this.txtSearch);
            this.ContainerPanel.Controls.Add(this.btnSearch);
            this.ContainerPanel.Controls.Add(this.label7);
            this.ContainerPanel.Controls.Add(this.label6);
            this.ContainerPanel.Controls.Add(this.label5);
            this.ContainerPanel.Controls.Add(this.btnSendBroadcast);
            this.ContainerPanel.Controls.Add(this.label2);
            this.ContainerPanel.Location = new System.Drawing.Point(0, 37);
            this.ContainerPanel.Margin = new System.Windows.Forms.Padding(0);
            this.ContainerPanel.Name = "ContainerPanel";
            this.ContainerPanel.Size = new System.Drawing.Size(791, 586);
            this.ContainerPanel.TabIndex = 7;
            // 
            // lblResultsCounts
            // 
            this.lblResultsCounts.Location = new System.Drawing.Point(12, 90);
            this.lblResultsCounts.Name = "lblResultsCounts";
            this.lblResultsCounts.Size = new System.Drawing.Size(100, 94);
            this.lblResultsCounts.TabIndex = 30;
            this.lblResultsCounts.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnBroadcasts
            // 
            this.btnBroadcasts.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBroadcasts.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.btnBroadcasts.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBroadcasts.FlatAppearance.BorderSize = 0;
            this.btnBroadcasts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBroadcasts.Image = global::Nuntius.App.Properties.Resources.broadcasts_white_45;
            this.btnBroadcasts.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBroadcasts.Location = new System.Drawing.Point(487, 530);
            this.btnBroadcasts.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.btnBroadcasts.Name = "btnBroadcasts";
            this.btnBroadcasts.Padding = new System.Windows.Forms.Padding(1);
            this.btnBroadcasts.Size = new System.Drawing.Size(119, 30);
            this.btnBroadcasts.TabIndex = 29;
            this.btnBroadcasts.Text = "Broadcasts";
            this.btnBroadcasts.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBroadcasts.UseVisualStyleBackColor = false;
            this.btnBroadcasts.Click += new System.EventHandler(this.btnBroadcasts_Click);
            // 
            // lblMessageCharactersCount
            // 
            this.lblMessageCharactersCount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMessageCharactersCount.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Italic);
            this.lblMessageCharactersCount.Location = new System.Drawing.Point(118, 509);
            this.lblMessageCharactersCount.Name = "lblMessageCharactersCount";
            this.lblMessageCharactersCount.Size = new System.Drawing.Size(594, 20);
            this.lblMessageCharactersCount.TabIndex = 28;
            // 
            // lbBroadcastContactsCount
            // 
            this.lbBroadcastContactsCount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbBroadcastContactsCount.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Italic);
            this.lbBroadcastContactsCount.Location = new System.Drawing.Point(118, 391);
            this.lbBroadcastContactsCount.Name = "lbBroadcastContactsCount";
            this.lbBroadcastContactsCount.Size = new System.Drawing.Size(594, 24);
            this.lbBroadcastContactsCount.TabIndex = 27;
            this.lbBroadcastContactsCount.Text = "30 Members";
            // 
            // rtxtMessageText
            // 
            this.rtxtMessageText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtxtMessageText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtxtMessageText.Font = new System.Drawing.Font("Bahnschrift", 14F);
            this.rtxtMessageText.Location = new System.Drawing.Point(118, 418);
            this.rtxtMessageText.Name = "rtxtMessageText";
            this.rtxtMessageText.Size = new System.Drawing.Size(594, 88);
            this.rtxtMessageText.TabIndex = 26;
            this.rtxtMessageText.Text = "";
            this.rtxtMessageText.TextChanged += new System.EventHandler(this.rtxtMessageText_TextChanged);
            // 
            // lbBroadcastContacts
            // 
            this.lbBroadcastContacts.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbBroadcastContacts.Font = new System.Drawing.Font("Bahnschrift", 14F);
            this.lbBroadcastContacts.FormattingEnabled = true;
            this.lbBroadcastContacts.IntegralHeight = false;
            this.lbBroadcastContacts.ItemHeight = 23;
            this.lbBroadcastContacts.Location = new System.Drawing.Point(118, 190);
            this.lbBroadcastContacts.Name = "lbBroadcastContacts";
            this.lbBroadcastContacts.Size = new System.Drawing.Size(594, 198);
            this.lbBroadcastContacts.TabIndex = 25;
            this.lbBroadcastContacts.DoubleClick += new System.EventHandler(this.lbGroupContacts_DoubleClick);
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(12, 190);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 45);
            this.label8.TabIndex = 24;
            this.label8.Text = "Broadcast to Contacts";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbSearchContacts
            // 
            this.lbSearchContacts.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbSearchContacts.Font = new System.Drawing.Font("Bahnschrift", 14F);
            this.lbSearchContacts.FormattingEnabled = true;
            this.lbSearchContacts.IntegralHeight = false;
            this.lbSearchContacts.ItemHeight = 23;
            this.lbSearchContacts.Location = new System.Drawing.Point(118, 68);
            this.lbSearchContacts.Name = "lbSearchContacts";
            this.lbSearchContacts.Size = new System.Drawing.Size(594, 116);
            this.lbSearchContacts.TabIndex = 23;
            this.lbSearchContacts.DoubleClick += new System.EventHandler(this.lbSearchContacts_DoubleClick);
            // 
            // txtSearch
            // 
            this.txtSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearch.Font = new System.Drawing.Font("Bahnschrift", 14F);
            this.txtSearch.Location = new System.Drawing.Point(118, 32);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(488, 30);
            this.txtSearch.TabIndex = 22;
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.btnSearch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSearch.FlatAppearance.BorderSize = 0;
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.Image = global::Nuntius.App.Properties.Resources.contact_search_white_45;
            this.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearch.Location = new System.Drawing.Point(612, 32);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Padding = new System.Windows.Forms.Padding(1);
            this.btnSearch.Size = new System.Drawing.Size(100, 30);
            this.btnSearch.TabIndex = 21;
            this.btnSearch.Text = "Search";
            this.btnSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(12, 32);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 30);
            this.label7.TabIndex = 20;
            this.label7.Text = "Search";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Italic);
            this.label6.Location = new System.Drawing.Point(118, 1);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(394, 28);
            this.label6.TabIndex = 18;
            this.label6.Text = "Add Contacts to this Broadcast";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(15, 418);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 42);
            this.label5.TabIndex = 17;
            this.label5.Text = "Message Text";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnSendBroadcast
            // 
            this.btnSendBroadcast.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSendBroadcast.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.btnSendBroadcast.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSendBroadcast.FlatAppearance.BorderSize = 0;
            this.btnSendBroadcast.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSendBroadcast.Image = global::Nuntius.App.Properties.Resources.save_white_45;
            this.btnSendBroadcast.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSendBroadcast.Location = new System.Drawing.Point(612, 530);
            this.btnSendBroadcast.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.btnSendBroadcast.Name = "btnSendBroadcast";
            this.btnSendBroadcast.Padding = new System.Windows.Forms.Padding(1);
            this.btnSendBroadcast.Size = new System.Drawing.Size(100, 30);
            this.btnSendBroadcast.TabIndex = 10;
            this.btnSendBroadcast.Text = "Send";
            this.btnSendBroadcast.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSendBroadcast.UseVisualStyleBackColor = false;
            this.btnSendBroadcast.Click += new System.EventHandler(this.btnSendBroadcast_Click);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(12, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 22);
            this.label2.TabIndex = 3;
            this.label2.Text = "Search Results";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // NewBroadcastUserControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(155)))), ((int)(((byte)(229)))));
            this.Controls.Add(this.ContainerPanel);
            this.Controls.Add(this.lblControlHeader);
            this.Font = new System.Drawing.Font("Bahnschrift", 9F);
            this.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.Name = "NewBroadcastUserControl";
            this.Size = new System.Drawing.Size(791, 623);
            this.ContainerPanel.ResumeLayout(false);
            this.ContainerPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblControlHeader;
        private System.Windows.Forms.Panel ContainerPanel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSendBroadcast;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ListBox lbBroadcastContacts;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ListBox lbSearchContacts;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.RichTextBox rtxtMessageText;
        private System.Windows.Forms.Label lbBroadcastContactsCount;
        private System.Windows.Forms.Label lblMessageCharactersCount;
        private System.Windows.Forms.Button btnBroadcasts;
        private System.Windows.Forms.Label lblResultsCounts;
    }
}
