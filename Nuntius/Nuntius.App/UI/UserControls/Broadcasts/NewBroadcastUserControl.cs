﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nuntius.Entities;
using Nuntius.Data;
using Nuntius.App.UI.Forms;
using Nuntius.Globals;
using Nuntius.App.Code;
using Nuntius.App.UI.UserControls.BaseControls;
using Nuntius.Services;

namespace Nuntius.App.UI.UserControls.Broadcasts
{
    public partial class NewBroadcastUserControl : NuntiusControl
    {
        #region Define as Singleton
        private static NewBroadcastUserControl _Instance;

        public static NewBroadcastUserControl Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new NewBroadcastUserControl();
                }

                return (_Instance);
            }
        }

        private NewBroadcastUserControl()
        {
            InitializeComponent();
        }
        #endregion
        
        public List<Contact> BroadcastContacts { get; set; }
        
        public new void Load(List<Contact> broadcastContacts)
        {
            base.Load();

            if(broadcastContacts != null)
            {
                this.BroadcastContacts = broadcastContacts;
                
                UpdateBroadcastContactsSource();
            }
            else
            {
                ClearControls();
            }            
        }

        private async void btnSendBroadcast_Click(object sender, EventArgs e)
        {
            if (ValidateControl())
            {
                DashboardForm.Instance.DisplayLoader();
                this.Enabled = false;

                Broadcast newBroadcast = new Broadcast();
                newBroadcast.Text = rtxtMessageText.Text;
                newBroadcast.StartTime = DateTime.Now;
                newBroadcast.Status = (int)BroadcastStatus.Processing;
                newBroadcast.Messages = BroadcastContacts.Select(x => new Entities.Message() { Status = (int)MessageStatus.Pending, ContactID = x.ID, BroadcasterID = Configurations.PrefferredBroadcaster }).ToList();

                if (await BroadcastsService.Instance.AddNewBroadcast(newBroadcast))
                {
                    ClearControls();

                    BroadcastUserControl.Instance.Load(newBroadcast.ID);
                }
                else
                {
                    DashboardForm.Instance.ShowErrorMessage(Messages.BroadcastNotAdded);
                }
                
                this.Enabled = true;
                DashboardForm.Instance.DisplayLoader(false);
            }
            else
            {
                DashboardForm.Instance.ShowErrorMessage(Messages.BroadcastValidationFailed);
            }
        }

        private void ClearControls()
        {
            rtxtMessageText.Clear();

            BroadcastContacts = new List<Contact>();
            UpdateBroadcastContactsSource();

            txtSearch.Clear();
            SearchContacts();
        }
        
        private bool ValidateControl()
        {
            if (string.IsNullOrEmpty(rtxtMessageText.Text.Trim())) return false;
            else if (BroadcastContacts == null || BroadcastContacts.Count <= 0) return false;
            else return true;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            SearchContacts();
        }

        private void lbSearchContacts_DoubleClick(object sender, EventArgs e)
        {
            Contact selectedContact = (Contact)lbSearchContacts.SelectedItem;

            if (selectedContact != null && !BroadcastContacts.Contains(selectedContact))
            {
                BroadcastContacts.Add(selectedContact);
                UpdateBroadcastContactsSource();
            }
        }

        private async void SearchContacts()
        {
            DashboardForm.Instance.DisplayLoader();
            this.Enabled = false;

            var contacts = string.IsNullOrEmpty(txtSearch.Text.Trim()) ? new List<Contact>() : await ContactsService.Instance.SearchContacts(txtSearch.Text.Trim());

            lbSearchContacts.DataSource = contacts;
            lbSearchContacts.DisplayMember = "DisplayName";
            lbSearchContacts.ValueMember = "ID";

            lblResultsCounts.Text = contacts != null && contacts.Count > 0 ? string.Format(Messages.NumberOfContacts, contacts.Count) : Messages.NoResults;

            this.Enabled = true;
            DashboardForm.Instance.DisplayLoader(false);
        }

        private void UpdateBroadcastContactsSource()
        {
            if (BroadcastContacts != null && BroadcastContacts.Count > 0)
            {
                var broadcastContactsSource = new BindingSource();
                broadcastContactsSource.DataSource = BroadcastContacts.OrderBy(x => x.Name).ToList();

                lbBroadcastContacts.DataSource = broadcastContactsSource;
                lbBroadcastContacts.ValueMember = "ID";
                lbBroadcastContacts.DisplayMember = "DisplayName";

                lbBroadcastContactsCount.Text = string.Format(Messages.BroadcastContactsCount, BroadcastContacts.Count);
            }
            else
            {
                lbBroadcastContacts.DataSource = null;
                lbBroadcastContactsCount.Text = string.Empty;
            }
        }

        private void lbGroupContacts_DoubleClick(object sender, EventArgs e)
        {
            Contact selectedContact = (Contact)lbBroadcastContacts.SelectedItem;

            if (selectedContact != null && BroadcastContacts.Contains(selectedContact))
            {
                BroadcastContacts.Remove(selectedContact);
                UpdateBroadcastContactsSource();
            }
        }

        private void btnBroadcasts_Click(object sender, EventArgs e)
        {
            AllBroadcastsUserControl.Instance.Load(Clear: true);
        }

        private void rtxtMessageText_TextChanged(object sender, EventArgs e)
        {
            lblMessageCharactersCount.Text = rtxtMessageText.Text.MessageCharactersLabel(Configurations.SingleMessageCharactersCount);
        }
    }
}
