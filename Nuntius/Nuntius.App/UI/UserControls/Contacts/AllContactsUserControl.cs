﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nuntius.Entities;
using Nuntius.Data;
using Nuntius.App.UI.Forms;
using Nuntius.Globals;
using Nuntius.App.UI.UserControls.Broadcasts;
using Nuntius.App.UI.UserControls.BaseControls;
using Nuntius.App.Code;
using Nuntius.Services;

namespace Nuntius.App.UI.UserControls.Contacts
{
    public partial class AllContactsUserControl : NuntiusControl
    {
        #region Define as Singleton
        private static AllContactsUserControl _Instance;

        public static AllContactsUserControl Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new AllContactsUserControl();
                }

                return (_Instance);
            }
        }

        private AllContactsUserControl()
        {
            InitializeComponent();
        }
        #endregion

        int PageNumber = 1;
        int TotalRecordsCount = 0;
        
        public new async void Load(bool Clear = false)
        {
            base.Load();

            DashboardForm.Instance.DisplayLoader();
            this.Enabled = false;
            
            if(Clear)
            {
                ClearControls();
            }
            
            var contacts = await ContactsService.Instance.SearchContacts(txtSearch.Text.Trim(), PageNumber, Configurations.SinglePageRecordsCount);
            TotalRecordsCount = await ContactsService.Instance.GetContactsCount(txtSearch.Text.Trim());
            
            dgvContacts.DataSource = contacts.Select(x=> new { ID = x.ID, Name = x.Name.IfNullOrEmptyShowAlternative("-"), Contacts = x.Numbers.JoinNumbers(", ") }).ToList();

            lblFoundContactsCount.Text = string.Format(Messages.ContactsDisplayLabel, TotalRecordsCount == 0 ? TotalRecordsCount : ((PageNumber - 1) * Configurations.SinglePageRecordsCount) + 1 , TotalRecordsCount > Configurations.SinglePageRecordsCount ? ((PageNumber - 1) * Configurations.SinglePageRecordsCount) + Configurations.SinglePageRecordsCount : TotalRecordsCount, TotalRecordsCount);

            this.Enabled = true;

            ValidatePagination();
            
            DashboardForm.Instance.DisplayLoader(false);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            PageNumber = PageNumber + 1;

            Load();
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            PageNumber = PageNumber - 1;

            Load();
        }

        private void ValidatePagination()
        {
            var TotalPages = Math.Ceiling((decimal)TotalRecordsCount / Configurations.SinglePageRecordsCount);

            if (PageNumber <= 1)
            {
                btnPrevious.Enabled = false;
            }
            else if(PageNumber <= TotalPages)
            {
                btnPrevious.Enabled = true;
            }

            if (PageNumber >= TotalPages)
            {
                btnNext.Enabled = false;
            }
            else
            {
                btnNext.Enabled = true;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            PageNumber = 1;

            Load();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            PageNumber = 1;

            Load(Clear: true);
        }

        private void ClearControls()
        {
            PageNumber = 1;
            txtSearch.Clear();
            lblFoundContactsCount.Text = string.Empty;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            ContactUserControl.Instance.Load(null);
        }

        private async void dgvContacts_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 &&
                (e.ColumnIndex == dgvContacts.Columns["colMessage"].Index ||
                e.ColumnIndex == dgvContacts.Columns["colDetails"].Index ||
                e.ColumnIndex == dgvContacts.Columns["colDelete"].Index))
            {
                DashboardForm.Instance.DisplayLoader();
                this.Enabled = false;

                int contactID = Convert.ToInt32(dgvContacts.Rows[e.RowIndex].Cells["colID"].Value);
                var contact = await ContactsService.Instance.GetContactByID(contactID);

                this.Enabled = true;
                DashboardForm.Instance.DisplayLoader(false);
                
                if (e.ColumnIndex == dgvContacts.Columns["colMessage"].Index)
                {
                    NewBroadcastUserControl.Instance.Load(new List<Contact>() { contact });
                }
                else if (e.ColumnIndex == dgvContacts.Columns["colDetails"].Index)
                {
                    ContactUserControl.Instance.Load(contact);
                }
                else if (e.ColumnIndex == dgvContacts.Columns["colDelete"].Index)
                {
                    if(DashboardForm.Instance.ShowConfirmationMessage(Messages.ContactConfirmDelete) == DialogResult.OK)
                    {
                        DashboardForm.Instance.DisplayLoader();
                        if (await ContactsService.Instance.DeleteContact(contact))
                        {
                            DashboardForm.Instance.DisplayLoader(false);

                            //contact has been deleted. Refresh the Contacts list from DB
                            PageNumber = 1;
                            Load(Clear: true);
                        }
                        else
                        {
                            DashboardForm.Instance.DisplayLoader(false);
                            DashboardForm.Instance.ShowErrorMessage(Messages.ContactNotDeleted);
                        }
                    }
                }
            }
        }

        private void dgvContacts_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                dgvContacts_CellContentClick(dgvContacts, new DataGridViewCellEventArgs(dgvContacts.CurrentCell.ColumnIndex, dgvContacts.CurrentCell.RowIndex));
            }
        }
    }
}
