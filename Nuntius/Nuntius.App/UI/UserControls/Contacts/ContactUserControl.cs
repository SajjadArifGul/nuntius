﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nuntius.Entities;
using Nuntius.Data;
using Nuntius.App.UI.Forms;
using Nuntius.Globals;
using Nuntius.App.UI.UserControls.BaseControls;
using Nuntius.Services;

namespace Nuntius.App.UI.UserControls.Contacts
{
    public partial class ContactUserControl : NuntiusControl
    {
        #region Define as Singleton
        private static ContactUserControl _Instance;

        public static ContactUserControl Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new ContactUserControl();
                }

                return (_Instance);
            }
        }

        private ContactUserControl()
        {
            InitializeComponent();
        }
        #endregion

        public Contact Contact { get; set; }

        public new void Load(Contact contact)
        {
            base.Load();

            if (contact != null)
            {
                this.Contact = contact;
                btnDelete.Show();

                txtName.Text = contact.Name;
                txtNumbers.Text = contact.Numbers.JoinNumbers(Environment.NewLine);
            }
            else
            {
                btnDelete.Hide();

                ClearControls();
            }            
        }

        private async void btnSaveContact_Click(object sender, EventArgs e)
        {
            DashboardForm.Instance.DisplayLoader();
            this.Enabled = false;

            //We have to update a contact if it is supplied
            if (Contact != null)
            {
                Contact.Name = txtName.Text.Trim();
                Contact.Numbers = txtNumbers.Lines.Select(x => !string.IsNullOrEmpty(x.Trim().ToPhoneNumber()) ? new Number() { Value = x.Trim().ToPhoneNumber() } : null).Where(x => x != null).ToList();

                var validationMessage = ValidateContact(Contact);

                if (string.IsNullOrEmpty(validationMessage))
                {
                    if (await ContactsService.Instance.UpdateContact(Contact))
                    {
                        ClearControls();

                        DashboardForm.Instance.ShowSuccessMessage(Messages.ContactUpdated);
                    }
                    else
                    {
                        DashboardForm.Instance.ShowErrorMessage(Messages.ContactNotUpdated);
                    }
                }
                else
                {
                    DashboardForm.Instance.ShowErrorMessage(validationMessage);
                }
            }
            else //no contact supplied so lets add a new one
            {
                Contact newContact = new Contact();

                newContact.Name = txtName.Text.Trim();
                newContact.Numbers = txtNumbers.Lines.Select(x => !string.IsNullOrEmpty(x.Trim().ToPhoneNumber()) ? new Number() { Value = x.Trim().ToPhoneNumber() } : null).Where(x => x != null).ToList();
                
                var validationMessage = ValidateContact(newContact);

                if (string.IsNullOrEmpty(validationMessage))
                {
                    if (await ContactsService.Instance.AddNewContact(newContact))
                    {
                        ClearControls();

                        DashboardForm.Instance.ShowSuccessMessage(Messages.ContactAdded);
                    }
                    else
                    {
                        DashboardForm.Instance.ShowErrorMessage(Messages.ContactNotSaved);
                    }
                }
                else
                {
                    Contact = null;
                    DashboardForm.Instance.ShowErrorMessage(validationMessage);
                }
            }

            this.Enabled = true;
            DashboardForm.Instance.DisplayLoader(false);

        }

        private void ClearControls()
        {
            Contact = null;

            txtName.Clear();
            txtNumbers.Clear();
        }
        
        private string ValidateContact(Contact contact)
        {
            //if (string.IsNullOrEmpty(contact.Name))
            //{
            //    return Messages.ContactNameNeeded;
            //}
            if (contact.Numbers == null || contact.Numbers.Count <= 0)
            {
                return Messages.ContactNumbersNeeded;
            }
            else
            {
                foreach (var number in contact.Numbers)
                {
                    if (string.IsNullOrEmpty(number.Value))
                    {
                        return Messages.ContactNumbersNeeded;
                    }
                }

                return string.Empty;
            }
        }

        private void btnContacts_Click(object sender, EventArgs e)
        {
            AllContactsUserControl.Instance.Load(Clear: true);
        }

        private async void btnDelete_Click(object sender, EventArgs e)
        {
            if (DashboardForm.Instance.ShowConfirmationMessage(Messages.ContactConfirmDelete) == DialogResult.OK)
            {
                DashboardForm.Instance.DisplayLoader();
                if (await ContactsService.Instance.DeleteContact(Contact))
                {
                    DashboardForm.Instance.DisplayLoader(false);

                    //contact has been deleted. Refresh the Control
                    Load(null);
                }
                else
                {
                    DashboardForm.Instance.DisplayLoader(false);
                    DashboardForm.Instance.ShowErrorMessage(Messages.ContactNotDeleted);
                }
            }
        }
    }
}
