﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nuntius.Entities;
using Nuntius.Data;
using Nuntius.App.UI.Forms;
using Nuntius.Globals;
using Nuntius.App.UI.UserControls.Broadcasts;
using Nuntius.App.UI.UserControls.BaseControls;
using Nuntius.App.Code;
using Nuntius.Services;

namespace Nuntius.App.UI.UserControls.Contacts
{
    public partial class ContactsListUserControl : NuntiusControl
    {
        #region Define as Singleton
        private static ContactsListUserControl _Instance;

        public static ContactsListUserControl Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new ContactsListUserControl();
                }

                return (_Instance);
            }
        }

        private ContactsListUserControl()
        {
            InitializeComponent();
        }
        #endregion

        int PageNumber = 1;
        int TotalRecordsCount = 0;

        List<Contact> Contacts = null;

        public new void Load(List<Contact> contacts)
        {
            DashboardForm.Instance.DisplayLoader();
            this.Enabled = false;

            this.Contacts = contacts;
            if (Contacts != null && Contacts.Count > 0)
            {
                PageNumber = 1;
                TotalRecordsCount = contacts.Count;

                DisplayData();
                btnSave.Enabled = true;
            }
            else
            {
                PageNumber = 1;
                TotalRecordsCount = 0;

                dgvContacts.Rows.Clear();

                ValidatePagination();
                btnSave.Enabled = false;
            }

            this.Enabled = true;                        
            DashboardForm.Instance.DisplayLoader(false);
        }
        
        private void btnNext_Click(object sender, EventArgs e)
        {
            PageNumber = PageNumber + 1;

            DisplayData();
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            PageNumber = PageNumber - 1;

            DisplayData();
        }

        private void DisplayData()
        {
            var pageContacts = Contacts.Skip((PageNumber - 1) * Configurations.SinglePageRecordsCount).Take(Configurations.SinglePageRecordsCount).ToList();

            dgvContacts.Rows.Clear();
            foreach (var contact in pageContacts)
            {
                dgvContacts.Rows.Add(contact.Name, contact.Numbers.JoinNumbers(", "));
            }

            ValidatePagination();
        }

        private void ValidatePagination()
        {
            var TotalPages = Math.Ceiling((decimal)TotalRecordsCount / Configurations.SinglePageRecordsCount);

            if (PageNumber <= 1)
            {
                btnPrevious.Enabled = false;
            }
            else if(PageNumber <= TotalPages)
            {
                btnPrevious.Enabled = true;
            }

            if (PageNumber >= TotalPages)
            {
                btnNext.Enabled = false;
            }
            else
            {
                btnNext.Enabled = true;
            }
        }
        
        private void btnNew_Click(object sender, EventArgs e)
        {
            ContactUserControl.Instance.Load(null);
        }

        private void dgvContacts_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 &&
                e.ColumnIndex == dgvContacts.Columns["colDelete"].Index)
            {
                if (DashboardForm.Instance.ShowConfirmationMessage(Messages.ContactConfirmDelete) == DialogResult.OK)
                {
                    dgvContacts.Rows.RemoveAt(e.RowIndex);
                }
            }
        }

        private void dgvContacts_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                dgvContacts_CellContentClick(dgvContacts, new DataGridViewCellEventArgs(dgvContacts.CurrentCell.ColumnIndex, dgvContacts.CurrentCell.RowIndex));
            }
        }

        private async void btnSave_Click(object sender, EventArgs e)
        {
            if(Contacts != null && Contacts.Count > 0)
            {
                DashboardForm.Instance.DisplayLoader();
                this.Enabled = false;

                if(await ContactsService.Instance.AddNewContacts(Contacts))
                {
                    DashboardForm.Instance.ShowSuccessMessage(Messages.ContactSavedSuccess);

                    Load(null);
                }

                this.Enabled = true;
                DashboardForm.Instance.DisplayLoader(false);
            }
            else
            {
                DashboardForm.Instance.ShowErrorMessage(Messages.NoContactsToSave);
            }
        }
    }
}
