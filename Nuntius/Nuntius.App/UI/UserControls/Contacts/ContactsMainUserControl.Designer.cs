﻿namespace Nuntius.App.UI.UserControls.Contacts
{
    partial class ContactsMainUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.UserControlMenuFlowPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.btnAllContacts = new System.Windows.Forms.Button();
            this.btnNewContact = new System.Windows.Forms.Button();
            this.btnImportContacts = new System.Windows.Forms.Button();
            this.btnExportContacts = new System.Windows.Forms.Button();
            this.lblControlHeader = new System.Windows.Forms.Label();
            this.UserControlMenuFlowPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // UserControlMenuFlowPanel
            // 
            this.UserControlMenuFlowPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.UserControlMenuFlowPanel.Controls.Add(this.btnAllContacts);
            this.UserControlMenuFlowPanel.Controls.Add(this.btnNewContact);
            this.UserControlMenuFlowPanel.Controls.Add(this.btnImportContacts);
            this.UserControlMenuFlowPanel.Controls.Add(this.btnExportContacts);
            this.UserControlMenuFlowPanel.Location = new System.Drawing.Point(0, 37);
            this.UserControlMenuFlowPanel.Margin = new System.Windows.Forms.Padding(0);
            this.UserControlMenuFlowPanel.Name = "UserControlMenuFlowPanel";
            this.UserControlMenuFlowPanel.Padding = new System.Windows.Forms.Padding(5);
            this.UserControlMenuFlowPanel.Size = new System.Drawing.Size(791, 586);
            this.UserControlMenuFlowPanel.TabIndex = 7;
            // 
            // btnAllContacts
            // 
            this.btnAllContacts.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.btnAllContacts.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAllContacts.FlatAppearance.BorderSize = 0;
            this.btnAllContacts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAllContacts.Font = new System.Drawing.Font("Bahnschrift SemiBold", 10F, System.Drawing.FontStyle.Bold);
            this.btnAllContacts.Image = global::Nuntius.App.Properties.Resources.contacts_white_45;
            this.btnAllContacts.Location = new System.Drawing.Point(10, 10);
            this.btnAllContacts.Margin = new System.Windows.Forms.Padding(5);
            this.btnAllContacts.Name = "btnAllContacts";
            this.btnAllContacts.Size = new System.Drawing.Size(192, 85);
            this.btnAllContacts.TabIndex = 0;
            this.btnAllContacts.Text = "All Contacts";
            this.btnAllContacts.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnAllContacts.UseVisualStyleBackColor = false;
            this.btnAllContacts.Click += new System.EventHandler(this.btnAllContacts_Click);
            // 
            // btnNewContact
            // 
            this.btnNewContact.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.btnNewContact.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNewContact.FlatAppearance.BorderSize = 0;
            this.btnNewContact.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNewContact.Font = new System.Drawing.Font("Bahnschrift SemiBold", 10F, System.Drawing.FontStyle.Bold);
            this.btnNewContact.Image = global::Nuntius.App.Properties.Resources.new_contact_white_45;
            this.btnNewContact.Location = new System.Drawing.Point(212, 10);
            this.btnNewContact.Margin = new System.Windows.Forms.Padding(5);
            this.btnNewContact.Name = "btnNewContact";
            this.btnNewContact.Size = new System.Drawing.Size(192, 85);
            this.btnNewContact.TabIndex = 1;
            this.btnNewContact.Text = "New Contact";
            this.btnNewContact.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnNewContact.UseVisualStyleBackColor = false;
            this.btnNewContact.Click += new System.EventHandler(this.btnNewContact_Click);
            // 
            // btnImportContacts
            // 
            this.btnImportContacts.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.btnImportContacts.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnImportContacts.FlatAppearance.BorderSize = 0;
            this.btnImportContacts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImportContacts.Font = new System.Drawing.Font("Bahnschrift SemiBold", 10F, System.Drawing.FontStyle.Bold);
            this.btnImportContacts.Image = global::Nuntius.App.Properties.Resources.import_white_45;
            this.btnImportContacts.Location = new System.Drawing.Point(414, 10);
            this.btnImportContacts.Margin = new System.Windows.Forms.Padding(5);
            this.btnImportContacts.Name = "btnImportContacts";
            this.btnImportContacts.Size = new System.Drawing.Size(192, 85);
            this.btnImportContacts.TabIndex = 2;
            this.btnImportContacts.Text = "Import Contacts";
            this.btnImportContacts.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnImportContacts.UseVisualStyleBackColor = false;
            this.btnImportContacts.Click += new System.EventHandler(this.btnImportContacts_Click);
            // 
            // btnExportContacts
            // 
            this.btnExportContacts.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.btnExportContacts.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExportContacts.FlatAppearance.BorderSize = 0;
            this.btnExportContacts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExportContacts.Font = new System.Drawing.Font("Bahnschrift SemiBold", 10F, System.Drawing.FontStyle.Bold);
            this.btnExportContacts.Image = global::Nuntius.App.Properties.Resources.export_white_45;
            this.btnExportContacts.Location = new System.Drawing.Point(10, 105);
            this.btnExportContacts.Margin = new System.Windows.Forms.Padding(5);
            this.btnExportContacts.Name = "btnExportContacts";
            this.btnExportContacts.Size = new System.Drawing.Size(192, 85);
            this.btnExportContacts.TabIndex = 3;
            this.btnExportContacts.Text = "Export Contacts";
            this.btnExportContacts.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnExportContacts.UseVisualStyleBackColor = false;
            this.btnExportContacts.Click += new System.EventHandler(this.btnExportContacts_Click);
            // 
            // lblControlHeader
            // 
            this.lblControlHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblControlHeader.BackColor = System.Drawing.Color.Transparent;
            this.lblControlHeader.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblControlHeader.Font = new System.Drawing.Font("Bahnschrift SemiBold", 13F, System.Drawing.FontStyle.Bold);
            this.lblControlHeader.Image = global::Nuntius.App.Properties.Resources.contacts_white_45;
            this.lblControlHeader.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblControlHeader.Location = new System.Drawing.Point(0, 0);
            this.lblControlHeader.Margin = new System.Windows.Forms.Padding(0);
            this.lblControlHeader.Name = "lblControlHeader";
            this.lblControlHeader.Size = new System.Drawing.Size(791, 37);
            this.lblControlHeader.TabIndex = 6;
            this.lblControlHeader.Text = "Contacts";
            this.lblControlHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ContactsMainUserControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(155)))), ((int)(((byte)(229)))));
            this.Controls.Add(this.UserControlMenuFlowPanel);
            this.Controls.Add(this.lblControlHeader);
            this.Font = new System.Drawing.Font("Bahnschrift", 9F);
            this.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.Name = "ContactsMainUserControl";
            this.Size = new System.Drawing.Size(791, 623);
            this.UserControlMenuFlowPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblControlHeader;
        private System.Windows.Forms.FlowLayoutPanel UserControlMenuFlowPanel;
        private System.Windows.Forms.Button btnAllContacts;
        private System.Windows.Forms.Button btnNewContact;
        private System.Windows.Forms.Button btnImportContacts;
        private System.Windows.Forms.Button btnExportContacts;
    }
}
