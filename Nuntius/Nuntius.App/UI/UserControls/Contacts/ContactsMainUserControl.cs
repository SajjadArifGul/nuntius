﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nuntius.App.UI.Forms;
using Nuntius.App.Code;
using Nuntius.App.UI.UserControls.BaseControls;

namespace Nuntius.App.UI.UserControls.Contacts
{
    public partial class ContactsMainUserControl : NuntiusControl
    {
        #region Define as Singleton
        private static ContactsMainUserControl _Instance;

        public static ContactsMainUserControl Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new ContactsMainUserControl();
                }

                return (_Instance);
            }
        }

        private ContactsMainUserControl()
        {
            InitializeComponent();
        }
        #endregion
        
        private void btnNewContact_Click(object sender, EventArgs e)
        {
            ContactUserControl.Instance.Load(null);
        }

        private void btnAllContacts_Click(object sender, EventArgs e)
        {
            AllContactsUserControl.Instance.Load(Clear: true);
        }

        private void btnImportContacts_Click(object sender, EventArgs e)
        {
            ImportContactsUserControl.Instance.Load();
        }

        private void btnExportContacts_Click(object sender, EventArgs e)
        {
            ExportContactsUserControl.Instance.Load();
        }
    }
}
