﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nuntius.Entities;
using Nuntius.Data;
using Nuntius.App.UI.Forms;
using Nuntius.Globals;
using Nuntius.App.UI.UserControls.BaseControls;
using Nuntius.Services;
using System.IO;
using System.Threading;

namespace Nuntius.App.UI.UserControls.Contacts
{
    public partial class ExportContactsUserControl : NuntiusControl
    {
        #region Define as Singleton
        private static ExportContactsUserControl _Instance;

        public static ExportContactsUserControl Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new ExportContactsUserControl();
                }

                return (_Instance);
            }
        }

        private ExportContactsUserControl()
        {
            InitializeComponent();
        }
        #endregion

        List<Contact> contacts;

        public new void Load()
        {
            base.Load();
            lblFoundContactsCount.Text = string.Empty;
            ReadyAllContacts();
        }

        private async void ReadyAllContacts()
        {
            DashboardForm.Instance.DisplayLoader(true);
            this.Enabled = false;

            contacts = await ContactsService.Instance.SearchContacts(string.Empty);

            if (contacts != null && contacts.Count > 0)
            {
                lblFoundContactsCount.Text = string.Format(Messages.NumberOfContacts, contacts.Count);

                btnExportContacts.Enabled = true;
            }
            else
            {
                btnExportContacts.Enabled = false;
            }

            this.Enabled = true;
            DashboardForm.Instance.DisplayLoader(false);
        }

        private async void btnExportContacts_Click(object sender, EventArgs e)
        {
            if (contacts != null && contacts.Count > 0)
            {
                SaveFileDialog sfd = new SaveFileDialog();

                sfd.Title = "Save Contacts File";
                sfd.DefaultExt = "txt";
                sfd.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";

                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    var fileName = sfd.FileName;

                    DashboardForm.Instance.DisplayLoader(true);
                    this.Enabled = false;

                    try
                    {
                        var contactsText = await ContactsService.Instance.ContactsToText(contacts);

                        using (StreamWriter outputFile = new StreamWriter(sfd.FileName))
                        {
                            await outputFile.WriteAsync(contactsText);
                        }

                        DashboardForm.Instance.ShowSuccessMessage(Messages.FileSaved);

                        this.Enabled = true;
                        DashboardForm.Instance.DisplayLoader(false);
                    }
                    catch
                    {
                        this.Enabled = true;
                        DashboardForm.Instance.DisplayLoader(false);

                        DashboardForm.Instance.ShowErrorMessage(Messages.UnableToWriteFile);
                    }
                }
            }
        }
    }
}
