﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nuntius.Entities;
using Nuntius.Data;
using Nuntius.App.UI.Forms;
using Nuntius.Globals;
using Nuntius.App.UI.UserControls.BaseControls;
using Nuntius.Services;
using System.IO;
using System.Threading;

namespace Nuntius.App.UI.UserControls.Contacts
{
    public partial class ImportContactsUserControl : NuntiusControl
    {
        #region Define as Singleton
        private static ImportContactsUserControl _Instance;

        public static ImportContactsUserControl Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new ImportContactsUserControl();
                }

                return (_Instance);
            }
        }

        private ImportContactsUserControl()
        {
            InitializeComponent();
        }
        #endregion

        public new void Load()
        {
            base.Load();

            ClearControls();
        }

        private void ClearControls()
        {
            txtFile.Clear();
            lblFoundContactsCount.Text = string.Empty;
            btnImportContacts.Enabled = false;
            ContactsListPanel.Controls.Clear();
        }

        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            txtFile.Text = string.Empty;
            btnImportContacts.Enabled = false;

            OpenFileDialog ofd = new OpenFileDialog();

            ofd.Title = "Select Contact File";
            ofd.CheckFileExists = true;
            ofd.CheckPathExists = true;
            ofd.DefaultExt = "txt";
            ofd.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            ofd.ReadOnlyChecked = true;
            ofd.ShowReadOnly = true;

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                txtFile.Text = ofd.FileName;
                btnImportContacts.Enabled = true;
            }
        }

        private async void btnImportContacts_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtFile.Text))
            {
                List<Contact> contacts = null;

                DashboardForm.Instance.DisplayLoader(true);
                this.Enabled = false;

                try
                {
                    contacts = await ContactsService.Instance.TextToContacts(File.ReadAllLines(txtFile.Text));
                }
                catch
                {
                    DashboardForm.Instance.ShowErrorMessage(Messages.UnableToReadFile);
                }
                
                this.Enabled = true;
                DashboardForm.Instance.DisplayLoader(false);

                if (contacts != null)
                {
                    lblFoundContactsCount.Text = string.Format(Messages.FoundContactsInFile, contacts.Count);

                    ContactsListPanel.Controls.Add(ContactsListUserControl.Instance);
                    ContactsListUserControl.Instance.Dock = DockStyle.Fill;
                    ContactsListUserControl.Instance.BringToFront();

                    ContactsListUserControl.Instance.Load(contacts);
                }
                else
                {
                    DashboardForm.Instance.ShowErrorMessage(Messages.NoFormmatedContactsFound);
                }
            }
            else
            {
                ClearControls();
            }
        }
    }
}
