﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nuntius.Entities;
using Nuntius.Data;
using Nuntius.App.UI.Forms;
using Nuntius.Globals;
using Nuntius.App.UI.UserControls.Broadcasts;
using Nuntius.App.UI.UserControls.BaseControls;
using Nuntius.App.Code;
using Nuntius.Services;

namespace Nuntius.App.UI.UserControls.Groups
{
    public partial class AllGroupsUserControl : NuntiusControl
    {
        #region Define as Singleton
        private static AllGroupsUserControl _Instance;

        public static AllGroupsUserControl Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new AllGroupsUserControl();
                }

                return (_Instance);
            }
        }

        private AllGroupsUserControl()
        {
            InitializeComponent();
        }
        #endregion

        int PageNumber = 1;
        int TotalRecordsCount = 0;

        public new async void Load(bool Clear = false)
        {
            base.Load();

            DashboardForm.Instance.DisplayLoader();
            this.Enabled = false;
            
            if(Clear)
            {
                ClearControls();
            }
            
            var groups = await GroupsService.Instance.SearchGroups(txtSearch.Text.Trim(), PageNumber, Configurations.SinglePageRecordsCount);
            TotalRecordsCount = await GroupsService.Instance.GetGroupsCount(txtSearch.Text.Trim());
            
            dgvGroups.DataSource = groups.Select(x=> new { ID = x.ID, Name = x.Name.IfNullOrEmptyShowAlternative("-"), Description = x.Description.IfNullOrEmptyShowAlternative("-"), Contacts = x.GroupContacts.Count }).ToList();

            lblFoundGroupsCount.Text = string.Format(Messages.GroupsDisplayLabel, TotalRecordsCount == 0 ? TotalRecordsCount : ((PageNumber - 1) * Configurations.SinglePageRecordsCount) + 1, TotalRecordsCount > Configurations.SinglePageRecordsCount ? ((PageNumber - 1) * Configurations.SinglePageRecordsCount) + Configurations.SinglePageRecordsCount : TotalRecordsCount, TotalRecordsCount);

            this.Enabled = true;

            ValidatePagination();

            DashboardForm.Instance.DisplayLoader(false);

            //DashboardForm.Instance.AcceptButton = btnSearch;
            //btnSearch.Focus();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            PageNumber = PageNumber + 1;

            Load();
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            PageNumber--;

            Load();
        }

        private void ValidatePagination()
        {
            var TotalPages = Math.Ceiling((decimal)TotalRecordsCount / Configurations.SinglePageRecordsCount);

            if (PageNumber <= 1)
            {
                btnPrevious.Enabled = false;
            }
            else if(PageNumber <= TotalPages)
            {
                btnPrevious.Enabled = true;
            }

            if (PageNumber >= TotalPages)
            {
                btnNext.Enabled = false;
            }
            else
            {
                btnNext.Enabled = true;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            PageNumber = 1;

            Load();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            PageNumber = 1;

            Load(true);
        }

        private void ClearControls()
        {
            PageNumber = 1;
            txtSearch.Clear();
            lblFoundGroupsCount.Text = string.Empty;
        }
        
        private void btnNew_Click(object sender, EventArgs e)
        {
            GroupUserControl.Instance.Load(null);
        }

        private async void dgvGroups_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 &&
                (e.ColumnIndex == dgvGroups.Columns["colMessage"].Index ||
                e.ColumnIndex == dgvGroups.Columns["colDetails"].Index ||
                e.ColumnIndex == dgvGroups.Columns["colDelete"].Index))
            {
                DashboardForm.Instance.DisplayLoader();
                this.Enabled = false;

                int groupID = Convert.ToInt32(dgvGroups.Rows[e.RowIndex].Cells["colID"].Value);
                var group = await GroupsService.Instance.GetGroupByID(groupID);

                this.Enabled = true;
                DashboardForm.Instance.DisplayLoader(false);
                
                if (e.ColumnIndex == dgvGroups.Columns["colMessage"].Index)
                {
                    NewBroadcastUserControl.Instance.Load(group.GroupContacts.Select(x=>x.Contact).ToList());
                }
                else if (e.ColumnIndex == dgvGroups.Columns["colDetails"].Index)
                {
                    GroupUserControl.Instance.Load(group);
                }
                else if (e.ColumnIndex == dgvGroups.Columns["colDelete"].Index)
                {
                    if(DashboardForm.Instance.ShowConfirmationMessage(Messages.GroupConfirmDelete) == DialogResult.OK)
                    {
                        DashboardForm.Instance.DisplayLoader();

                        if (await GroupsService.Instance.DeleteGroup(group))
                        {
                            DashboardForm.Instance.DisplayLoader(false);

                            PageNumber = 1;
                            Load(true);
                        }
                        else
                        {
                            DashboardForm.Instance.DisplayLoader(false);
                            DashboardForm.Instance.ShowErrorMessage(Messages.GroupNotDeleted);
                        }
                    }
                }
            }
        }

        private void dgvGroups_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                dgvGroups_CellContentClick(dgvGroups, new DataGridViewCellEventArgs(dgvGroups.CurrentCell.ColumnIndex, dgvGroups.CurrentCell.RowIndex));
            }
        }
    }
}
