﻿namespace Nuntius.App.UI.UserControls.Groups
{
    partial class GroupUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblControlHeader = new System.Windows.Forms.Label();
            this.ContainerPanel = new System.Windows.Forms.Panel();
            this.lbGroupContactsCount = new System.Windows.Forms.Label();
            this.rtxtDescription = new System.Windows.Forms.RichTextBox();
            this.lbGroupContacts = new System.Windows.Forms.ListBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lbSearchContacts = new System.Windows.Forms.ListBox();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnGroups = new System.Windows.Forms.Button();
            this.btnSaveContact = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblResultsCounts = new System.Windows.Forms.Label();
            this.ContainerPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblControlHeader
            // 
            this.lblControlHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblControlHeader.BackColor = System.Drawing.Color.Transparent;
            this.lblControlHeader.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblControlHeader.Font = new System.Drawing.Font("Bahnschrift SemiBold", 13F, System.Drawing.FontStyle.Bold);
            this.lblControlHeader.Image = global::Nuntius.App.Properties.Resources.new_group_white_45;
            this.lblControlHeader.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblControlHeader.Location = new System.Drawing.Point(0, 0);
            this.lblControlHeader.Margin = new System.Windows.Forms.Padding(0);
            this.lblControlHeader.Name = "lblControlHeader";
            this.lblControlHeader.Size = new System.Drawing.Size(791, 37);
            this.lblControlHeader.TabIndex = 6;
            this.lblControlHeader.Text = "New Group";
            this.lblControlHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ContainerPanel
            // 
            this.ContainerPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ContainerPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(155)))), ((int)(((byte)(229)))));
            this.ContainerPanel.Controls.Add(this.lblResultsCounts);
            this.ContainerPanel.Controls.Add(this.lbGroupContactsCount);
            this.ContainerPanel.Controls.Add(this.rtxtDescription);
            this.ContainerPanel.Controls.Add(this.lbGroupContacts);
            this.ContainerPanel.Controls.Add(this.label8);
            this.ContainerPanel.Controls.Add(this.lbSearchContacts);
            this.ContainerPanel.Controls.Add(this.txtSearch);
            this.ContainerPanel.Controls.Add(this.btnSearch);
            this.ContainerPanel.Controls.Add(this.label7);
            this.ContainerPanel.Controls.Add(this.label6);
            this.ContainerPanel.Controls.Add(this.label5);
            this.ContainerPanel.Controls.Add(this.btnGroups);
            this.ContainerPanel.Controls.Add(this.btnSaveContact);
            this.ContainerPanel.Controls.Add(this.label2);
            this.ContainerPanel.Controls.Add(this.label1);
            this.ContainerPanel.Controls.Add(this.txtName);
            this.ContainerPanel.Location = new System.Drawing.Point(0, 37);
            this.ContainerPanel.Margin = new System.Windows.Forms.Padding(0);
            this.ContainerPanel.Name = "ContainerPanel";
            this.ContainerPanel.Size = new System.Drawing.Size(791, 586);
            this.ContainerPanel.TabIndex = 7;
            // 
            // lbGroupContactsCount
            // 
            this.lbGroupContactsCount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbGroupContactsCount.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Italic);
            this.lbGroupContactsCount.Location = new System.Drawing.Point(118, 505);
            this.lbGroupContactsCount.Name = "lbGroupContactsCount";
            this.lbGroupContactsCount.Size = new System.Drawing.Size(594, 24);
            this.lbGroupContactsCount.TabIndex = 27;
            this.lbGroupContactsCount.Text = "30 Members";
            // 
            // rtxtDescription
            // 
            this.rtxtDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtxtDescription.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtxtDescription.Font = new System.Drawing.Font("Bahnschrift", 14F);
            this.rtxtDescription.Location = new System.Drawing.Point(118, 45);
            this.rtxtDescription.Name = "rtxtDescription";
            this.rtxtDescription.Size = new System.Drawing.Size(594, 65);
            this.rtxtDescription.TabIndex = 26;
            this.rtxtDescription.Text = "";
            // 
            // lbGroupContacts
            // 
            this.lbGroupContacts.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbGroupContacts.Font = new System.Drawing.Font("Bahnschrift", 14F);
            this.lbGroupContacts.FormattingEnabled = true;
            this.lbGroupContacts.IntegralHeight = false;
            this.lbGroupContacts.ItemHeight = 23;
            this.lbGroupContacts.Location = new System.Drawing.Point(118, 302);
            this.lbGroupContacts.Name = "lbGroupContacts";
            this.lbGroupContacts.Size = new System.Drawing.Size(594, 198);
            this.lbGroupContacts.TabIndex = 25;
            this.lbGroupContacts.DoubleClick += new System.EventHandler(this.lbGroupContacts_DoubleClick);
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(12, 302);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 22);
            this.label8.TabIndex = 24;
            this.label8.Text = "Group Contacts";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbSearchContacts
            // 
            this.lbSearchContacts.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbSearchContacts.Font = new System.Drawing.Font("Bahnschrift", 14F);
            this.lbSearchContacts.FormattingEnabled = true;
            this.lbSearchContacts.IntegralHeight = false;
            this.lbSearchContacts.ItemHeight = 23;
            this.lbSearchContacts.Location = new System.Drawing.Point(118, 180);
            this.lbSearchContacts.Name = "lbSearchContacts";
            this.lbSearchContacts.Size = new System.Drawing.Size(594, 116);
            this.lbSearchContacts.TabIndex = 23;
            this.lbSearchContacts.DoubleClick += new System.EventHandler(this.lbSearchContacts_DoubleClick);
            // 
            // txtSearch
            // 
            this.txtSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearch.Font = new System.Drawing.Font("Bahnschrift", 14F);
            this.txtSearch.Location = new System.Drawing.Point(118, 144);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(489, 30);
            this.txtSearch.TabIndex = 22;
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.btnSearch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSearch.FlatAppearance.BorderSize = 0;
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.Image = global::Nuntius.App.Properties.Resources.contact_search_white_45;
            this.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearch.Location = new System.Drawing.Point(612, 144);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Padding = new System.Windows.Forms.Padding(1);
            this.btnSearch.Size = new System.Drawing.Size(100, 30);
            this.btnSearch.TabIndex = 21;
            this.btnSearch.Text = "Search";
            this.btnSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(12, 144);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 30);
            this.label7.TabIndex = 20;
            this.label7.Text = "Search";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Italic);
            this.label6.Location = new System.Drawing.Point(118, 113);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(594, 28);
            this.label6.TabIndex = 18;
            this.label6.Text = "Add Contacts to this Group";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(12, 45);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 28);
            this.label5.TabIndex = 17;
            this.label5.Text = "Description";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnGroups
            // 
            this.btnGroups.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGroups.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.btnGroups.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGroups.FlatAppearance.BorderSize = 0;
            this.btnGroups.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGroups.Image = global::Nuntius.App.Properties.Resources.groups_white_45;
            this.btnGroups.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGroups.Location = new System.Drawing.Point(506, 530);
            this.btnGroups.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.btnGroups.Name = "btnGroups";
            this.btnGroups.Padding = new System.Windows.Forms.Padding(1);
            this.btnGroups.Size = new System.Drawing.Size(100, 30);
            this.btnGroups.TabIndex = 15;
            this.btnGroups.Text = "Groups";
            this.btnGroups.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGroups.UseVisualStyleBackColor = false;
            this.btnGroups.Click += new System.EventHandler(this.btnContacts_Click);
            // 
            // btnSaveContact
            // 
            this.btnSaveContact.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveContact.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.btnSaveContact.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSaveContact.FlatAppearance.BorderSize = 0;
            this.btnSaveContact.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaveContact.Image = global::Nuntius.App.Properties.Resources.save_white_45;
            this.btnSaveContact.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSaveContact.Location = new System.Drawing.Point(612, 530);
            this.btnSaveContact.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.btnSaveContact.Name = "btnSaveContact";
            this.btnSaveContact.Padding = new System.Windows.Forms.Padding(1);
            this.btnSaveContact.Size = new System.Drawing.Size(100, 30);
            this.btnSaveContact.TabIndex = 10;
            this.btnSaveContact.Text = "Save";
            this.btnSaveContact.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSaveContact.UseVisualStyleBackColor = false;
            this.btnSaveContact.Click += new System.EventHandler(this.btnSaveContact_Click);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(12, 180);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 22);
            this.label2.TabIndex = 3;
            this.label2.Text = "Search Results";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 28);
            this.label1.TabIndex = 1;
            this.label1.Text = "Name";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtName
            // 
            this.txtName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtName.Font = new System.Drawing.Font("Bahnschrift", 14F);
            this.txtName.Location = new System.Drawing.Point(118, 9);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(594, 30);
            this.txtName.TabIndex = 0;
            // 
            // lblResultsCounts
            // 
            this.lblResultsCounts.Location = new System.Drawing.Point(12, 202);
            this.lblResultsCounts.Name = "lblResultsCounts";
            this.lblResultsCounts.Size = new System.Drawing.Size(100, 94);
            this.lblResultsCounts.TabIndex = 31;
            this.lblResultsCounts.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // GroupUserControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(155)))), ((int)(((byte)(229)))));
            this.Controls.Add(this.ContainerPanel);
            this.Controls.Add(this.lblControlHeader);
            this.Font = new System.Drawing.Font("Bahnschrift", 9F);
            this.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.Name = "GroupUserControl";
            this.Size = new System.Drawing.Size(791, 623);
            this.ContainerPanel.ResumeLayout(false);
            this.ContainerPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblControlHeader;
        private System.Windows.Forms.Panel ContainerPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSaveContact;
        private System.Windows.Forms.Button btnGroups;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ListBox lbGroupContacts;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ListBox lbSearchContacts;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.RichTextBox rtxtDescription;
        private System.Windows.Forms.Label lbGroupContactsCount;
        private System.Windows.Forms.Label lblResultsCounts;
    }
}
