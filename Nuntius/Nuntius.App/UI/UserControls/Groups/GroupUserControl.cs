﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nuntius.Entities;
using Nuntius.Data;
using Nuntius.App.UI.Forms;
using Nuntius.Globals;
using Nuntius.App.UI.UserControls.BaseControls;
using Nuntius.Services;

namespace Nuntius.App.UI.UserControls.Groups
{
    public partial class GroupUserControl : NuntiusControl
    {
        #region Define as Singleton
        private static GroupUserControl _Instance;

        public static GroupUserControl Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new GroupUserControl();
                }

                return (_Instance);
            }
        }

        private GroupUserControl()
        {
            InitializeComponent();
        }
        #endregion

        public Group Group { get; set; }

        public List<Contact> GroupContacts { get; set; }

        public new void Load(Group group)
        {
            base.Load();

            if (group != null)
            {
                this.Group = group;

                txtName.Text = group.Name;
                rtxtDescription.Text = group.Description;

                GroupContacts = group.GroupContacts.Select(x=>x.Contact).ToList();
                UpdateGroupContactsSource();
            }
            else
            {
                ClearControls();
            }            
        }

        private async void btnSaveContact_Click(object sender, EventArgs e)
        {
            DashboardForm.Instance.DisplayLoader();
            this.Enabled = false;

            //We have to update a contact if it is supplied
            if (Group != null)
            {
                Group.Name = txtName.Text;
                Group.Description = rtxtDescription.Text;
                Group.GroupContacts.AddRange(GroupContacts.Select(x=> new GroupContact() { ContactID = x.ID }).ToList());

                if (await GroupsService.Instance.UpdateGroup(Group))
                {
                    ClearControls();

                    DashboardForm.Instance.ShowSuccessMessage(Messages.GroupUpdated);
                }
                else
                {
                    DashboardForm.Instance.ShowErrorMessage(Messages.GroupNotUpdated);
                }
            }
            else //no contact supplied so lets add a new one
            {
                Group newGroup = new Group();

                newGroup.Name = txtName.Text;
                newGroup.Description = rtxtDescription.Text;
                newGroup.GroupContacts = GroupContacts.Select(x => new GroupContact() { ContactID = x.ID }).ToList();

                if (await GroupsService.Instance.AddNewGroup(newGroup))
                {
                    ClearControls();
                    
                    DashboardForm.Instance.ShowSuccessMessage(Messages.GroupAdded);
                }
                else
                {
                    DashboardForm.Instance.ShowErrorMessage(Messages.GroupNotSaved);
                }
            }

            this.Enabled = true;
            DashboardForm.Instance.DisplayLoader(false);

        }

        private void ClearControls()
        {
            Group = null;
            txtName.Clear();
            rtxtDescription.Clear();

            GroupContacts = new List<Contact>();
            UpdateGroupContactsSource();

            txtSearch.Clear();
            SearchContacts();
        }
        
        private void btnContacts_Click(object sender, EventArgs e)
        {
            AllGroupsUserControl.Instance.Load(true);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            SearchContacts();
        }
        
        private async void SearchContacts()
        {
            DashboardForm.Instance.DisplayLoader();
            this.Enabled = false;
            
            var contacts = string.IsNullOrEmpty(txtSearch.Text.Trim()) ? new List<Contact>() : await ContactsService.Instance.SearchContacts(txtSearch.Text.Trim());

            lbSearchContacts.DataSource = contacts;
            lbSearchContacts.DisplayMember = "DisplayName";
            lbSearchContacts.ValueMember = "ID";

            lblResultsCounts.Text = contacts != null && contacts.Count > 0 ? string.Format(Messages.NumberOfContacts, contacts.Count) : Messages.NoResults;

            this.Enabled = true;
            DashboardForm.Instance.DisplayLoader(false);
        }

        private void UpdateGroupContactsSource()
        {
            if (GroupContacts != null && GroupContacts.Count > 0)
            {
                var groupContactsSource = new BindingSource();
                groupContactsSource.DataSource = GroupContacts.OrderBy(x => x.Name).ToList();

                lbGroupContacts.DataSource = groupContactsSource;
                lbGroupContacts.ValueMember = "ID";
                lbGroupContacts.DisplayMember = "DisplayName";

                lbGroupContactsCount.Text = string.Format(Messages.GroupContactsCount, GroupContacts.Count);
            }
            else
            {
                lbGroupContacts.DataSource = null;
                lbGroupContactsCount.Text = string.Empty;
            }
        }

        private void lbSearchContacts_DoubleClick(object sender, EventArgs e)
        {
            Contact selectedContact = (Contact)lbSearchContacts.SelectedItem;

            if (selectedContact != null && !GroupContacts.Any(x=>x.ID == selectedContact.ID))
            {
                GroupContacts.Add(selectedContact);
                UpdateGroupContactsSource();
            }
        }
        private void lbGroupContacts_DoubleClick(object sender, EventArgs e)
        {
            Contact selectedContact = (Contact)lbGroupContacts.SelectedItem;

            if (selectedContact != null)
            {
                GroupContacts.Remove(selectedContact);
                UpdateGroupContactsSource();
            }
        }
    }
}
