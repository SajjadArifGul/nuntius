﻿namespace Nuntius.App.UI.UserControls.Groups
{
    partial class GroupsMainUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.UserControlMenuFlowPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.btnAllGroups = new System.Windows.Forms.Button();
            this.btnNewGroup = new System.Windows.Forms.Button();
            this.lblControlHeader = new System.Windows.Forms.Label();
            this.UserControlMenuFlowPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // UserControlMenuFlowPanel
            // 
            this.UserControlMenuFlowPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.UserControlMenuFlowPanel.Controls.Add(this.btnAllGroups);
            this.UserControlMenuFlowPanel.Controls.Add(this.btnNewGroup);
            this.UserControlMenuFlowPanel.Location = new System.Drawing.Point(0, 37);
            this.UserControlMenuFlowPanel.Margin = new System.Windows.Forms.Padding(0);
            this.UserControlMenuFlowPanel.Name = "UserControlMenuFlowPanel";
            this.UserControlMenuFlowPanel.Padding = new System.Windows.Forms.Padding(5);
            this.UserControlMenuFlowPanel.Size = new System.Drawing.Size(791, 586);
            this.UserControlMenuFlowPanel.TabIndex = 7;
            // 
            // btnAllGroups
            // 
            this.btnAllGroups.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.btnAllGroups.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAllGroups.FlatAppearance.BorderSize = 0;
            this.btnAllGroups.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAllGroups.Font = new System.Drawing.Font("Bahnschrift SemiBold", 10F, System.Drawing.FontStyle.Bold);
            this.btnAllGroups.Image = global::Nuntius.App.Properties.Resources.groups_white_45;
            this.btnAllGroups.Location = new System.Drawing.Point(10, 10);
            this.btnAllGroups.Margin = new System.Windows.Forms.Padding(5);
            this.btnAllGroups.Name = "btnAllGroups";
            this.btnAllGroups.Size = new System.Drawing.Size(192, 85);
            this.btnAllGroups.TabIndex = 0;
            this.btnAllGroups.Text = "All Groups";
            this.btnAllGroups.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnAllGroups.UseVisualStyleBackColor = false;
            this.btnAllGroups.Click += new System.EventHandler(this.btnAllGroups_Click);
            // 
            // btnNewGroup
            // 
            this.btnNewGroup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.btnNewGroup.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNewGroup.FlatAppearance.BorderSize = 0;
            this.btnNewGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNewGroup.Font = new System.Drawing.Font("Bahnschrift SemiBold", 10F, System.Drawing.FontStyle.Bold);
            this.btnNewGroup.Image = global::Nuntius.App.Properties.Resources.new_group_white_45;
            this.btnNewGroup.Location = new System.Drawing.Point(212, 10);
            this.btnNewGroup.Margin = new System.Windows.Forms.Padding(5);
            this.btnNewGroup.Name = "btnNewGroup";
            this.btnNewGroup.Size = new System.Drawing.Size(192, 85);
            this.btnNewGroup.TabIndex = 1;
            this.btnNewGroup.Text = "New Group";
            this.btnNewGroup.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnNewGroup.UseVisualStyleBackColor = false;
            this.btnNewGroup.Click += new System.EventHandler(this.btnNewGroup_Click);
            // 
            // lblControlHeader
            // 
            this.lblControlHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblControlHeader.BackColor = System.Drawing.Color.Transparent;
            this.lblControlHeader.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblControlHeader.Font = new System.Drawing.Font("Bahnschrift SemiBold", 13F, System.Drawing.FontStyle.Bold);
            this.lblControlHeader.Image = global::Nuntius.App.Properties.Resources.groups_white_45;
            this.lblControlHeader.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblControlHeader.Location = new System.Drawing.Point(0, 0);
            this.lblControlHeader.Margin = new System.Windows.Forms.Padding(0);
            this.lblControlHeader.Name = "lblControlHeader";
            this.lblControlHeader.Size = new System.Drawing.Size(791, 37);
            this.lblControlHeader.TabIndex = 6;
            this.lblControlHeader.Text = "Groups";
            this.lblControlHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GroupsMainUserControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(155)))), ((int)(((byte)(229)))));
            this.Controls.Add(this.UserControlMenuFlowPanel);
            this.Controls.Add(this.lblControlHeader);
            this.Font = new System.Drawing.Font("Bahnschrift", 9F);
            this.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.Name = "GroupsMainUserControl";
            this.Size = new System.Drawing.Size(791, 623);
            this.UserControlMenuFlowPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblControlHeader;
        private System.Windows.Forms.FlowLayoutPanel UserControlMenuFlowPanel;
        private System.Windows.Forms.Button btnAllGroups;
        private System.Windows.Forms.Button btnNewGroup;
    }
}
