﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nuntius.App.UI.Forms;
using Nuntius.App.UI.UserControls.BaseControls;

namespace Nuntius.App.UI.UserControls.Groups
{
    public partial class GroupsMainUserControl : NuntiusControl
    {
        #region Define as Singleton
        private static GroupsMainUserControl _Instance;

        public static GroupsMainUserControl Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new GroupsMainUserControl();
                }

                return (_Instance);
            }
        }

        private GroupsMainUserControl()
        {
            InitializeComponent();
        }
        #endregion

        private void btnAllGroups_Click(object sender, EventArgs e)
        {
            AllGroupsUserControl.Instance.Load(true);
        }

        private void btnNewGroup_Click(object sender, EventArgs e)
        {
            GroupUserControl.Instance.Load(null);
        }
    }
}
