﻿using System;
using System.Linq;
using System.Windows.Forms;
using Nuntius.Entities;
using Nuntius.App.UI.Forms;
using Nuntius.Globals;
using Nuntius.App.UI.UserControls.BaseControls;
using Nuntius.App.Code;
using Nuntius.Services;
using System.Diagnostics;

namespace Nuntius.App.UI.UserControls.Help
{
    public partial class ContactUsUserControl : NuntiusControl
    {
        #region Define as Singleton
        private static ContactUsUserControl _Instance;

        public static ContactUsUserControl Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new ContactUsUserControl();
                }

                return (_Instance);
            }
        }

        private ContactUsUserControl()
        {
            InitializeComponent();
        }
        #endregion
        
        public new void Load()
        {
            base.Load();

            DashboardForm.Instance.DisplayLoader();
            this.Enabled = false;

            txtEmail.Text = Configurations.ContactEmail.ToString();
            txtPhone.Text = Configurations.ContactPhone.ToString();
            txtWebsite.Text = Configurations.WebsiteURL.ToString();
            lblContactUs.Text = Configurations.ContactDetails.ToString();

            this.Enabled = true;
            DashboardForm.Instance.DisplayLoader(false);
        }
    }
}
