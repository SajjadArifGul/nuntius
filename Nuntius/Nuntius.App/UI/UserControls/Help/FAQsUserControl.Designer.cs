﻿namespace Nuntius.App.UI.UserControls.Help
{
    partial class FAQsUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblControlHeader = new System.Windows.Forms.Label();
            this.ContainerPanel = new System.Windows.Forms.Panel();
            this.lblFAQAnswer = new System.Windows.Forms.Label();
            this.cmbFAQs = new System.Windows.Forms.ComboBox();
            this.lblCustomFAQURL = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ContainerPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblControlHeader
            // 
            this.lblControlHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblControlHeader.BackColor = System.Drawing.Color.Transparent;
            this.lblControlHeader.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblControlHeader.Font = new System.Drawing.Font("Bahnschrift SemiBold", 13F, System.Drawing.FontStyle.Bold);
            this.lblControlHeader.Image = global::Nuntius.App.Properties.Resources.faqs_white_45;
            this.lblControlHeader.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblControlHeader.Location = new System.Drawing.Point(0, 0);
            this.lblControlHeader.Margin = new System.Windows.Forms.Padding(0);
            this.lblControlHeader.Name = "lblControlHeader";
            this.lblControlHeader.Size = new System.Drawing.Size(791, 37);
            this.lblControlHeader.TabIndex = 6;
            this.lblControlHeader.Text = "FAQs";
            this.lblControlHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ContainerPanel
            // 
            this.ContainerPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ContainerPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(155)))), ((int)(((byte)(229)))));
            this.ContainerPanel.Controls.Add(this.lblFAQAnswer);
            this.ContainerPanel.Controls.Add(this.cmbFAQs);
            this.ContainerPanel.Controls.Add(this.lblCustomFAQURL);
            this.ContainerPanel.Controls.Add(this.label1);
            this.ContainerPanel.Location = new System.Drawing.Point(0, 37);
            this.ContainerPanel.Margin = new System.Windows.Forms.Padding(0);
            this.ContainerPanel.Name = "ContainerPanel";
            this.ContainerPanel.Size = new System.Drawing.Size(791, 586);
            this.ContainerPanel.TabIndex = 7;
            // 
            // lblFAQAnswer
            // 
            this.lblFAQAnswer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFAQAnswer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(155)))), ((int)(((byte)(229)))));
            this.lblFAQAnswer.Font = new System.Drawing.Font("Bahnschrift", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFAQAnswer.Location = new System.Drawing.Point(118, 64);
            this.lblFAQAnswer.Name = "lblFAQAnswer";
            this.lblFAQAnswer.Size = new System.Drawing.Size(593, 486);
            this.lblFAQAnswer.TabIndex = 29;
            // 
            // cmbFAQs
            // 
            this.cmbFAQs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbFAQs.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFAQs.Font = new System.Drawing.Font("Bahnschrift", 14F);
            this.cmbFAQs.FormattingEnabled = true;
            this.cmbFAQs.Location = new System.Drawing.Point(118, 30);
            this.cmbFAQs.Name = "cmbFAQs";
            this.cmbFAQs.Size = new System.Drawing.Size(593, 31);
            this.cmbFAQs.TabIndex = 28;
            this.cmbFAQs.SelectedIndexChanged += new System.EventHandler(this.cmbFAQs_SelectedIndexChanged);
            // 
            // lblCustomFAQURL
            // 
            this.lblCustomFAQURL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCustomFAQURL.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblCustomFAQURL.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Italic);
            this.lblCustomFAQURL.Location = new System.Drawing.Point(120, 4);
            this.lblCustomFAQURL.Name = "lblCustomFAQURL";
            this.lblCustomFAQURL.Size = new System.Drawing.Size(593, 23);
            this.lblCustomFAQURL.TabIndex = 20;
            this.lblCustomFAQURL.Text = "Select questions from below list. If you dont find your question, contact us by c" +
    "licking here.";
            this.lblCustomFAQURL.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblCustomFAQURL.Click += new System.EventHandler(this.lblCustomFAQURL_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 30);
            this.label1.TabIndex = 1;
            this.label1.Text = "Questions";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FAQsUserControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(155)))), ((int)(((byte)(229)))));
            this.Controls.Add(this.ContainerPanel);
            this.Controls.Add(this.lblControlHeader);
            this.Font = new System.Drawing.Font("Bahnschrift", 9F);
            this.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.Name = "FAQsUserControl";
            this.Size = new System.Drawing.Size(791, 623);
            this.ContainerPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblControlHeader;
        private System.Windows.Forms.Panel ContainerPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblCustomFAQURL;
        private System.Windows.Forms.ComboBox cmbFAQs;
        private System.Windows.Forms.Label lblFAQAnswer;
    }
}
