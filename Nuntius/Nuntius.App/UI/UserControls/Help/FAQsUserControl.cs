﻿using System;
using System.Linq;
using System.Windows.Forms;
using Nuntius.Entities;
using Nuntius.App.UI.Forms;
using Nuntius.Globals;
using Nuntius.App.UI.UserControls.BaseControls;
using Nuntius.App.Code;
using Nuntius.Services;
using System.Diagnostics;

namespace Nuntius.App.UI.UserControls.Help
{
    public partial class FAQsUserControl : NuntiusControl
    {
        #region Define as Singleton
        private static FAQsUserControl _Instance;

        public static FAQsUserControl Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new FAQsUserControl();
                }

                return (_Instance);
            }
        }

        private FAQsUserControl()
        {
            InitializeComponent();
        }
        #endregion
        
        public async new void Load()
        {
            base.Load();

            DashboardForm.Instance.DisplayLoader();
            this.Enabled = false;
            
            var faqs = await FAQsService.Instance.GetFAQs();

            cmbFAQs.DataSource = faqs;
            cmbFAQs.DisplayMember = "Question";
            cmbFAQs.ValueMember = "ID";
            cmbFAQs.SelectedValue = -1;

            this.Enabled = true;
            DashboardForm.Instance.DisplayLoader(false);
        }

        private void cmbFAQs_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblFAQAnswer.Text = string.Empty;

            FAQ selectedFAQ = (FAQ)cmbFAQs.SelectedItem;

            if(selectedFAQ != null)
            {
                lblFAQAnswer.Text = string.Format("Q: {0}{1}A: {2}", selectedFAQ.Question, Environment.NewLine, selectedFAQ.Answer);
            }
        }

        private void lblCustomFAQURL_Click(object sender, EventArgs e)
        {
            Process.Start(Configurations.CustomFAQURL);
        }
    }
}
