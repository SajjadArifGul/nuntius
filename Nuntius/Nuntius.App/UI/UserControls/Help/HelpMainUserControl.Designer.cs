﻿namespace Nuntius.App.UI.UserControls.Help
{
    partial class HelpMainUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.UserControlMenuFlowPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.btnFAQs = new System.Windows.Forms.Button();
            this.btnContactUs = new System.Windows.Forms.Button();
            this.lblControlHeader = new System.Windows.Forms.Label();
            this.UserControlMenuFlowPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // UserControlMenuFlowPanel
            // 
            this.UserControlMenuFlowPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.UserControlMenuFlowPanel.Controls.Add(this.btnFAQs);
            this.UserControlMenuFlowPanel.Controls.Add(this.btnContactUs);
            this.UserControlMenuFlowPanel.Location = new System.Drawing.Point(0, 37);
            this.UserControlMenuFlowPanel.Margin = new System.Windows.Forms.Padding(0);
            this.UserControlMenuFlowPanel.Name = "UserControlMenuFlowPanel";
            this.UserControlMenuFlowPanel.Padding = new System.Windows.Forms.Padding(5);
            this.UserControlMenuFlowPanel.Size = new System.Drawing.Size(791, 586);
            this.UserControlMenuFlowPanel.TabIndex = 7;
            // 
            // btnFAQs
            // 
            this.btnFAQs.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.btnFAQs.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFAQs.FlatAppearance.BorderSize = 0;
            this.btnFAQs.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFAQs.Font = new System.Drawing.Font("Bahnschrift SemiBold", 10F, System.Drawing.FontStyle.Bold);
            this.btnFAQs.Image = global::Nuntius.App.Properties.Resources.faqs_white_45;
            this.btnFAQs.Location = new System.Drawing.Point(10, 10);
            this.btnFAQs.Margin = new System.Windows.Forms.Padding(5);
            this.btnFAQs.Name = "btnFAQs";
            this.btnFAQs.Size = new System.Drawing.Size(192, 85);
            this.btnFAQs.TabIndex = 0;
            this.btnFAQs.Text = "FAQs";
            this.btnFAQs.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnFAQs.UseVisualStyleBackColor = false;
            this.btnFAQs.Click += new System.EventHandler(this.btnFAQs_Click);
            // 
            // btnContactUs
            // 
            this.btnContactUs.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.btnContactUs.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnContactUs.FlatAppearance.BorderSize = 0;
            this.btnContactUs.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnContactUs.Font = new System.Drawing.Font("Bahnschrift SemiBold", 10F, System.Drawing.FontStyle.Bold);
            this.btnContactUs.Image = global::Nuntius.App.Properties.Resources.ask_developers_white_45;
            this.btnContactUs.Location = new System.Drawing.Point(212, 10);
            this.btnContactUs.Margin = new System.Windows.Forms.Padding(5);
            this.btnContactUs.Name = "btnContactUs";
            this.btnContactUs.Size = new System.Drawing.Size(192, 85);
            this.btnContactUs.TabIndex = 1;
            this.btnContactUs.Text = "Contact Us";
            this.btnContactUs.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnContactUs.UseVisualStyleBackColor = false;
            this.btnContactUs.Click += new System.EventHandler(this.btnContactUs_Click);
            // 
            // lblControlHeader
            // 
            this.lblControlHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblControlHeader.BackColor = System.Drawing.Color.Transparent;
            this.lblControlHeader.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblControlHeader.Font = new System.Drawing.Font("Bahnschrift SemiBold", 13F, System.Drawing.FontStyle.Bold);
            this.lblControlHeader.Image = global::Nuntius.App.Properties.Resources.help_white_45;
            this.lblControlHeader.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblControlHeader.Location = new System.Drawing.Point(0, 0);
            this.lblControlHeader.Margin = new System.Windows.Forms.Padding(0);
            this.lblControlHeader.Name = "lblControlHeader";
            this.lblControlHeader.Size = new System.Drawing.Size(791, 37);
            this.lblControlHeader.TabIndex = 6;
            this.lblControlHeader.Text = "Help";
            this.lblControlHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // HelpMainUserControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(155)))), ((int)(((byte)(229)))));
            this.Controls.Add(this.UserControlMenuFlowPanel);
            this.Controls.Add(this.lblControlHeader);
            this.Font = new System.Drawing.Font("Bahnschrift", 9F);
            this.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.Name = "HelpMainUserControl";
            this.Size = new System.Drawing.Size(791, 623);
            this.UserControlMenuFlowPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblControlHeader;
        private System.Windows.Forms.FlowLayoutPanel UserControlMenuFlowPanel;
        private System.Windows.Forms.Button btnFAQs;
        private System.Windows.Forms.Button btnContactUs;
    }
}
