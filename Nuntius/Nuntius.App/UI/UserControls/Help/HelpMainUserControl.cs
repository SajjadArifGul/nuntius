﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nuntius.App.UI.UserControls.BaseControls;

namespace Nuntius.App.UI.UserControls.Help
{
    public partial class HelpMainUserControl : NuntiusControl
    {
        #region Define as Singleton
        private static HelpMainUserControl _Instance;

        public static HelpMainUserControl Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new HelpMainUserControl();
                }

                return (_Instance);
            }
        }

        private HelpMainUserControl()
        {
            InitializeComponent();
        }
        #endregion

        private void btnFAQs_Click(object sender, EventArgs e)
        {
            FAQsUserControl.Instance.Load();
        }

        private void btnContactUs_Click(object sender, EventArgs e)
        {
            ContactUsUserControl.Instance.Load();
        }
    }
}
