﻿namespace Nuntius.App.UI.UserControls.Settings
{
    partial class GSMModemSettingsUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ContainerPanel = new System.Windows.Forms.Panel();
            this.lblPreferredBroadcaster = new System.Windows.Forms.Label();
            this.txtTimeout = new System.Windows.Forms.TextBox();
            this.lblTimeoutURL = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblBaudRateURL = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtBaudRate = new System.Windows.Forms.TextBox();
            this.lblComPortURL = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtComPort = new System.Windows.Forms.TextBox();
            this.grpTestMessageHolder = new System.Windows.Forms.GroupBox();
            this.lblTestMessageHeading = new System.Windows.Forms.Label();
            this.txtTestMessageNumber = new System.Windows.Forms.TextBox();
            this.btnChangeGSMStatus = new System.Windows.Forms.Button();
            this.btnSendTestMessage = new System.Windows.Forms.Button();
            this.btnReSearchComPort = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblControlHeader = new System.Windows.Forms.Label();
            this.ContainerPanel.SuspendLayout();
            this.grpTestMessageHolder.SuspendLayout();
            this.SuspendLayout();
            // 
            // ContainerPanel
            // 
            this.ContainerPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ContainerPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(155)))), ((int)(((byte)(229)))));
            this.ContainerPanel.Controls.Add(this.btnChangeGSMStatus);
            this.ContainerPanel.Controls.Add(this.grpTestMessageHolder);
            this.ContainerPanel.Controls.Add(this.btnReSearchComPort);
            this.ContainerPanel.Controls.Add(this.lblPreferredBroadcaster);
            this.ContainerPanel.Controls.Add(this.txtTimeout);
            this.ContainerPanel.Controls.Add(this.lblTimeoutURL);
            this.ContainerPanel.Controls.Add(this.label7);
            this.ContainerPanel.Controls.Add(this.lblBaudRateURL);
            this.ContainerPanel.Controls.Add(this.label5);
            this.ContainerPanel.Controls.Add(this.txtBaudRate);
            this.ContainerPanel.Controls.Add(this.lblComPortURL);
            this.ContainerPanel.Controls.Add(this.label3);
            this.ContainerPanel.Controls.Add(this.btnReset);
            this.ContainerPanel.Controls.Add(this.btnSave);
            this.ContainerPanel.Controls.Add(this.label1);
            this.ContainerPanel.Controls.Add(this.txtComPort);
            this.ContainerPanel.Location = new System.Drawing.Point(0, 37);
            this.ContainerPanel.Margin = new System.Windows.Forms.Padding(0);
            this.ContainerPanel.Name = "ContainerPanel";
            this.ContainerPanel.Size = new System.Drawing.Size(791, 586);
            this.ContainerPanel.TabIndex = 7;
            // 
            // lblPreferredBroadcaster
            // 
            this.lblPreferredBroadcaster.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPreferredBroadcaster.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblPreferredBroadcaster.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Italic);
            this.lblPreferredBroadcaster.Location = new System.Drawing.Point(118, 241);
            this.lblPreferredBroadcaster.Name = "lblPreferredBroadcaster";
            this.lblPreferredBroadcaster.Size = new System.Drawing.Size(593, 23);
            this.lblPreferredBroadcaster.TabIndex = 29;
            this.lblPreferredBroadcaster.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblPreferredBroadcaster.Click += new System.EventHandler(this.lblPreferredBroadcaster_Click);
            // 
            // txtTimeout
            // 
            this.txtTimeout.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTimeout.Font = new System.Drawing.Font("Bahnschrift", 14F);
            this.txtTimeout.Location = new System.Drawing.Point(117, 148);
            this.txtTimeout.Name = "txtTimeout";
            this.txtTimeout.Size = new System.Drawing.Size(594, 30);
            this.txtTimeout.TabIndex = 28;
            // 
            // lblTimeoutURL
            // 
            this.lblTimeoutURL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTimeoutURL.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblTimeoutURL.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Italic);
            this.lblTimeoutURL.Location = new System.Drawing.Point(120, 181);
            this.lblTimeoutURL.Name = "lblTimeoutURL";
            this.lblTimeoutURL.Size = new System.Drawing.Size(591, 23);
            this.lblTimeoutURL.TabIndex = 27;
            this.lblTimeoutURL.Text = "Timeout is used for communication between system and GSM modem. Click here to kno" +
    "w more about Timeout.";
            this.lblTimeoutURL.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblTimeoutURL.Click += new System.EventHandler(this.lblTimeoutURL_Click);
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(12, 148);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 30);
            this.label7.TabIndex = 26;
            this.label7.Text = "Timeout";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblBaudRateURL
            // 
            this.lblBaudRateURL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBaudRateURL.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblBaudRateURL.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Italic);
            this.lblBaudRateURL.Location = new System.Drawing.Point(120, 122);
            this.lblBaudRateURL.Name = "lblBaudRateURL";
            this.lblBaudRateURL.Size = new System.Drawing.Size(591, 23);
            this.lblBaudRateURL.TabIndex = 24;
            this.lblBaudRateURL.Text = "This is the baud rate for your GSM modem. Click here to know more about what baud" +
    " rate is.";
            this.lblBaudRateURL.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblBaudRateURL.Click += new System.EventHandler(this.lblBaudRateURL_Click);
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(12, 89);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 30);
            this.label5.TabIndex = 23;
            this.label5.Text = "Baud Rate";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtBaudRate
            // 
            this.txtBaudRate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBaudRate.Font = new System.Drawing.Font("Bahnschrift", 14F);
            this.txtBaudRate.Location = new System.Drawing.Point(118, 89);
            this.txtBaudRate.Name = "txtBaudRate";
            this.txtBaudRate.Size = new System.Drawing.Size(594, 30);
            this.txtBaudRate.TabIndex = 22;
            // 
            // lblComPortURL
            // 
            this.lblComPortURL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblComPortURL.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblComPortURL.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Italic);
            this.lblComPortURL.Location = new System.Drawing.Point(120, 63);
            this.lblComPortURL.Name = "lblComPortURL";
            this.lblComPortURL.Size = new System.Drawing.Size(591, 23);
            this.lblComPortURL.TabIndex = 21;
            this.lblComPortURL.Text = "Enter com port value manually if the system is unable to pick it. Click here to s" +
    "ee how to check com port value.";
            this.lblComPortURL.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblComPortURL.Click += new System.EventHandler(this.lblComPortURL_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Italic);
            this.label3.Location = new System.Drawing.Point(120, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(593, 23);
            this.label3.TabIndex = 20;
            this.label3.Text = "This is where you should specify GSM related configurations depending upon the mo" +
    "dem you are using.";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 30);
            this.label1.TabIndex = 1;
            this.label1.Text = "Com Port";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtComPort
            // 
            this.txtComPort.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtComPort.Font = new System.Drawing.Font("Bahnschrift", 14F);
            this.txtComPort.Location = new System.Drawing.Point(118, 30);
            this.txtComPort.Name = "txtComPort";
            this.txtComPort.Size = new System.Drawing.Size(381, 30);
            this.txtComPort.TabIndex = 0;
            // 
            // grpTestMessageHolder
            // 
            this.grpTestMessageHolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpTestMessageHolder.Controls.Add(this.txtTestMessageNumber);
            this.grpTestMessageHolder.Controls.Add(this.btnSendTestMessage);
            this.grpTestMessageHolder.Controls.Add(this.lblTestMessageHeading);
            this.grpTestMessageHolder.Enabled = false;
            this.grpTestMessageHolder.Location = new System.Drawing.Point(108, 241);
            this.grpTestMessageHolder.Name = "grpTestMessageHolder";
            this.grpTestMessageHolder.Size = new System.Drawing.Size(615, 90);
            this.grpTestMessageHolder.TabIndex = 31;
            this.grpTestMessageHolder.TabStop = false;
            this.grpTestMessageHolder.Visible = false;
            // 
            // lblTestMessageHeading
            // 
            this.lblTestMessageHeading.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTestMessageHeading.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblTestMessageHeading.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Italic);
            this.lblTestMessageHeading.Location = new System.Drawing.Point(8, 18);
            this.lblTestMessageHeading.Name = "lblTestMessageHeading";
            this.lblTestMessageHeading.Size = new System.Drawing.Size(596, 23);
            this.lblTestMessageHeading.TabIndex = 28;
            this.lblTestMessageHeading.Text = "Test your GSM Connection by sending a test message. Write your number below and c" +
    "lick send.";
            this.lblTestMessageHeading.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTestMessageNumber
            // 
            this.txtTestMessageNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTestMessageNumber.Font = new System.Drawing.Font("Bahnschrift", 14F);
            this.txtTestMessageNumber.Location = new System.Drawing.Point(11, 46);
            this.txtTestMessageNumber.Name = "txtTestMessageNumber";
            this.txtTestMessageNumber.Size = new System.Drawing.Size(487, 30);
            this.txtTestMessageNumber.TabIndex = 33;
            // 
            // btnChangeGSMStatus
            // 
            this.btnChangeGSMStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChangeGSMStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnChangeGSMStatus.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnChangeGSMStatus.FlatAppearance.BorderSize = 0;
            this.btnChangeGSMStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnChangeGSMStatus.Image = global::Nuntius.App.Properties.Resources.usb_disconnected_white_24;
            this.btnChangeGSMStatus.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnChangeGSMStatus.Location = new System.Drawing.Point(581, 30);
            this.btnChangeGSMStatus.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.btnChangeGSMStatus.Name = "btnChangeGSMStatus";
            this.btnChangeGSMStatus.Padding = new System.Windows.Forms.Padding(1);
            this.btnChangeGSMStatus.Size = new System.Drawing.Size(130, 30);
            this.btnChangeGSMStatus.TabIndex = 32;
            this.btnChangeGSMStatus.Text = "Disconnected";
            this.btnChangeGSMStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnChangeGSMStatus.UseVisualStyleBackColor = false;
            this.btnChangeGSMStatus.Click += new System.EventHandler(this.btnChangeGSMStatus_Click);
            // 
            // btnSendTestMessage
            // 
            this.btnSendTestMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSendTestMessage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.btnSendTestMessage.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSendTestMessage.FlatAppearance.BorderSize = 0;
            this.btnSendTestMessage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSendTestMessage.Image = global::Nuntius.App.Properties.Resources.new_broadcast_white_45;
            this.btnSendTestMessage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSendTestMessage.Location = new System.Drawing.Point(504, 46);
            this.btnSendTestMessage.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.btnSendTestMessage.Name = "btnSendTestMessage";
            this.btnSendTestMessage.Padding = new System.Windows.Forms.Padding(1);
            this.btnSendTestMessage.Size = new System.Drawing.Size(100, 30);
            this.btnSendTestMessage.TabIndex = 32;
            this.btnSendTestMessage.Text = "Send";
            this.btnSendTestMessage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSendTestMessage.UseVisualStyleBackColor = false;
            this.btnSendTestMessage.Click += new System.EventHandler(this.btnSendTestMessage_Click);
            // 
            // btnReSearchComPort
            // 
            this.btnReSearchComPort.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReSearchComPort.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.btnReSearchComPort.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnReSearchComPort.FlatAppearance.BorderSize = 0;
            this.btnReSearchComPort.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReSearchComPort.Image = global::Nuntius.App.Properties.Resources.reset_white_45;
            this.btnReSearchComPort.Location = new System.Drawing.Point(505, 30);
            this.btnReSearchComPort.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.btnReSearchComPort.Name = "btnReSearchComPort";
            this.btnReSearchComPort.Padding = new System.Windows.Forms.Padding(1);
            this.btnReSearchComPort.Size = new System.Drawing.Size(70, 30);
            this.btnReSearchComPort.TabIndex = 30;
            this.btnReSearchComPort.UseVisualStyleBackColor = false;
            this.btnReSearchComPort.Click += new System.EventHandler(this.btnReSearchComPort_Click);
            // 
            // btnReset
            // 
            this.btnReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.btnReset.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnReset.FlatAppearance.BorderSize = 0;
            this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReset.Image = global::Nuntius.App.Properties.Resources.contacts_white_45;
            this.btnReset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReset.Location = new System.Drawing.Point(505, 207);
            this.btnReset.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.btnReset.Name = "btnReset";
            this.btnReset.Padding = new System.Windows.Forms.Padding(1);
            this.btnReset.Size = new System.Drawing.Size(100, 30);
            this.btnReset.TabIndex = 15;
            this.btnReset.Text = "Reset";
            this.btnReset.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.btnSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Image = global::Nuntius.App.Properties.Resources.save_white_45;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(611, 207);
            this.btnSave.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.btnSave.Name = "btnSave";
            this.btnSave.Padding = new System.Windows.Forms.Padding(1);
            this.btnSave.Size = new System.Drawing.Size(100, 30);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSaveContact_Click);
            // 
            // lblControlHeader
            // 
            this.lblControlHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblControlHeader.BackColor = System.Drawing.Color.Transparent;
            this.lblControlHeader.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblControlHeader.Font = new System.Drawing.Font("Bahnschrift SemiBold", 13F, System.Drawing.FontStyle.Bold);
            this.lblControlHeader.Image = global::Nuntius.App.Properties.Resources.gsm_model_settings_white_45;
            this.lblControlHeader.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblControlHeader.Location = new System.Drawing.Point(0, 0);
            this.lblControlHeader.Margin = new System.Windows.Forms.Padding(0);
            this.lblControlHeader.Name = "lblControlHeader";
            this.lblControlHeader.Size = new System.Drawing.Size(791, 37);
            this.lblControlHeader.TabIndex = 6;
            this.lblControlHeader.Text = "GSM Modem Settings";
            this.lblControlHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GSMModemSettingsUserControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(155)))), ((int)(((byte)(229)))));
            this.Controls.Add(this.ContainerPanel);
            this.Controls.Add(this.lblControlHeader);
            this.Font = new System.Drawing.Font("Bahnschrift", 9F);
            this.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.Name = "GSMModemSettingsUserControl";
            this.Size = new System.Drawing.Size(791, 623);
            this.ContainerPanel.ResumeLayout(false);
            this.ContainerPanel.PerformLayout();
            this.grpTestMessageHolder.ResumeLayout(false);
            this.grpTestMessageHolder.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblControlHeader;
        private System.Windows.Forms.Panel ContainerPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtComPort;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblBaudRateURL;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtBaudRate;
        private System.Windows.Forms.Label lblComPortURL;
        private System.Windows.Forms.Label lblTimeoutURL;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtTimeout;
        private System.Windows.Forms.Label lblPreferredBroadcaster;
        private System.Windows.Forms.Button btnReSearchComPort;
        private System.Windows.Forms.GroupBox grpTestMessageHolder;
        private System.Windows.Forms.Label lblTestMessageHeading;
        private System.Windows.Forms.Button btnSendTestMessage;
        private System.Windows.Forms.Button btnChangeGSMStatus;
        private System.Windows.Forms.TextBox txtTestMessageNumber;
    }
}
