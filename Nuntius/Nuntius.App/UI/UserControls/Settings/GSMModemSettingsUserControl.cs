﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using Nuntius.Entities;
using Nuntius.App.UI.Forms;
using Nuntius.Globals;
using Nuntius.App.Code;
using Nuntius.App.UI.UserControls.BaseControls;
using Nuntius.Services;
using Nuntius.Messenger;
using System.Drawing;
using System.Threading.Tasks;

namespace Nuntius.App.UI.UserControls.Settings
{
    public partial class GSMModemSettingsUserControl : NuntiusControl
    {
        #region Define as Singleton
        private static GSMModemSettingsUserControl _Instance;

        public static GSMModemSettingsUserControl Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new GSMModemSettingsUserControl();
                }

                return (_Instance);
            }
        }

        private GSMModemSettingsUserControl()
        {
            InitializeComponent();
        }
        #endregion
        
        public new void Load()
        {
            base.Load();

            DashboardForm.Instance.DisplayLoader();
            this.Enabled = false;

            txtComPort.Text = Configurations.GSMComPortConfig.ToString();
            txtBaudRate.Text = Configurations.GSMBaudRateConfig.ToString();
            txtTimeout.Text = Configurations.GSMTimeoutConfig.ToString();
            lblPreferredBroadcaster.Text = string.Format(Messages.PreferredBroadcasterMessage, ((BroadcasterEnums)(Configurations.PrefferredBroadcaster)).ToString());

            UpdateConnectionStatus();

            this.Enabled = true;
            DashboardForm.Instance.DisplayLoader(false);
        }

        private void btnReSearchComPort_Click(object sender, EventArgs e)
        {
            ResearchComPort();
        }

        private void ResearchComPort()
        {
            DashboardForm.Instance.DisplayLoader();
            this.Enabled = false;

            int newComPort = SystemHelper.GetActiveComPort();

            txtComPort.Text = newComPort.ToString();
            Configurations.UpdateComPort(newComPort);

            this.Enabled = true;
            DashboardForm.Instance.DisplayLoader(false);
        }

        private async void btnSaveContact_Click(object sender, EventArgs e)
        {
            var comPort = 0;
            var baudRate = 0;
            var timeOut = 0;

            try
            {
                comPort = int.Parse(txtComPort.Text);
                baudRate = int.Parse(txtBaudRate.Text);
                timeOut = int.Parse(txtTimeout.Text);
            }
            catch
            {
                DashboardForm.Instance.ShowErrorMessage(Messages.ValueFormatIssue);
                return;
            }

            DashboardForm.Instance.DisplayLoader();
            this.Enabled = false;

            Configurations.UpdateComPort(comPort);
            var GSMBaudRateConfig = await ConfigurationsService.Instance.UpdateConfiguration(new Configuration() { ID = (int)ConfigurationEnums.GSMBaudRateConfig, Value = baudRate.ToString() });
            var GSMTimeoutConfig = await ConfigurationsService.Instance.UpdateConfiguration(new Configuration() { ID = (int)ConfigurationEnums.GSMTimeoutConfig, Value = timeOut.ToString() });

            Configurations.LoadConfigurations();

            this.Enabled = true;
            DashboardForm.Instance.DisplayLoader(false);

            DashboardForm.Instance.ShowSuccessMessage(Messages.GSMModemSettingsUpdatedSuccess);
            Load();
        }
        
        private void btnReset_Click(object sender, EventArgs e)
        {
            if (DashboardForm.Instance.ShowConfirmationMessage(Messages.ConfirmSettingsReset) == DialogResult.OK)
            {
                txtBaudRate.Text = Defaults.GSMBaudRate.ToString();
                txtTimeout.Text = Defaults.GSMTimeout.ToString();

                btnSave.PerformClick();
            }
        }
        private void lblPreferredBroadcaster_Click(object sender, EventArgs e)
        {
            SMSSettingsUserControl.Instance.Load();
        }

        private void lblComPortURL_Click(object sender, EventArgs e)
        {
            Process.Start(Configurations.GSMComPortURL);
        }

        private void lblBaudRateURL_Click(object sender, EventArgs e)
        {
            Process.Start(Configurations.GSMBaudRateURL);
        }

        private void lblTimeoutURL_Click(object sender, EventArgs e)
        {
            Process.Start(Configurations.GSMTimeoutURL);
        }

        private void btnChangeGSMStatus_Click(object sender, EventArgs e)
        {
            var comPort = 0;

            try
            {
                comPort = int.Parse(txtComPort.Text);
            }
            catch
            {
                DashboardForm.Instance.ShowErrorMessage(Messages.ValueFormatIssue);
                return;
            }

            if(comPort != Configurations.GSMComPortConfig)
            {
                Configurations.UpdateComPort(comPort);
            }
                        
            if(!GSMMessenger.ConnectionStatus())
            {
                GSMMessenger.Connect();
            }
            else
            {
                GSMMessenger.Disconnect();
            }

            UpdateConnectionStatus();
        }

        private void UpdateConnectionStatus()
        {
            if (GSMMessenger.ConnectionStatus())
            {
                btnChangeGSMStatus.Image = Properties.Resources.usb_connected_white_24;
                btnChangeGSMStatus.BackColor = Color.Green;
                btnChangeGSMStatus.Text = "Connected";

                grpTestMessageHolder.Visible = true;
                grpTestMessageHolder.Enabled = true;
            }
            else
            {
                btnChangeGSMStatus.Image = Properties.Resources.usb_disconnected_white_24;
                btnChangeGSMStatus.BackColor = Color.Red;
                btnChangeGSMStatus.Text = "Disconnected";

                grpTestMessageHolder.Visible = false;
                grpTestMessageHolder.Enabled = false;
            }
        }

        private async void btnSendTestMessage_Click(object sender, EventArgs e)
        {
            var number = txtTestMessageNumber.Text.Trim();

            if (string.IsNullOrEmpty(number))
            {
                DashboardForm.Instance.ShowWarningMessage(Messages.ContactNumbersNeeded);
                return;
            }
            
            try
            {
                DashboardForm.Instance.DisplayLoader();
                this.Enabled = false;

                var messageResponse = await SendTestMessage(number);

                if (!string.IsNullOrEmpty(messageResponse))
                {
                    DashboardForm.Instance.ShowSuccessMessage(Messages.TestMessageSent);
                }
                else
                {
                    throw new Exception(Messages.TestMessageNotSent);
                }
            }
            catch (Exception ex)
            {
                DashboardForm.Instance.ShowErrorMessage(string.Format(Messages.ErrorOccurredWithCustomMessage, ex.Message));
            }
            finally
            {
                this.Enabled = true;
                DashboardForm.Instance.DisplayLoader(false);
            }
        }

        public Task<string> SendTestMessage(string number)
        {
            return Task.Run(() =>
            {
                return GSMMessenger.SendTestMessage(Messages.NuntiusTestMessageContent, number);
            });
        }
    }
}
