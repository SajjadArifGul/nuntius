﻿namespace Nuntius.App.UI.UserControls.Settings
{
    partial class GeneralSettingsUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ContainerPanel = new System.Windows.Forms.Panel();
            this.lblRecordsCountURL = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtRecordsCount = new System.Windows.Forms.TextBox();
            this.lblControlHeader = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtMessageSignature = new System.Windows.Forms.TextBox();
            this.cbIncludeSignature = new System.Windows.Forms.CheckBox();
            this.ContainerPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ContainerPanel
            // 
            this.ContainerPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ContainerPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(155)))), ((int)(((byte)(229)))));
            this.ContainerPanel.Controls.Add(this.cbIncludeSignature);
            this.ContainerPanel.Controls.Add(this.label2);
            this.ContainerPanel.Controls.Add(this.label4);
            this.ContainerPanel.Controls.Add(this.txtMessageSignature);
            this.ContainerPanel.Controls.Add(this.lblRecordsCountURL);
            this.ContainerPanel.Controls.Add(this.label3);
            this.ContainerPanel.Controls.Add(this.btnReset);
            this.ContainerPanel.Controls.Add(this.btnSave);
            this.ContainerPanel.Controls.Add(this.label1);
            this.ContainerPanel.Controls.Add(this.txtRecordsCount);
            this.ContainerPanel.Location = new System.Drawing.Point(0, 37);
            this.ContainerPanel.Margin = new System.Windows.Forms.Padding(0);
            this.ContainerPanel.Name = "ContainerPanel";
            this.ContainerPanel.Size = new System.Drawing.Size(791, 586);
            this.ContainerPanel.TabIndex = 7;
            // 
            // lblRecordsCountURL
            // 
            this.lblRecordsCountURL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRecordsCountURL.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblRecordsCountURL.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Italic);
            this.lblRecordsCountURL.Location = new System.Drawing.Point(120, 63);
            this.lblRecordsCountURL.Name = "lblRecordsCountURL";
            this.lblRecordsCountURL.Size = new System.Drawing.Size(591, 23);
            this.lblRecordsCountURL.TabIndex = 21;
            this.lblRecordsCountURL.Text = "Set the value for how many records you want to show on a view.";
            this.lblRecordsCountURL.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Italic);
            this.label3.Location = new System.Drawing.Point(120, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(593, 23);
            this.label3.TabIndex = 20;
            this.label3.Text = "This is where you should specify general system related configurations.";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnReset
            // 
            this.btnReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.btnReset.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnReset.FlatAppearance.BorderSize = 0;
            this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReset.Image = global::Nuntius.App.Properties.Resources.contacts_white_45;
            this.btnReset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReset.Location = new System.Drawing.Point(505, 148);
            this.btnReset.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.btnReset.Name = "btnReset";
            this.btnReset.Padding = new System.Windows.Forms.Padding(1);
            this.btnReset.Size = new System.Drawing.Size(100, 30);
            this.btnReset.TabIndex = 15;
            this.btnReset.Text = "Reset";
            this.btnReset.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.btnSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Image = global::Nuntius.App.Properties.Resources.save_white_45;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(611, 148);
            this.btnSave.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.btnSave.Name = "btnSave";
            this.btnSave.Padding = new System.Windows.Forms.Padding(1);
            this.btnSave.Size = new System.Drawing.Size(100, 30);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSaveContact_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 30);
            this.label1.TabIndex = 1;
            this.label1.Text = "Records Count";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtRecordsCount
            // 
            this.txtRecordsCount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtRecordsCount.Font = new System.Drawing.Font("Bahnschrift", 14F);
            this.txtRecordsCount.Location = new System.Drawing.Point(118, 30);
            this.txtRecordsCount.Name = "txtRecordsCount";
            this.txtRecordsCount.Size = new System.Drawing.Size(593, 30);
            this.txtRecordsCount.TabIndex = 0;
            // 
            // lblControlHeader
            // 
            this.lblControlHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblControlHeader.BackColor = System.Drawing.Color.Transparent;
            this.lblControlHeader.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblControlHeader.Font = new System.Drawing.Font("Bahnschrift SemiBold", 13F, System.Drawing.FontStyle.Bold);
            this.lblControlHeader.Image = global::Nuntius.App.Properties.Resources.settings_white_45;
            this.lblControlHeader.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblControlHeader.Location = new System.Drawing.Point(0, 0);
            this.lblControlHeader.Margin = new System.Windows.Forms.Padding(0);
            this.lblControlHeader.Name = "lblControlHeader";
            this.lblControlHeader.Size = new System.Drawing.Size(791, 37);
            this.lblControlHeader.TabIndex = 6;
            this.lblControlHeader.Text = "General Settings";
            this.lblControlHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.label2.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Italic);
            this.label2.Location = new System.Drawing.Point(120, 122);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(591, 23);
            this.label2.TabIndex = 24;
            this.label2.Text = "Signature is text added in the end of every message. Add your signature text and " +
    "check on Include Signature to add.";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(12, 89);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 30);
            this.label4.TabIndex = 23;
            this.label4.Text = "Signature";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtMessageSignature
            // 
            this.txtMessageSignature.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMessageSignature.Font = new System.Drawing.Font("Bahnschrift", 14F);
            this.txtMessageSignature.Location = new System.Drawing.Point(118, 89);
            this.txtMessageSignature.Name = "txtMessageSignature";
            this.txtMessageSignature.Size = new System.Drawing.Size(466, 30);
            this.txtMessageSignature.TabIndex = 22;
            // 
            // cbIncludeSignature
            // 
            this.cbIncludeSignature.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbIncludeSignature.Location = new System.Drawing.Point(590, 89);
            this.cbIncludeSignature.Name = "cbIncludeSignature";
            this.cbIncludeSignature.Size = new System.Drawing.Size(121, 30);
            this.cbIncludeSignature.TabIndex = 25;
            this.cbIncludeSignature.Text = "Include Signature";
            this.cbIncludeSignature.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbIncludeSignature.UseVisualStyleBackColor = true;
            // 
            // GeneralSettingsUserControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(155)))), ((int)(((byte)(229)))));
            this.Controls.Add(this.ContainerPanel);
            this.Controls.Add(this.lblControlHeader);
            this.Font = new System.Drawing.Font("Bahnschrift", 9F);
            this.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.Name = "GeneralSettingsUserControl";
            this.Size = new System.Drawing.Size(791, 623);
            this.ContainerPanel.ResumeLayout(false);
            this.ContainerPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblControlHeader;
        private System.Windows.Forms.Panel ContainerPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtRecordsCount;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblRecordsCountURL;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtMessageSignature;
        private System.Windows.Forms.CheckBox cbIncludeSignature;
    }
}
