﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using Nuntius.Entities;
using Nuntius.App.UI.Forms;
using Nuntius.Globals;
using Nuntius.App.Code;
using Nuntius.App.UI.UserControls.BaseControls;
using Nuntius.Services;

namespace Nuntius.App.UI.UserControls.Settings
{
    public partial class GeneralSettingsUserControl : NuntiusControl
    {
        #region Define as Singleton
        private static GeneralSettingsUserControl _Instance;

        public static GeneralSettingsUserControl Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new GeneralSettingsUserControl();
                }

                return (_Instance);
            }
        }

        private GeneralSettingsUserControl()
        {
            InitializeComponent();
        }
        #endregion
        
        public new void Load()
        {
            base.Load();

            DashboardForm.Instance.DisplayLoader();
            this.Enabled = false;

            txtRecordsCount.Text = Configurations.SinglePageRecordsCount.ToString();
            txtMessageSignature.Text = Configurations.MessageSignature.ToString();
            cbIncludeSignature.Checked = Configurations.IncludeMessageSignature;

            this.Enabled = true;
            DashboardForm.Instance.DisplayLoader(false);
        }
        
        private async void btnSaveContact_Click(object sender, EventArgs e)
        {
            var recordsCount = 0;
            var messageSignature = string.Empty;
            var includeSignature = cbIncludeSignature.Checked;

            try
            {
                recordsCount = int.Parse(txtRecordsCount.Text);
                if (recordsCount == 0) throw new Exception(Messages.ValueFormatIssue);

                messageSignature = txtMessageSignature.Text.Trim();

                if(includeSignature && string.IsNullOrEmpty(messageSignature)) throw new Exception(Messages.NoSignatureToInclude);
            }
            catch (FormatException)
            {
                DashboardForm.Instance.ShowErrorMessage(Messages.ValueFormatIssue);
                return;
            }
            catch (Exception ex)
            {
                DashboardForm.Instance.ShowErrorMessage(ex.Message);
                return;
            }

            DashboardForm.Instance.DisplayLoader();
            this.Enabled = false;

            var RecordsCountConfig = await ConfigurationsService.Instance.UpdateConfiguration(new Configuration() { ID = (int)ConfigurationEnums.SinglePageRecordsCount, Value = recordsCount.ToString() });
            var MessageSignatureConfig = await ConfigurationsService.Instance.UpdateConfiguration(new Configuration() { ID = (int)ConfigurationEnums.MessageSignature, Value = messageSignature });
            var IncludeMessageSignatureConfig = await ConfigurationsService.Instance.UpdateConfiguration(new Configuration() { ID = (int)ConfigurationEnums.IncludeMessageSignature, Value = includeSignature.ToString() });

            Configurations.LoadConfigurations();

            this.Enabled = true;
            DashboardForm.Instance.DisplayLoader(false);

            DashboardForm.Instance.ShowSuccessMessage(Messages.SettingsUpdatedSuccess);
            Load();
        }
        
        private void btnReset_Click(object sender, EventArgs e)
        {
            if (DashboardForm.Instance.ShowConfirmationMessage(Messages.ConfirmSettingsReset) == DialogResult.OK)
            {
                txtRecordsCount.Text = Defaults.SinglePageRecordsCount.ToString();
                txtMessageSignature.Text = string.Empty;
                cbIncludeSignature.Checked = false;

                btnSave.PerformClick();
            }
        }
    }
}
