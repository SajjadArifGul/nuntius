﻿using System;
using System.Linq;
using System.Windows.Forms;
using Nuntius.Entities;
using Nuntius.App.UI.Forms;
using Nuntius.Globals;
using Nuntius.App.UI.UserControls.BaseControls;
using Nuntius.App.Code;
using Nuntius.Services;

namespace Nuntius.App.UI.UserControls.Settings
{
    public partial class SMSSettingsUserControl : NuntiusControl
    {
        #region Define as Singleton
        private static SMSSettingsUserControl _Instance;

        public static SMSSettingsUserControl Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new SMSSettingsUserControl();
                }

                return (_Instance);
            }
        }

        private SMSSettingsUserControl()
        {
            InitializeComponent();
        }
        #endregion
        
        public async new void Load()
        {
            base.Load();

            DashboardForm.Instance.DisplayLoader();
            this.Enabled = false;
            
            txtTime.Text = Configurations.SMSTime.ToString();
            txtMessageCount.Text = Configurations.SMSMessageCount.ToString();
            
            var broadcasters = await BroadcastersService.Instance.GetBroadcasters();

            if(!Configurations.IsTwilioSetupped)
            {
                var twilioBroadcaster = broadcasters.FirstOrDefault(x => x.ID == (int)BroadcasterEnums.Twilio);

                if (twilioBroadcaster != null) broadcasters.Remove(twilioBroadcaster);
            }

            cmbPreferredBroadcaster.DataSource = broadcasters;
            cmbPreferredBroadcaster.DisplayMember = "Name";
            cmbPreferredBroadcaster.ValueMember = "ID";
            cmbPreferredBroadcaster.SelectedValue = Configurations.PrefferredBroadcaster;

            this.Enabled = true;
            DashboardForm.Instance.DisplayLoader(false);
        }

        private async void btnSaveContact_Click(object sender, EventArgs e)
        {
            var time = 0;
            var messageCount = 0;

            try
            {
                time = int.Parse(txtTime.Text);
                messageCount = int.Parse(txtMessageCount.Text);
            }
            catch
            {
                DashboardForm.Instance.ShowErrorMessage(Messages.ValueFormatIssue);
                return;
            }

            DashboardForm.Instance.DisplayLoader();
            this.Enabled = false;

            var SMSTimeConfig = await ConfigurationsService.Instance.UpdateConfiguration(new Configuration() { ID = (int)ConfigurationEnums.SMSTimeConfig, Value = time.ToString() });
            var SMSMessageCountConfig = await ConfigurationsService.Instance.UpdateConfiguration(new Configuration() { ID = (int)ConfigurationEnums.SMSMessageCountConfig, Value = messageCount.ToString() });
            var PrefferedBroadcaster = await ConfigurationsService.Instance.UpdateConfiguration(new Configuration() { ID = (int)ConfigurationEnums.PrefferedBroadcaster, Value = cmbPreferredBroadcaster.SelectedValue.ToString() });

            Configurations.LoadConfigurations();

            this.Enabled = true;
            DashboardForm.Instance.DisplayLoader(false);
            
            DashboardForm.Instance.ShowSuccessMessage(Messages.SettingsUpdatedSuccess);
            Load();
        }
        
        private void btnReset_Click(object sender, EventArgs e)
        {
            if (DashboardForm.Instance.ShowConfirmationMessage(Messages.ConfirmSettingsReset) == DialogResult.OK)
            {
                txtTime.Text = Defaults.SMSTime.ToString();
                txtMessageCount.Text = Defaults.SMSMessageCount.ToString();
                cmbPreferredBroadcaster.SelectedValue = Defaults.PrefferredBroadcaster;

                btnSave.PerformClick();
            }
        }
    }
}
