﻿namespace Nuntius.App.UI.UserControls.Settings
{
    partial class SettingsMainUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.UserControlMenuFlowPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.btnGeneralSettings = new System.Windows.Forms.Button();
            this.btnSMSSettings = new System.Windows.Forms.Button();
            this.btnGSMModemSettings = new System.Windows.Forms.Button();
            this.btnTwilioSettings = new System.Windows.Forms.Button();
            this.lblControlHeader = new System.Windows.Forms.Label();
            this.UserControlMenuFlowPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // UserControlMenuFlowPanel
            // 
            this.UserControlMenuFlowPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.UserControlMenuFlowPanel.Controls.Add(this.btnGeneralSettings);
            this.UserControlMenuFlowPanel.Controls.Add(this.btnSMSSettings);
            this.UserControlMenuFlowPanel.Controls.Add(this.btnGSMModemSettings);
            this.UserControlMenuFlowPanel.Controls.Add(this.btnTwilioSettings);
            this.UserControlMenuFlowPanel.Location = new System.Drawing.Point(0, 37);
            this.UserControlMenuFlowPanel.Margin = new System.Windows.Forms.Padding(0);
            this.UserControlMenuFlowPanel.Name = "UserControlMenuFlowPanel";
            this.UserControlMenuFlowPanel.Padding = new System.Windows.Forms.Padding(5);
            this.UserControlMenuFlowPanel.Size = new System.Drawing.Size(791, 586);
            this.UserControlMenuFlowPanel.TabIndex = 7;
            // 
            // btnGeneralSettings
            // 
            this.btnGeneralSettings.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.btnGeneralSettings.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGeneralSettings.FlatAppearance.BorderSize = 0;
            this.btnGeneralSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGeneralSettings.Font = new System.Drawing.Font("Bahnschrift SemiBold", 10F, System.Drawing.FontStyle.Bold);
            this.btnGeneralSettings.Image = global::Nuntius.App.Properties.Resources.settings_white_45;
            this.btnGeneralSettings.Location = new System.Drawing.Point(10, 10);
            this.btnGeneralSettings.Margin = new System.Windows.Forms.Padding(5);
            this.btnGeneralSettings.Name = "btnGeneralSettings";
            this.btnGeneralSettings.Size = new System.Drawing.Size(192, 85);
            this.btnGeneralSettings.TabIndex = 4;
            this.btnGeneralSettings.Text = "General Settings";
            this.btnGeneralSettings.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnGeneralSettings.UseVisualStyleBackColor = false;
            this.btnGeneralSettings.Click += new System.EventHandler(this.btnGeneralSettings_Click);
            // 
            // btnSMSSettings
            // 
            this.btnSMSSettings.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.btnSMSSettings.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSMSSettings.FlatAppearance.BorderSize = 0;
            this.btnSMSSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSMSSettings.Font = new System.Drawing.Font("Bahnschrift SemiBold", 10F, System.Drawing.FontStyle.Bold);
            this.btnSMSSettings.Image = global::Nuntius.App.Properties.Resources.new_broadcast_white_45;
            this.btnSMSSettings.Location = new System.Drawing.Point(212, 10);
            this.btnSMSSettings.Margin = new System.Windows.Forms.Padding(5);
            this.btnSMSSettings.Name = "btnSMSSettings";
            this.btnSMSSettings.Size = new System.Drawing.Size(192, 85);
            this.btnSMSSettings.TabIndex = 0;
            this.btnSMSSettings.Text = "SMS Settings";
            this.btnSMSSettings.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnSMSSettings.UseVisualStyleBackColor = false;
            this.btnSMSSettings.Click += new System.EventHandler(this.btnSMSSettings_Click);
            // 
            // btnGSMModemSettings
            // 
            this.btnGSMModemSettings.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.btnGSMModemSettings.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGSMModemSettings.FlatAppearance.BorderSize = 0;
            this.btnGSMModemSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGSMModemSettings.Font = new System.Drawing.Font("Bahnschrift SemiBold", 10F, System.Drawing.FontStyle.Bold);
            this.btnGSMModemSettings.Image = global::Nuntius.App.Properties.Resources.gsm_model_settings_white_45;
            this.btnGSMModemSettings.Location = new System.Drawing.Point(414, 10);
            this.btnGSMModemSettings.Margin = new System.Windows.Forms.Padding(5);
            this.btnGSMModemSettings.Name = "btnGSMModemSettings";
            this.btnGSMModemSettings.Size = new System.Drawing.Size(192, 85);
            this.btnGSMModemSettings.TabIndex = 2;
            this.btnGSMModemSettings.Text = "GSM Modem Settings";
            this.btnGSMModemSettings.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnGSMModemSettings.UseVisualStyleBackColor = false;
            this.btnGSMModemSettings.Click += new System.EventHandler(this.btnGSMModemSettings_Click);
            // 
            // btnTwilioSettings
            // 
            this.btnTwilioSettings.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.btnTwilioSettings.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTwilioSettings.FlatAppearance.BorderSize = 0;
            this.btnTwilioSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTwilioSettings.Font = new System.Drawing.Font("Bahnschrift SemiBold", 10F, System.Drawing.FontStyle.Bold);
            this.btnTwilioSettings.Image = global::Nuntius.App.Properties.Resources.twilio_logo_red_45;
            this.btnTwilioSettings.Location = new System.Drawing.Point(10, 105);
            this.btnTwilioSettings.Margin = new System.Windows.Forms.Padding(5);
            this.btnTwilioSettings.Name = "btnTwilioSettings";
            this.btnTwilioSettings.Size = new System.Drawing.Size(192, 85);
            this.btnTwilioSettings.TabIndex = 3;
            this.btnTwilioSettings.Text = "Twilio Settings";
            this.btnTwilioSettings.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnTwilioSettings.UseVisualStyleBackColor = false;
            this.btnTwilioSettings.Click += new System.EventHandler(this.btnTwilioSettings_Click);
            // 
            // lblControlHeader
            // 
            this.lblControlHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblControlHeader.BackColor = System.Drawing.Color.Transparent;
            this.lblControlHeader.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblControlHeader.Font = new System.Drawing.Font("Bahnschrift SemiBold", 13F, System.Drawing.FontStyle.Bold);
            this.lblControlHeader.Image = global::Nuntius.App.Properties.Resources.settings_white_45;
            this.lblControlHeader.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblControlHeader.Location = new System.Drawing.Point(0, 0);
            this.lblControlHeader.Margin = new System.Windows.Forms.Padding(0);
            this.lblControlHeader.Name = "lblControlHeader";
            this.lblControlHeader.Size = new System.Drawing.Size(791, 37);
            this.lblControlHeader.TabIndex = 6;
            this.lblControlHeader.Text = "Settings";
            this.lblControlHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SettingsMainUserControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(155)))), ((int)(((byte)(229)))));
            this.Controls.Add(this.UserControlMenuFlowPanel);
            this.Controls.Add(this.lblControlHeader);
            this.Font = new System.Drawing.Font("Bahnschrift", 9F);
            this.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.Name = "SettingsMainUserControl";
            this.Size = new System.Drawing.Size(791, 623);
            this.UserControlMenuFlowPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblControlHeader;
        private System.Windows.Forms.FlowLayoutPanel UserControlMenuFlowPanel;
        private System.Windows.Forms.Button btnSMSSettings;
        private System.Windows.Forms.Button btnGSMModemSettings;
        private System.Windows.Forms.Button btnTwilioSettings;
        private System.Windows.Forms.Button btnGeneralSettings;
    }
}
