﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nuntius.App.UI.Forms;
using Nuntius.App.UI.UserControls.BaseControls;

namespace Nuntius.App.UI.UserControls.Settings
{
    public partial class SettingsMainUserControl : NuntiusControl
    {
        #region Define as Singleton
        private static SettingsMainUserControl _Instance;

        public static SettingsMainUserControl Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new SettingsMainUserControl();
                }

                return (_Instance);
            }
        }

        private SettingsMainUserControl()
        {
            InitializeComponent();
        }
        #endregion

        private void btnSMSSettings_Click(object sender, EventArgs e)
        {
            SMSSettingsUserControl.Instance.Load();
        }
        
        private void btnGSMModemSettings_Click(object sender, EventArgs e)
        {
            GSMModemSettingsUserControl.Instance.Load();
        }

        private void btnTwilioSettings_Click(object sender, EventArgs e)
        {
            TwilioSettingsUserControl.Instance.Load();
        }

        private void btnGeneralSettings_Click(object sender, EventArgs e)
        {
            GeneralSettingsUserControl.Instance.Load();
        }
    }
}
