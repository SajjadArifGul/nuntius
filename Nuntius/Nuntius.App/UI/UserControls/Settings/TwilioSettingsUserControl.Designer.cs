﻿namespace Nuntius.App.UI.UserControls.Settings
{
    partial class TwilioSettingsUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ContainerPanel = new System.Windows.Forms.Panel();
            this.grpTestMessageHolder = new System.Windows.Forms.GroupBox();
            this.txtTestMessageNumber = new System.Windows.Forms.TextBox();
            this.btnSendTestMessage = new System.Windows.Forms.Button();
            this.lblTestMessageHeading = new System.Windows.Forms.Label();
            this.btnToggleAccountSidDisplay = new System.Windows.Forms.Button();
            this.btnToggleAuthTokenDisplay = new System.Windows.Forms.Button();
            this.lblPreferredBroadcaster = new System.Windows.Forms.Label();
            this.txtTwilioNumber = new System.Windows.Forms.TextBox();
            this.lblGetTwilioPhoneNumber = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblGetTwilioAuthToken = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtAuthToken = new System.Windows.Forms.TextBox();
            this.lblGetTwilioAccountSid = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtAccountSid = new System.Windows.Forms.TextBox();
            this.lblControlHeader = new System.Windows.Forms.Label();
            this.ContainerPanel.SuspendLayout();
            this.grpTestMessageHolder.SuspendLayout();
            this.SuspendLayout();
            // 
            // ContainerPanel
            // 
            this.ContainerPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ContainerPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(155)))), ((int)(((byte)(229)))));
            this.ContainerPanel.Controls.Add(this.grpTestMessageHolder);
            this.ContainerPanel.Controls.Add(this.btnToggleAccountSidDisplay);
            this.ContainerPanel.Controls.Add(this.btnToggleAuthTokenDisplay);
            this.ContainerPanel.Controls.Add(this.lblPreferredBroadcaster);
            this.ContainerPanel.Controls.Add(this.txtTwilioNumber);
            this.ContainerPanel.Controls.Add(this.lblGetTwilioPhoneNumber);
            this.ContainerPanel.Controls.Add(this.label7);
            this.ContainerPanel.Controls.Add(this.lblGetTwilioAuthToken);
            this.ContainerPanel.Controls.Add(this.label5);
            this.ContainerPanel.Controls.Add(this.txtAuthToken);
            this.ContainerPanel.Controls.Add(this.lblGetTwilioAccountSid);
            this.ContainerPanel.Controls.Add(this.label3);
            this.ContainerPanel.Controls.Add(this.btnSave);
            this.ContainerPanel.Controls.Add(this.label1);
            this.ContainerPanel.Controls.Add(this.txtAccountSid);
            this.ContainerPanel.Location = new System.Drawing.Point(0, 37);
            this.ContainerPanel.Margin = new System.Windows.Forms.Padding(0);
            this.ContainerPanel.Name = "ContainerPanel";
            this.ContainerPanel.Size = new System.Drawing.Size(791, 586);
            this.ContainerPanel.TabIndex = 7;
            // 
            // grpTestMessageHolder
            // 
            this.grpTestMessageHolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpTestMessageHolder.Controls.Add(this.txtTestMessageNumber);
            this.grpTestMessageHolder.Controls.Add(this.btnSendTestMessage);
            this.grpTestMessageHolder.Controls.Add(this.lblTestMessageHeading);
            this.grpTestMessageHolder.Location = new System.Drawing.Point(108, 275);
            this.grpTestMessageHolder.Name = "grpTestMessageHolder";
            this.grpTestMessageHolder.Size = new System.Drawing.Size(615, 90);
            this.grpTestMessageHolder.TabIndex = 33;
            this.grpTestMessageHolder.TabStop = false;
            // 
            // txtTestMessageNumber
            // 
            this.txtTestMessageNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTestMessageNumber.Font = new System.Drawing.Font("Bahnschrift", 14F);
            this.txtTestMessageNumber.Location = new System.Drawing.Point(11, 46);
            this.txtTestMessageNumber.Name = "txtTestMessageNumber";
            this.txtTestMessageNumber.Size = new System.Drawing.Size(487, 30);
            this.txtTestMessageNumber.TabIndex = 33;
            // 
            // btnSendTestMessage
            // 
            this.btnSendTestMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSendTestMessage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.btnSendTestMessage.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSendTestMessage.FlatAppearance.BorderSize = 0;
            this.btnSendTestMessage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSendTestMessage.Image = global::Nuntius.App.Properties.Resources.new_broadcast_white_45;
            this.btnSendTestMessage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSendTestMessage.Location = new System.Drawing.Point(504, 46);
            this.btnSendTestMessage.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.btnSendTestMessage.Name = "btnSendTestMessage";
            this.btnSendTestMessage.Padding = new System.Windows.Forms.Padding(1);
            this.btnSendTestMessage.Size = new System.Drawing.Size(100, 30);
            this.btnSendTestMessage.TabIndex = 32;
            this.btnSendTestMessage.Text = "Send";
            this.btnSendTestMessage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSendTestMessage.UseVisualStyleBackColor = false;
            this.btnSendTestMessage.Click += new System.EventHandler(this.btnSendTestMessage_Click);
            // 
            // lblTestMessageHeading
            // 
            this.lblTestMessageHeading.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTestMessageHeading.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblTestMessageHeading.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Italic);
            this.lblTestMessageHeading.Location = new System.Drawing.Point(8, 18);
            this.lblTestMessageHeading.Name = "lblTestMessageHeading";
            this.lblTestMessageHeading.Size = new System.Drawing.Size(596, 23);
            this.lblTestMessageHeading.TabIndex = 28;
            this.lblTestMessageHeading.Text = "Test your Twilio configurations by sending a test message. Write your number belo" +
    "w and click send.";
            this.lblTestMessageHeading.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnToggleAccountSidDisplay
            // 
            this.btnToggleAccountSidDisplay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnToggleAccountSidDisplay.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.btnToggleAccountSidDisplay.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnToggleAccountSidDisplay.FlatAppearance.BorderSize = 0;
            this.btnToggleAccountSidDisplay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnToggleAccountSidDisplay.Image = global::Nuntius.App.Properties.Resources.text_hide_white_45;
            this.btnToggleAccountSidDisplay.Location = new System.Drawing.Point(651, 30);
            this.btnToggleAccountSidDisplay.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.btnToggleAccountSidDisplay.Name = "btnToggleAccountSidDisplay";
            this.btnToggleAccountSidDisplay.Padding = new System.Windows.Forms.Padding(1);
            this.btnToggleAccountSidDisplay.Size = new System.Drawing.Size(60, 30);
            this.btnToggleAccountSidDisplay.TabIndex = 32;
            this.btnToggleAccountSidDisplay.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnToggleAccountSidDisplay.UseVisualStyleBackColor = false;
            this.btnToggleAccountSidDisplay.Click += new System.EventHandler(this.btnToggleAccountSidDisplay_Click);
            // 
            // btnToggleAuthTokenDisplay
            // 
            this.btnToggleAuthTokenDisplay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnToggleAuthTokenDisplay.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.btnToggleAuthTokenDisplay.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnToggleAuthTokenDisplay.FlatAppearance.BorderSize = 0;
            this.btnToggleAuthTokenDisplay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnToggleAuthTokenDisplay.Image = global::Nuntius.App.Properties.Resources.text_hide_white_45;
            this.btnToggleAuthTokenDisplay.Location = new System.Drawing.Point(651, 89);
            this.btnToggleAuthTokenDisplay.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.btnToggleAuthTokenDisplay.Name = "btnToggleAuthTokenDisplay";
            this.btnToggleAuthTokenDisplay.Padding = new System.Windows.Forms.Padding(1);
            this.btnToggleAuthTokenDisplay.Size = new System.Drawing.Size(60, 30);
            this.btnToggleAuthTokenDisplay.TabIndex = 31;
            this.btnToggleAuthTokenDisplay.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnToggleAuthTokenDisplay.UseVisualStyleBackColor = false;
            this.btnToggleAuthTokenDisplay.Click += new System.EventHandler(this.btnToggleAuthTokenDisplay_Click);
            // 
            // lblPreferredBroadcaster
            // 
            this.lblPreferredBroadcaster.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPreferredBroadcaster.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblPreferredBroadcaster.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Italic);
            this.lblPreferredBroadcaster.Location = new System.Drawing.Point(119, 242);
            this.lblPreferredBroadcaster.Name = "lblPreferredBroadcaster";
            this.lblPreferredBroadcaster.Size = new System.Drawing.Size(593, 23);
            this.lblPreferredBroadcaster.TabIndex = 30;
            this.lblPreferredBroadcaster.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblPreferredBroadcaster.Click += new System.EventHandler(this.lblPreferredBroadcaster_Click);
            // 
            // txtTwilioNumber
            // 
            this.txtTwilioNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTwilioNumber.Font = new System.Drawing.Font("Bahnschrift", 14F);
            this.txtTwilioNumber.Location = new System.Drawing.Point(117, 146);
            this.txtTwilioNumber.Name = "txtTwilioNumber";
            this.txtTwilioNumber.Size = new System.Drawing.Size(594, 30);
            this.txtTwilioNumber.TabIndex = 28;
            // 
            // lblGetTwilioPhoneNumber
            // 
            this.lblGetTwilioPhoneNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblGetTwilioPhoneNumber.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblGetTwilioPhoneNumber.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Italic);
            this.lblGetTwilioPhoneNumber.Location = new System.Drawing.Point(118, 179);
            this.lblGetTwilioPhoneNumber.Name = "lblGetTwilioPhoneNumber";
            this.lblGetTwilioPhoneNumber.Size = new System.Drawing.Size(591, 23);
            this.lblGetTwilioPhoneNumber.TabIndex = 27;
            this.lblGetTwilioPhoneNumber.Text = "Your Twilio Phone Number. Click here to find out how to get your Twilio Phone Num" +
    "ber.";
            this.lblGetTwilioPhoneNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblGetTwilioPhoneNumber.Click += new System.EventHandler(this.lblGetTwilioPhoneNumber_Click);
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(12, 148);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 30);
            this.label7.TabIndex = 26;
            this.label7.Text = "Twilio Number";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblGetTwilioAuthToken
            // 
            this.lblGetTwilioAuthToken.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblGetTwilioAuthToken.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblGetTwilioAuthToken.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Italic);
            this.lblGetTwilioAuthToken.Location = new System.Drawing.Point(120, 122);
            this.lblGetTwilioAuthToken.Name = "lblGetTwilioAuthToken";
            this.lblGetTwilioAuthToken.Size = new System.Drawing.Size(591, 23);
            this.lblGetTwilioAuthToken.TabIndex = 24;
            this.lblGetTwilioAuthToken.Text = "Your Twilio Account Authentication Token. Click here to find out how to get your " +
    "Authentication Token.";
            this.lblGetTwilioAuthToken.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblGetTwilioAuthToken.Click += new System.EventHandler(this.lblGetTwilioAuthToken_Click);
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(12, 89);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 30);
            this.label5.TabIndex = 23;
            this.label5.Text = "Auth Token";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtAuthToken
            // 
            this.txtAuthToken.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAuthToken.Font = new System.Drawing.Font("Bahnschrift", 14F);
            this.txtAuthToken.Location = new System.Drawing.Point(118, 89);
            this.txtAuthToken.Name = "txtAuthToken";
            this.txtAuthToken.Size = new System.Drawing.Size(530, 30);
            this.txtAuthToken.TabIndex = 22;
            this.txtAuthToken.UseSystemPasswordChar = true;
            // 
            // lblGetTwilioAccountSid
            // 
            this.lblGetTwilioAccountSid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblGetTwilioAccountSid.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblGetTwilioAccountSid.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Italic);
            this.lblGetTwilioAccountSid.Location = new System.Drawing.Point(120, 63);
            this.lblGetTwilioAccountSid.Name = "lblGetTwilioAccountSid";
            this.lblGetTwilioAccountSid.Size = new System.Drawing.Size(591, 23);
            this.lblGetTwilioAccountSid.TabIndex = 21;
            this.lblGetTwilioAccountSid.Text = "Your Twilio Account Sid. Click here to find out how to get your Account Sid.";
            this.lblGetTwilioAccountSid.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblGetTwilioAccountSid.Click += new System.EventHandler(this.lblGetTwilioAccountSid_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Font = new System.Drawing.Font("Bahnschrift", 9F, System.Drawing.FontStyle.Italic);
            this.label3.Location = new System.Drawing.Point(120, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(593, 23);
            this.label3.TabIndex = 20;
            this.label3.Text = "Specify your Twilio account settings here. Find your Twilio settings at twilio.co" +
    "m/console";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(101)))), ((int)(((byte)(192)))));
            this.btnSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Image = global::Nuntius.App.Properties.Resources.save_white_45;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(611, 207);
            this.btnSave.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.btnSave.Name = "btnSave";
            this.btnSave.Padding = new System.Windows.Forms.Padding(1);
            this.btnSave.Size = new System.Drawing.Size(100, 30);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSaveContact_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 30);
            this.label1.TabIndex = 1;
            this.label1.Text = "Account Sid";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtAccountSid
            // 
            this.txtAccountSid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccountSid.Font = new System.Drawing.Font("Bahnschrift", 14F);
            this.txtAccountSid.Location = new System.Drawing.Point(118, 30);
            this.txtAccountSid.Name = "txtAccountSid";
            this.txtAccountSid.Size = new System.Drawing.Size(530, 30);
            this.txtAccountSid.TabIndex = 0;
            this.txtAccountSid.UseSystemPasswordChar = true;
            // 
            // lblControlHeader
            // 
            this.lblControlHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblControlHeader.BackColor = System.Drawing.Color.Transparent;
            this.lblControlHeader.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblControlHeader.Font = new System.Drawing.Font("Bahnschrift SemiBold", 13F, System.Drawing.FontStyle.Bold);
            this.lblControlHeader.Image = global::Nuntius.App.Properties.Resources.twilio_logo_red_45;
            this.lblControlHeader.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblControlHeader.Location = new System.Drawing.Point(0, 0);
            this.lblControlHeader.Margin = new System.Windows.Forms.Padding(0);
            this.lblControlHeader.Name = "lblControlHeader";
            this.lblControlHeader.Size = new System.Drawing.Size(791, 37);
            this.lblControlHeader.TabIndex = 6;
            this.lblControlHeader.Text = "Twilio Settings";
            this.lblControlHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TwilioSettingsUserControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(155)))), ((int)(((byte)(229)))));
            this.Controls.Add(this.ContainerPanel);
            this.Controls.Add(this.lblControlHeader);
            this.Font = new System.Drawing.Font("Bahnschrift", 9F);
            this.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.Name = "TwilioSettingsUserControl";
            this.Size = new System.Drawing.Size(791, 623);
            this.ContainerPanel.ResumeLayout(false);
            this.ContainerPanel.PerformLayout();
            this.grpTestMessageHolder.ResumeLayout(false);
            this.grpTestMessageHolder.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblControlHeader;
        private System.Windows.Forms.Panel ContainerPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtAccountSid;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblGetTwilioAuthToken;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtAuthToken;
        private System.Windows.Forms.Label lblGetTwilioAccountSid;
        private System.Windows.Forms.Label lblGetTwilioPhoneNumber;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtTwilioNumber;
        private System.Windows.Forms.Label lblPreferredBroadcaster;
        private System.Windows.Forms.Button btnToggleAuthTokenDisplay;
        private System.Windows.Forms.Button btnToggleAccountSidDisplay;
        private System.Windows.Forms.GroupBox grpTestMessageHolder;
        private System.Windows.Forms.TextBox txtTestMessageNumber;
        private System.Windows.Forms.Button btnSendTestMessage;
        private System.Windows.Forms.Label lblTestMessageHeading;
    }
}
