﻿using System;
using System.Diagnostics;
using Nuntius.Entities;
using Nuntius.App.UI.Forms;
using Nuntius.Globals;
using Nuntius.App.UI.UserControls.BaseControls;
using Nuntius.App.Code;
using Nuntius.Services;
using Nuntius.Messenger;
using System.Threading.Tasks;

namespace Nuntius.App.UI.UserControls.Settings
{
    public partial class TwilioSettingsUserControl : NuntiusControl
    {
        #region Define as Singleton
        private static TwilioSettingsUserControl _Instance;

        public static TwilioSettingsUserControl Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new TwilioSettingsUserControl();
                }

                return (_Instance);
            }
        }

        private TwilioSettingsUserControl()
        {
            InitializeComponent();
        }
        #endregion

        public Configuration TwilioAccountSid { get; set; }
        public Configuration TwilioAuthToken { get; set; }
        public Configuration TwilioFromNumber { get; set; }
        public Configuration PrefferedBroadcaster { get; set; }

        public new void Load()
        {
            base.Load();

            DashboardForm.Instance.DisplayLoader();
            this.Enabled = false;
            
            txtAccountSid.Text = Configurations.TwilioAccountSid.ToString();
            txtAuthToken.Text = Configurations.TwilioAuthToken.ToString();
            txtTwilioNumber.Text = Configurations.TwilioFromNumber.ToString();
            lblPreferredBroadcaster.Text = string.Format(Messages.PreferredBroadcasterMessage, ((BroadcasterEnums)(Configurations.PrefferredBroadcaster)).ToString());

            this.Enabled = true;
            DashboardForm.Instance.DisplayLoader(false);
        }

        private async void btnSaveContact_Click(object sender, EventArgs e)
        {
            var accountSID = txtAccountSid.Text.Trim();
            var authToken = txtAuthToken.Text.Trim();
            var number = txtTwilioNumber.Text.Trim();

            if (string.IsNullOrEmpty(accountSID) || string.IsNullOrEmpty(authToken) || string.IsNullOrEmpty(number))
            {
                DashboardForm.Instance.ShowErrorMessage(Messages.ValueFormatIssue);
                return;
            }

            DashboardForm.Instance.DisplayLoader();
            this.Enabled = false;

            var TwilioAccountSidConfig = await ConfigurationsService.Instance.UpdateConfiguration(new Configuration() { ID = (int)ConfigurationEnums.TwilioAccountSid, Value = accountSID });
            var TwilioAuthTokenConfig = await ConfigurationsService.Instance.UpdateConfiguration(new Configuration() { ID = (int)ConfigurationEnums.TwilioAuthToken, Value = authToken });
            var TwilioFromNumberConfig = await ConfigurationsService.Instance.UpdateConfiguration(new Configuration() { ID = (int)ConfigurationEnums.TwilioFromNumber, Value = number });

            Configurations.LoadConfigurations();

            this.Enabled = true;
            DashboardForm.Instance.DisplayLoader(false);

            DashboardForm.Instance.ShowSuccessMessage(Messages.TwilioSettingsUpdatedSuccess);
            Load();
        }

        private void lblGetTwilioAccountSid_Click(object sender, EventArgs e)
        {
            Process.Start(Configurations.TwilioAccountSidURL);
        }

        private void lblGetTwilioAuthToken_Click(object sender, EventArgs e)
        {
            Process.Start(Configurations.TwilioAuthTokenURL);
        }

        private void lblGetTwilioPhoneNumber_Click(object sender, EventArgs e)
        {
            Process.Start(Configurations.TwilioFromNumberURL);
        }

        private void lblPreferredBroadcaster_Click(object sender, EventArgs e)
        {
            SMSSettingsUserControl.Instance.Load();
        }

        private void btnToggleAccountSidDisplay_Click(object sender, EventArgs e)
        {
            if(txtAccountSid.UseSystemPasswordChar)
            {
                txtAccountSid.UseSystemPasswordChar = false;
                btnToggleAccountSidDisplay.Image = Properties.Resources.text_show_white_45;
            }
            else
            {
                txtAccountSid.UseSystemPasswordChar = true;
                btnToggleAccountSidDisplay.Image = Properties.Resources.text_hide_white_45;
            }
        }

        private void btnToggleAuthTokenDisplay_Click(object sender, EventArgs e)
        {
            if (txtAuthToken.UseSystemPasswordChar)
            {
                txtAuthToken.UseSystemPasswordChar = false;
                btnToggleAuthTokenDisplay.Image = Properties.Resources.text_show_white_45;
            }
            else
            {
                txtAuthToken.UseSystemPasswordChar = true;
                btnToggleAuthTokenDisplay.Image = Properties.Resources.text_hide_white_45;
            }
        }

        private async void btnSendTestMessage_Click(object sender, EventArgs e)
        {
            var number = txtTestMessageNumber.Text.Trim();

            if(string.IsNullOrEmpty(number))
            {
                DashboardForm.Instance.ShowWarningMessage(Messages.ContactNumbersNeeded);
                return;
            }

            DashboardForm.Instance.DisplayLoader();
            this.Enabled = false;

            var messageResponse = await SendTestMessage(number);

            this.Enabled = true;
            DashboardForm.Instance.DisplayLoader(false);

            if (!string.IsNullOrEmpty(messageResponse))
            {
                DashboardForm.Instance.ShowSuccessMessage(Messages.TestMessageSent);
            }
            else
            {
                DashboardForm.Instance.ShowErrorMessage(Messages.TestMessageNotSent);
            }
        }

        public Task<string> SendTestMessage(string number)
        {
            return Task.Run(() =>
            {
                return TwilioMessenger.SendMessage(Messages.NuntiusTestMessageContent, number);
            });

        }
    }
}
