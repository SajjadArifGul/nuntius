namespace Nuntius.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initialized : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Broadcasts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Status = c.String(),
                        StartTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Messages",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                        Status = c.Boolean(nullable: false),
                        SentTime = c.DateTime(),
                        Contact_ID = c.Int(),
                        Broadcast_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Contacts", t => t.Contact_ID)
                .ForeignKey("dbo.Broadcasts", t => t.Broadcast_ID)
                .Index(t => t.Contact_ID)
                .Index(t => t.Broadcast_ID);
            
            CreateTable(
                "dbo.Contacts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Group_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Groups", t => t.Group_ID)
                .Index(t => t.Group_ID);
            
            CreateTable(
                "dbo.Numbers",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Num = c.String(),
                        Contact_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Contacts", t => t.Contact_ID)
                .Index(t => t.Contact_ID);
            
            CreateTable(
                "dbo.Groups",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Contacts", "Group_ID", "dbo.Groups");
            DropForeignKey("dbo.Messages", "Broadcast_ID", "dbo.Broadcasts");
            DropForeignKey("dbo.Messages", "Contact_ID", "dbo.Contacts");
            DropForeignKey("dbo.Numbers", "Contact_ID", "dbo.Contacts");
            DropIndex("dbo.Numbers", new[] { "Contact_ID" });
            DropIndex("dbo.Contacts", new[] { "Group_ID" });
            DropIndex("dbo.Messages", new[] { "Broadcast_ID" });
            DropIndex("dbo.Messages", new[] { "Contact_ID" });
            DropTable("dbo.Groups");
            DropTable("dbo.Numbers");
            DropTable("dbo.Contacts");
            DropTable("dbo.Messages");
            DropTable("dbo.Broadcasts");
        }
    }
}
