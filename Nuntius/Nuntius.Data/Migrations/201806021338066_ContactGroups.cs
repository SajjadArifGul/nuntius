namespace Nuntius.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ContactGroups : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Contacts", "Group_ID", "dbo.Groups");
            DropIndex("dbo.Contacts", new[] { "Group_ID" });
            CreateTable(
                "dbo.GroupContacts",
                c => new
                    {
                        Group_ID = c.Int(nullable: false),
                        Contact_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Group_ID, t.Contact_ID })
                .ForeignKey("dbo.Groups", t => t.Group_ID, cascadeDelete: true)
                .ForeignKey("dbo.Contacts", t => t.Contact_ID, cascadeDelete: true)
                .Index(t => t.Group_ID)
                .Index(t => t.Contact_ID);
            
            DropColumn("dbo.Contacts", "Group_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Contacts", "Group_ID", c => c.Int());
            DropForeignKey("dbo.GroupContacts", "Contact_ID", "dbo.Contacts");
            DropForeignKey("dbo.GroupContacts", "Group_ID", "dbo.Groups");
            DropIndex("dbo.GroupContacts", new[] { "Contact_ID" });
            DropIndex("dbo.GroupContacts", new[] { "Group_ID" });
            DropTable("dbo.GroupContacts");
            CreateIndex("dbo.Contacts", "Group_ID");
            AddForeignKey("dbo.Contacts", "Group_ID", "dbo.Groups", "ID");
        }
    }
}
