namespace Nuntius.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class messageStatusesAdded : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Broadcasts", "Status", c => c.Int(nullable: false));
            AlterColumn("dbo.Messages", "Status", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Messages", "Status", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Broadcasts", "Status", c => c.String());
        }
    }
}
