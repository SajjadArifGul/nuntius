// <auto-generated />
namespace Nuntius.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class messagetextFielded : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(messagetextFielded));
        
        string IMigrationMetadata.Id
        {
            get { return "201806061531300_messagetextFielded"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
