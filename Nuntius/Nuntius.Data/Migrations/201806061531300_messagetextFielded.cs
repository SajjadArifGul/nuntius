namespace Nuntius.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class messagetextFielded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Broadcasts", "Text", c => c.String());
            AddColumn("dbo.Broadcasts", "CompletedTime", c => c.DateTime());
            DropColumn("dbo.Messages", "Text");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Messages", "Text", c => c.String());
            DropColumn("dbo.Broadcasts", "CompletedTime");
            DropColumn("dbo.Broadcasts", "Text");
        }
    }
}
