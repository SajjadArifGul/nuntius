namespace Nuntius.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class configvalue : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Configurations", "Value", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Configurations", "Value");
        }
    }
}
