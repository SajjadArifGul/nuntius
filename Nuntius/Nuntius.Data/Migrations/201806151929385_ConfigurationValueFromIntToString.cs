namespace Nuntius.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ConfigurationValueFromIntToString : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Configurations", "Value", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Configurations", "Value", c => c.Int(nullable: false));
        }
    }
}
