namespace Nuntius.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class messageEntityDetails : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Messages", "BroadcasterID", c => c.Int(nullable: false));
            AddColumn("dbo.Messages", "Result", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Messages", "Result");
            DropColumn("dbo.Messages", "BroadcasterID");
        }
    }
}
