namespace Nuntius.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class broadcasters : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Broadcasters",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Broadcasters");
        }
    }
}
