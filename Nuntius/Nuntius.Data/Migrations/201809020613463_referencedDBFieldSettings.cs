namespace Nuntius.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class referencedDBFieldSettings : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Messages", "Broadcast_ID", "dbo.Broadcasts");
            DropForeignKey("dbo.Messages", "Contact_ID", "dbo.Contacts");
            DropForeignKey("dbo.Numbers", "Contact_ID", "dbo.Contacts");
            DropIndex("dbo.Messages", new[] { "Contact_ID" });
            DropIndex("dbo.Messages", new[] { "Broadcast_ID" });
            DropIndex("dbo.Numbers", new[] { "Contact_ID" });
            RenameColumn(table: "dbo.Messages", name: "Broadcast_ID", newName: "BroadcastID");
            RenameColumn(table: "dbo.Messages", name: "Contact_ID", newName: "ContactID");
            RenameColumn(table: "dbo.Numbers", name: "Contact_ID", newName: "ContactID");
            AddColumn("dbo.Broadcasts", "Contact_ID", c => c.Int());
            AddColumn("dbo.Numbers", "Value", c => c.String());
            AddColumn("dbo.Numbers", "Note", c => c.String());
            AlterColumn("dbo.Messages", "ContactID", c => c.Int(nullable: false));
            AlterColumn("dbo.Messages", "BroadcastID", c => c.Int(nullable: false));
            AlterColumn("dbo.Numbers", "ContactID", c => c.Int(nullable: false));
            CreateIndex("dbo.Broadcasts", "Contact_ID");
            CreateIndex("dbo.Messages", "BroadcastID");
            CreateIndex("dbo.Messages", "ContactID");
            CreateIndex("dbo.Messages", "BroadcasterID");
            CreateIndex("dbo.Numbers", "ContactID");
            AddForeignKey("dbo.Messages", "BroadcasterID", "dbo.Broadcasters", "ID", cascadeDelete: true);
            AddForeignKey("dbo.Broadcasts", "Contact_ID", "dbo.Contacts", "ID");
            AddForeignKey("dbo.Messages", "BroadcastID", "dbo.Broadcasts", "ID", cascadeDelete: true);
            AddForeignKey("dbo.Messages", "ContactID", "dbo.Contacts", "ID", cascadeDelete: true);
            AddForeignKey("dbo.Numbers", "ContactID", "dbo.Contacts", "ID", cascadeDelete: true);
            DropColumn("dbo.Numbers", "Num");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Numbers", "Num", c => c.String());
            DropForeignKey("dbo.Numbers", "ContactID", "dbo.Contacts");
            DropForeignKey("dbo.Messages", "ContactID", "dbo.Contacts");
            DropForeignKey("dbo.Messages", "BroadcastID", "dbo.Broadcasts");
            DropForeignKey("dbo.Broadcasts", "Contact_ID", "dbo.Contacts");
            DropForeignKey("dbo.Messages", "BroadcasterID", "dbo.Broadcasters");
            DropIndex("dbo.Numbers", new[] { "ContactID" });
            DropIndex("dbo.Messages", new[] { "BroadcasterID" });
            DropIndex("dbo.Messages", new[] { "ContactID" });
            DropIndex("dbo.Messages", new[] { "BroadcastID" });
            DropIndex("dbo.Broadcasts", new[] { "Contact_ID" });
            AlterColumn("dbo.Numbers", "ContactID", c => c.Int());
            AlterColumn("dbo.Messages", "BroadcastID", c => c.Int());
            AlterColumn("dbo.Messages", "ContactID", c => c.Int());
            DropColumn("dbo.Numbers", "Note");
            DropColumn("dbo.Numbers", "Value");
            DropColumn("dbo.Broadcasts", "Contact_ID");
            RenameColumn(table: "dbo.Numbers", name: "ContactID", newName: "Contact_ID");
            RenameColumn(table: "dbo.Messages", name: "ContactID", newName: "Contact_ID");
            RenameColumn(table: "dbo.Messages", name: "BroadcastID", newName: "Broadcast_ID");
            CreateIndex("dbo.Numbers", "Contact_ID");
            CreateIndex("dbo.Messages", "Broadcast_ID");
            CreateIndex("dbo.Messages", "Contact_ID");
            AddForeignKey("dbo.Numbers", "Contact_ID", "dbo.Contacts", "ID");
            AddForeignKey("dbo.Messages", "Contact_ID", "dbo.Contacts", "ID");
            AddForeignKey("dbo.Messages", "Broadcast_ID", "dbo.Broadcasts", "ID");
        }
    }
}
