namespace Nuntius.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removedContactBroadcastWrongRelation : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Broadcasts", "Contact_ID", "dbo.Contacts");
            DropIndex("dbo.Broadcasts", new[] { "Contact_ID" });
            DropColumn("dbo.Broadcasts", "Contact_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Broadcasts", "Contact_ID", c => c.Int());
            CreateIndex("dbo.Broadcasts", "Contact_ID");
            AddForeignKey("dbo.Broadcasts", "Contact_ID", "dbo.Contacts", "ID");
        }
    }
}
