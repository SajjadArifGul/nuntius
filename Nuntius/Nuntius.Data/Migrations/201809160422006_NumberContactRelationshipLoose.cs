namespace Nuntius.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NumberContactRelationshipLoose : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Numbers", "ContactID", "dbo.Contacts");
            DropIndex("dbo.Numbers", new[] { "ContactID" });
            RenameColumn(table: "dbo.Numbers", name: "ContactID", newName: "Contact_ID");
            AlterColumn("dbo.Numbers", "Contact_ID", c => c.Int());
            CreateIndex("dbo.Numbers", "Contact_ID");
            AddForeignKey("dbo.Numbers", "Contact_ID", "dbo.Contacts", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Numbers", "Contact_ID", "dbo.Contacts");
            DropIndex("dbo.Numbers", new[] { "Contact_ID" });
            AlterColumn("dbo.Numbers", "Contact_ID", c => c.Int(nullable: false));
            RenameColumn(table: "dbo.Numbers", name: "Contact_ID", newName: "ContactID");
            CreateIndex("dbo.Numbers", "ContactID");
            AddForeignKey("dbo.Numbers", "ContactID", "dbo.Contacts", "ID", cascadeDelete: true);
        }
    }
}
