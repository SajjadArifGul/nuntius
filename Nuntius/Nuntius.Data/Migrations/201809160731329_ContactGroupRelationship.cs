namespace Nuntius.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ContactGroupRelationship : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.GroupContacts", "Group_ID", "dbo.Groups");
            DropForeignKey("dbo.GroupContacts", "Contact_ID", "dbo.Contacts");
            DropIndex("dbo.GroupContacts", new[] { "Group_ID" });
            DropIndex("dbo.GroupContacts", new[] { "Contact_ID" });
            DropPrimaryKey("dbo.GroupContacts");
            AddColumn("dbo.Groups", "Contact_ID", c => c.Int());
            AddColumn("dbo.GroupContacts", "ID", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.GroupContacts", "Group_ID", c => c.Int());
            AlterColumn("dbo.GroupContacts", "Contact_ID", c => c.Int());
            AddPrimaryKey("dbo.GroupContacts", "ID");
            CreateIndex("dbo.Groups", "Contact_ID");
            CreateIndex("dbo.GroupContacts", "Contact_ID");
            CreateIndex("dbo.GroupContacts", "Group_ID");
            AddForeignKey("dbo.Groups", "Contact_ID", "dbo.Contacts", "ID");
            AddForeignKey("dbo.GroupContacts", "Group_ID", "dbo.Groups", "ID");
            AddForeignKey("dbo.GroupContacts", "Contact_ID", "dbo.Contacts", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.GroupContacts", "Contact_ID", "dbo.Contacts");
            DropForeignKey("dbo.GroupContacts", "Group_ID", "dbo.Groups");
            DropForeignKey("dbo.Groups", "Contact_ID", "dbo.Contacts");
            DropIndex("dbo.GroupContacts", new[] { "Group_ID" });
            DropIndex("dbo.GroupContacts", new[] { "Contact_ID" });
            DropIndex("dbo.Groups", new[] { "Contact_ID" });
            DropPrimaryKey("dbo.GroupContacts");
            AlterColumn("dbo.GroupContacts", "Contact_ID", c => c.Int(nullable: false));
            AlterColumn("dbo.GroupContacts", "Group_ID", c => c.Int(nullable: false));
            DropColumn("dbo.GroupContacts", "ID");
            DropColumn("dbo.Groups", "Contact_ID");
            AddPrimaryKey("dbo.GroupContacts", new[] { "Group_ID", "Contact_ID" });
            CreateIndex("dbo.GroupContacts", "Contact_ID");
            CreateIndex("dbo.GroupContacts", "Group_ID");
            AddForeignKey("dbo.GroupContacts", "Contact_ID", "dbo.Contacts", "ID", cascadeDelete: true);
            AddForeignKey("dbo.GroupContacts", "Group_ID", "dbo.Groups", "ID", cascadeDelete: true);
        }
    }
}
