namespace Nuntius.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ContactGroupRelationship1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Groups", "Contact_ID", "dbo.Contacts");
            DropIndex("dbo.Groups", new[] { "Contact_ID" });
            DropColumn("dbo.Groups", "Contact_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Groups", "Contact_ID", c => c.Int());
            CreateIndex("dbo.Groups", "Contact_ID");
            AddForeignKey("dbo.Groups", "Contact_ID", "dbo.Contacts", "ID");
        }
    }
}
