namespace Nuntius.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class stfu : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.GroupContacts", "Group_ID", "dbo.Groups");
            DropForeignKey("dbo.GroupContacts", "Contact_ID", "dbo.Contacts");
            DropIndex("dbo.GroupContacts", new[] { "Contact_ID" });
            DropIndex("dbo.GroupContacts", new[] { "Group_ID" });
            RenameColumn(table: "dbo.GroupContacts", name: "Group_ID", newName: "GroupID");
            RenameColumn(table: "dbo.GroupContacts", name: "Contact_ID", newName: "ContactID");
            AlterColumn("dbo.GroupContacts", "ContactID", c => c.Int(nullable: false));
            AlterColumn("dbo.GroupContacts", "GroupID", c => c.Int(nullable: false));
            CreateIndex("dbo.GroupContacts", "ContactID");
            CreateIndex("dbo.GroupContacts", "GroupID");
            AddForeignKey("dbo.GroupContacts", "GroupID", "dbo.Groups", "ID", cascadeDelete: true);
            AddForeignKey("dbo.GroupContacts", "ContactID", "dbo.Contacts", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.GroupContacts", "ContactID", "dbo.Contacts");
            DropForeignKey("dbo.GroupContacts", "GroupID", "dbo.Groups");
            DropIndex("dbo.GroupContacts", new[] { "GroupID" });
            DropIndex("dbo.GroupContacts", new[] { "ContactID" });
            AlterColumn("dbo.GroupContacts", "GroupID", c => c.Int());
            AlterColumn("dbo.GroupContacts", "ContactID", c => c.Int());
            RenameColumn(table: "dbo.GroupContacts", name: "ContactID", newName: "Contact_ID");
            RenameColumn(table: "dbo.GroupContacts", name: "GroupID", newName: "Group_ID");
            CreateIndex("dbo.GroupContacts", "Group_ID");
            CreateIndex("dbo.GroupContacts", "Contact_ID");
            AddForeignKey("dbo.GroupContacts", "Contact_ID", "dbo.Contacts", "ID");
            AddForeignKey("dbo.GroupContacts", "Group_ID", "dbo.Groups", "ID");
        }
    }
}
