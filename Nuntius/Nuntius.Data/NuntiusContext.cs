﻿using Nuntius.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nuntius.Data
{
    public class NuntiusContext : DbContext, IDisposable
    {
        public NuntiusContext() : base("name=NuntiusConnection")
        {
            Database.SetInitializer<NuntiusContext>(new NuntiusDatabaseInitializer());
        }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Message>().Property(e => e.Contact).WithOptional(s => s.Parent).WillCascadeOnDelete(true);
        }

        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Number> Numbers { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<GroupContact> GroupContacts { get; set; }
        public DbSet<Broadcast> Broadcasts { get; set; }
        public DbSet<Broadcaster> Broadcasters { get; set; }
        public DbSet<Message> Message { get; set; }
        public DbSet<Configuration> Configurations { get; set; }
        public DbSet<FAQ> FAQs { get; set; }
    }
}
