﻿using Nuntius.Entities;
using Nuntius.Globals;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nuntius.Data
{
    public class NuntiusDatabaseInitializer : CreateDatabaseIfNotExists<NuntiusContext>
    {
        protected override void Seed(NuntiusContext context)
        {
            AddNuntiusDefaults(context);
            AddBroadcasters(context);
            SetSMSDefaultConfigurations(context);
            SetGSMCommDefaultConfigurations(context);
            SetTwilioDefaultConfigurations(context);

            context.SaveChanges();
        }

        public void AddNuntiusDefaults(NuntiusContext context)
        {
            var NuntiusVersion = context.Configurations.Find((int)ConfigurationEnums.NuntiusVersion);
            if (NuntiusVersion == null)
            {
                NuntiusVersion = new Configuration() { ID = (int)ConfigurationEnums.NuntiusVersion, Name = "NuntiusVersion", Value = "2" };

                NuntiusVersion = context.Configurations.Add(NuntiusVersion);
            }

            var SinglePageRecordsCount = context.Configurations.Find((int)ConfigurationEnums.SinglePageRecordsCount);
            if (SinglePageRecordsCount == null)
            {
                SinglePageRecordsCount = new Configuration() { ID = (int)ConfigurationEnums.SinglePageRecordsCount, Name = "SinglePageRecordsCount", Value = "10" };

                SinglePageRecordsCount = context.Configurations.Add(SinglePageRecordsCount);
            }
        }

        public void AddBroadcasters(NuntiusContext context)
        {
            var GSMBroadcaster = context.Broadcasters.Find((int)BroadcasterEnums.GSM);
            if (GSMBroadcaster == null)
            {
                GSMBroadcaster = new Entities.Broadcaster() { ID = (int)BroadcasterEnums.GSM, Name = "GSM" };

                GSMBroadcaster = context.Broadcasters.Add(GSMBroadcaster);
            }

            var TwilioBroadcaster = context.Broadcasters.Find((int)BroadcasterEnums.Twilio);
            if (TwilioBroadcaster == null)
            {
                TwilioBroadcaster = new Entities.Broadcaster() { ID = (int)BroadcasterEnums.Twilio, Name = "Twilio" };

                TwilioBroadcaster = context.Broadcasters.Add(TwilioBroadcaster);
            }
        }

        public void SetSMSDefaultConfigurations(NuntiusContext context)
        {
            var SMSTimeConfig = context.Configurations.Find((int)ConfigurationEnums.SMSTimeConfig);
            if (SMSTimeConfig == null)
            {
                SMSTimeConfig = new Configuration() { ID = (int)ConfigurationEnums.SMSTimeConfig, Name = "SMSTimeConfig", Value = Defaults.SMSTime.ToString() };

                SMSTimeConfig = context.Configurations.Add(SMSTimeConfig);
            }

            var SMSMessageCountConfig = context.Configurations.Find((int)ConfigurationEnums.SMSMessageCountConfig);
            if (SMSMessageCountConfig == null)
            {
                SMSMessageCountConfig = new Configuration() { ID = (int)ConfigurationEnums.SMSMessageCountConfig, Name = "SMSMessageCountConfig", Value = Defaults.SMSMessageCount.ToString() };

                SMSMessageCountConfig = context.Configurations.Add(SMSMessageCountConfig);
            }

            var PrefferedBroadcaster = context.Configurations.Find((int)ConfigurationEnums.PrefferedBroadcaster);
            if (PrefferedBroadcaster == null)
            {
                PrefferedBroadcaster = new Configuration() { ID = (int)ConfigurationEnums.PrefferedBroadcaster, Name = "PrefferedBroadcaster", Value = Defaults.PrefferredBroadcaster.ToString() };

                PrefferedBroadcaster = context.Configurations.Add(PrefferedBroadcaster);
            }
        }

        public void SetGSMCommDefaultConfigurations(NuntiusContext context)
        {
            var GSMBaudRateConfig = context.Configurations.Find((int)ConfigurationEnums.GSMBaudRateConfig);
            if (GSMBaudRateConfig == null)
            {
                GSMBaudRateConfig = new Configuration() { ID = (int)ConfigurationEnums.GSMBaudRateConfig, Name = "GSMBaudRateConfig", Value = Defaults.GSMBaudRate.ToString() };

                GSMBaudRateConfig = context.Configurations.Add(GSMBaudRateConfig);
            }

            var GSMTimeoutConfig = context.Configurations.Find((int)ConfigurationEnums.GSMTimeoutConfig);
            if (GSMTimeoutConfig == null)
            {
                GSMTimeoutConfig = new Configuration() { ID = (int)ConfigurationEnums.GSMTimeoutConfig, Name = "GSMTimeoutConfig", Value = Defaults.GSMTimeout.ToString() };

                GSMTimeoutConfig = context.Configurations.Add(GSMTimeoutConfig);
            }
        }

        public void SetTwilioDefaultConfigurations(NuntiusContext context)
        {
            var TwilioAccountSid = context.Configurations.Find((int)ConfigurationEnums.TwilioAccountSid);
            if (TwilioAccountSid == null)
            {
                TwilioAccountSid = new Configuration() { ID = (int)ConfigurationEnums.TwilioAccountSid, Name = "TwilioAccountSid", Value = Defaults.TwilioAccountSid.ToString() };

                TwilioAccountSid = context.Configurations.Add(TwilioAccountSid);
            }

            var TwilioAuthToken = context.Configurations.Find((int)ConfigurationEnums.TwilioAuthToken);
            if (TwilioAuthToken == null)
            {
                TwilioAuthToken = new Configuration() { ID = (int)ConfigurationEnums.TwilioAuthToken, Name = "TwilioAuthToken", Value = Defaults.TwilioAuthToken.ToString() };

                TwilioAuthToken = context.Configurations.Add(TwilioAuthToken);
            }

            var TwilioFromNumber = context.Configurations.Find((int)ConfigurationEnums.TwilioFromNumber);
            if (TwilioFromNumber == null)
            {
                TwilioFromNumber = new Configuration() { ID = (int)ConfigurationEnums.TwilioFromNumber, Name = "TwilioFromNumber", Value = Defaults.TwilioFromNumber.ToString() };

                TwilioFromNumber = context.Configurations.Add(TwilioFromNumber);
            }
        }
    }
}