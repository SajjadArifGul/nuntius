﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nuntius.Entities
{
    public class Broadcast : BaseEntity
    {
        public string Text { get; set; }
        public int Status { get; set; }

        public Nullable<DateTime> StartTime { get; set; }
        public Nullable<DateTime> CompletedTime { get; set; }

        public virtual List<Message> Messages { get; set; }
    }
}
