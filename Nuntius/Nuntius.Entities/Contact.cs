﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nuntius.Entities
{
    public class Contact : BaseEntity
    {
        [NotMapped]
        public string DisplayName { get { return !string.IsNullOrEmpty(Name) ? Name : "-"; } }

        public string Name { get; set; }

        /// <summary>
        /// Contact has these Numbers
        /// </summary>
        public virtual List<Number> Numbers { get; set; }

        public virtual List<GroupContact> GroupContacts { get; set; }
    }
}
