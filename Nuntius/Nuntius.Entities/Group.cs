﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nuntius.Entities
{
    public class Group : BaseEntity
    {
        [NotMapped]
        public string DisplayName { get { return !string.IsNullOrEmpty(Name) ? Name : "-"; } }

        public string Name { get; set; }

        public string Description { get; set; }

        public virtual List<GroupContact> GroupContacts { get; set; }
    }
}
