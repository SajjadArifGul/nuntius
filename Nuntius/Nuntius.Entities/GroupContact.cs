﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nuntius.Entities
{
    public class GroupContact : BaseEntity
    {
        [NotMapped]
        public string Name {
            get
            {
                if (Contact != null)
                {
                    return Contact.Name;
                }
                else return string.Empty;
            }
        }

        public int ContactID { get; set; }
        public virtual Contact Contact { get; set; }

        public int GroupID { get; set; }
        public virtual Group Group { get; set; }
    }
}
