﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nuntius.Entities
{
    public class Message : BaseEntity
    {
        public int BroadcastID { get; set; }
        public virtual Broadcast Broadcast { get; set; }

        public int ContactID { get; set; }
        public virtual Contact Contact { get; set; }

        public int BroadcasterID { get; set; }
        public virtual Broadcaster Broadcaster { get; set; }

        public int Status { get; set; }
        public Nullable<DateTime> SentTime { get; set; }
        public string Result { get; set; }
    }
}
