﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nuntius.Entities
{
    public class Number : BaseEntity
    {
        public string Value { get; set; }
        public string Note { get; set; }
        
        //public int? ContactID { get; set; }
        //public virtual Contact Contact { get; set; }
    }
}
