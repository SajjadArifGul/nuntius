﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nuntius.Globals
{
    public static class Defaults
    {
        public static int PrefferredBroadcaster { get { return (int)BroadcasterEnums.GSM; } }
        public static int SinglePageRecordsCount { get { return 10; } }
        public static int GSMTimeout { get { return 300; } }
        public static int GSMBaudRate { get { return 9600; } }
        public static string TwilioAccountSid { get { return string.Empty; } }
        public static string TwilioAuthToken { get { return string.Empty; } }
        public static string TwilioFromNumber { get { return string.Empty; } }
        public static int SMSTime { get { return 1; } }
        public static int SMSMessageCount { get { return 25; } }
        public static int NuntiusVersion { get { return 25; } }        
    }
}
