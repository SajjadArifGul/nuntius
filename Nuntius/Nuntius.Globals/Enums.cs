﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nuntius.Globals
{
    public enum BroadcasterEnums
    {
        GSM = 1,
        Twilio = 2
    }

    public enum MessageStatus
    {
        Pending = 1,
        Sent = 2,
        Cancelled = 3,
        Failed = 4
    }

    public enum BroadcastStatus
    {
        Pending = 1,
        Processing = 2,
        Completed = 3,
        Cancelled = 4,
        Failed = 5
    }

    public enum ConfigurationEnums
    {
        SMSTimeConfig = 1,
        SMSMessageCountConfig = 2,
        PrefferedBroadcaster = 3,
        GSMBaudRateConfig = 4,
        GSMTimeoutConfig = 5,
        TwilioAccountSid = 6,
        TwilioAuthToken = 7,
        TwilioFromNumber = 8,
        NuntiusVersion = 9,
        SinglePageRecordsCount = 10,
        GSMComPort = 11,
        AboutDetails = 12,
        IncludeMessageSignature = 13,
        MessageSignature = 14,
        ContactEmail = 15,
        ContactPhone = 16,
        ContactDetails = 17
    }
}
