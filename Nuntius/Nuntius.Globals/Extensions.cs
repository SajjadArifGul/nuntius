﻿using Nuntius.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Nuntius.Globals
{
    public static class Extensions
    {
        public static string JoinNumbers(this List<Number> numbers, string joiner)
        {
            if (numbers != null)
            {
                return string.Join(joiner, numbers.Select(x=>x.Value));
            }
            else return string.Empty;
        }

        public static int GetComPortNumber(this string value)
        {
            //Example : "HUAWEI MOBILE CONNECT - 3G PC UI INTERFACE (COM19)"
            int startIndex = value.IndexOf("(COM") + "(COM".Length;
            int endIndex = value.IndexOf(")");
            return Convert.ToInt32(value.Substring(startIndex, endIndex - startIndex));
        }

        public static string ToPhoneNumber(this string number)
        {
            if (!string.IsNullOrEmpty(number))
            {
                return new string(number.Where(c => char.IsDigit(c) || c == '+').ToArray());
            }
            else return string.Empty;
        }

        public static string MessageCharactersLabel(this string MessageText, int SingleMessageCharactersCount)
        {
            if (!string.IsNullOrEmpty(MessageText) && MessageText.Count() > 0)
            {
                return string.Format("Characters: {0}, Parts: {1}", MessageText.Count(), (MessageText.Count() / SingleMessageCharactersCount) + 1);
            }
            else return string.Empty;
        }

        public static IList<T> Clone<T>(this IList<T> listToClone) where T : ICloneable
        {
            return listToClone.Select(item => (T)item.Clone()).ToList();
        }

        public static string GetSubstringText(this string str, string start, string end)
        {
            try
            {
                var StartingIndex = !string.IsNullOrEmpty(start) ? str.IndexOf(start) + 1 : 0;

                var EndingIndex = !string.IsNullOrEmpty(end) ? str.IndexOf(end) : str.Length;

                var Length = EndingIndex - StartingIndex;

                return str.Substring(StartingIndex, Length).Trim();
            }
            catch
            {
                return null;
            }
        }

        public static List<Number> TextToNumbers(this string numbersText)
        {
            if (!string.IsNullOrEmpty(numbersText))
            {
                List<Number> numbers = new List<Number>();

                if (numbersText.Contains(','))
                {
                    foreach (var number in numbersText.Split(','))
                    {
                        numbers.Add(new Number() { Value = number.ToPhoneNumber() });
                    }
                }
                else
                {
                    numbers.Add(new Number() { Value = numbersText.ToPhoneNumber() });
                }

                return numbers;
            }

            return null;
        }

        public static string IfNullOrEmptyShowAlternative(this string text, string alternative)
        {
            return !string.IsNullOrEmpty(text) ? text : alternative;
        }

        public static string ConvertToString(this object obj)
        {
            if (obj != null)
            {
                var objType = obj.GetType();

                PropertyInfo[] _PropertyInfos = objType.GetProperties();

                if (_PropertyInfos != null)
                {
                    var sb = new StringBuilder();

                    foreach (var propertyInfo in _PropertyInfos)
                    {
                        var value = propertyInfo.GetValue(obj, null) ?? "(null)";
                        sb.AppendLine(propertyInfo.Name + ": " + value.ToString());


                        PropertyInfo[] _ChildPropertyInfos = propertyInfo.GetType().GetProperties();
                    }

                    return sb.ToString();
                }
            }

            return string.Empty;
        }
    }
}
