﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nuntius.Globals
{
    /// <summary>
    /// Messages to display to user
    /// </summary>
    public static class Messages
    {
        public static string NuntiusLoading = "Loading...";

        public static string ErrorOccurredMessage = "An error has occured. Please report to developer.\n\n{1}\n\nError{0}";

        public static string ErrorOccurredWithCustomMessage = "An error has occured.\n\nMessage: {0}";

        public static string BroadcastThreadStartErrorOccurredMessage = "An error has occured while processing broadcast.\n\n{1}\n\nError{0}";

        public static string BroadcastThreadStopErrorOccurredMessage = "An error has occured while cancelling broadcast.\n\n{1}\n\nError{0}";

        public static string ContactNameNeeded = "Please supply a valid contact name.";

        public static string ContactNumbersNeeded = "Please supply valid contact numbers.";

        public static string ContactAdded = "Contact added sucessfully.";

        public static string ContactSavedSuccess = "Contacts Saved sucessfully.";

        public static string NoContactsToSave = "No Contacts to save.";

        public static string ContactUpdated = "Contact updated sucessfully.";

        public static string ContactNotSaved = "Contact not saved.";

        public static string ContactNotUpdated = "Contact not updated.";

        public static string ContactConfirmDelete = "Are you sure you want to delete this Contact?";

        public static string ContactNotDeleted = "Contact not deleted.";

        public static string GroupContactsCount = "{0} contacts in this group.";
        
        public static string GroupAdded = "Group added sucessfully.";

        public static string GroupNotSaved = "Group not saved.";

        public static string GroupUpdated = "Group updated sucessfully.";

        public static string GroupNotUpdated = "Group not updated.";

        public static string GroupConfirmDelete = "Are you sure you want to delete this Group?";

        public static string GroupNotDeleted = "Group not deleted.";

        public static string BroadcastContactsCount = "{0} contacts in this broadcast.";

        public static string BroadcastCharactersCount = "{0} characters.";

        public static string BroadcastCompleted = "Broadcast sent completely.";

        public static string NoSignatureToInclude = "There is no signature text to add to messages.";

        public static string BroadcastAdded = "Broadcast added to queue.";

        public static string BroadcastNotAdded = "Unable to queue broadcast.";

        public static string ConfirmBroadcastResend = "Are you sure you want to resend this broadcast?";

        public static string ConfirmSettingsReset = "Are you sure you want to reset settings? Your current setting will be removed permanently.";

        public static string ConfirmBroadcastCancel = "Are you sure you want to cancel this broadcast?";

        public static string BroadcastNotCancelled = "Unable to cancel Broadcast.";

        public static string BroadcastNotResend = "Unable to add Broadcast to queue.";

        public static string BroadcastConfirmDelete = "Are you sure you want to delete this Broadcast?";

        public static string BroadcastNotDeleted = "Broadcast not deleted.";

        public static string BroadcastValidationFailed = "Please add valid broadcast massage and contacts.";

        public static string SettingsUpdatedSuccess = "Settings updated successfully.";

        public static string SettingsNoteUpdated = "Unable to updated settings.";

        public static string GSMModemSettingsUpdatedSuccess = "GSM Modem Settings updated successfully.";

        public static string GSMModemSettingsNoteUpdated = "Unable to updated GSM modem settings.";

        public static string TwilioSettingsUpdatedSuccess = "Twilio Settings updated successfully.";

        public static string TwilioSettingsNoteUpdated = "Unable to updated Twilio settings.";

        public static string PreferredBroadcasterMessage = "Your preferred broadcaster is {0}. Click here to change.";

        public static string SMSTimeConfigNotDefined = "SMS Waiting Time is not defined in settings. Please update in settings.";

        public static string SMSMessageCountConfigNotDefined = "SMS Message Count is not defined in settings. Please update in settings.";

        public static string PrefferedBroadcasterNotDefined = "Preferred Broadcaster is not defined. Please update in settings.";

        public static string ValueFormatIssue = "Values are not correctly formatted.";

        public static string ValueOtherIssue = "An error occured while reading values.";

        public static string UnableToReadFile = "Unable to read file content.";

        public static string UnableToWriteFile = "Unable to write file content.";

        public static string NoFormmatedContactsFound = "No formatted contacts found in the file.";

        public static string FileSaved = "File save to location.";

        public static string BroadcastAlreadyInProgress = "This broadcast is already in progress.";

        public static string BroadcastNotInProgress = "This broadcast is not in progress.";

        public static string FoundContactsInFile = "Found {0} contacts in the file.";

        public static string NumberOfContacts = "{0} contacts.";

        public static string NoResults = "No search results found.";

        public static string ContactsDisplayLabel = "Showing {0} - {1} of {2} contacts.";

        public static string GroupsDisplayLabel = "Showing {0} - {1} of {2} groups.";

        public static string BroadcastsDisplayLabel = "Showing {0} - {1} of {2} broadcasts.";

        public static string GSMMessageSuccess = "Message sent to {0} via GSM Modem. Message PDU = {1}.";

        public static string TwilioMessageSuccess = "Message sent to {0} via Twilio. Message SID = {1}.";

        public static string GSMMessageError = "Error sending message to {0} via GSM Modem. Error Message = {1}.";

        public static string TwilioMessageError = "Error sending message to {0} via Twilio. Error Message = {1}.";
        
        public static string TestMessageSent = "Test message sent successfully.";

        public static string TestMessageNotSent = "Test message sent successfully.";
        
        public static string NuntiusTestMessageContent = "Hi. This is a test message from Nuntius Message Broadcaster.";

        public static string MessageProcessCount = "{0}/{1} Messages processed.";

        public static string MessageProcessWaitTime = "Message process paused for {0} minutes.";
    }
}
