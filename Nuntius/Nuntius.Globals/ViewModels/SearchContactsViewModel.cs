﻿using Nuntius.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nuntius.Globals.ViewModels
{
    public class SearchContactsViewModel
    {
        public List<Contact> Contacts { get; set; }
        public int Count { get; set; }
    }
}
