﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nuntius.Messenger
{
    public abstract class BaseMessenger
    {
        public virtual string SendMessage() { return string.Empty; }
    }
}
