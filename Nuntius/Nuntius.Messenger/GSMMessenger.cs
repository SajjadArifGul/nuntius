﻿using GsmComm.GsmCommunication;
using GsmComm.PduConverter;
using Nuntius.Globals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nuntius.Messenger
{
    public static class GSMMessenger
    {
        static GsmCommMain comm = new GsmCommMain();

        public static int Comm_Port = 0;
        public static int Comm_BaudRate = 0;
        public static int Comm_TimeOut = 0;

        public static void Setup(int port, int baudRate, int timeOut)
        {
            Comm_Port = port;
            Comm_BaudRate = baudRate;
            Comm_TimeOut = timeOut;
        }

        public static string SendMessage(string message, string contactNumber)
        {
            try
            {
                var pdu = new SmsSubmitPdu(message, contactNumber);

                comm.SendMessage(pdu);

                return string.Format(Messages.GSMMessageSuccess, contactNumber, pdu.ToString());
            }
            catch (Exception ex)
            {
                return string.Format(Messages.GSMMessageError, contactNumber, ex.Message);
            }
        }

        public static bool ConnectionStatus()
        {
            return comm.IsOpen() && comm.IsConnected();
        }

        public static bool Connect()
        {
            if (!ConnectionStatus())
            {
                comm = new GsmCommMain(Comm_Port, Comm_BaudRate, Comm_TimeOut);
                comm.Open();
            }

            return ConnectionStatus();
        }

        public static bool Disconnect()
        {
            if (ConnectionStatus())
            {
                comm.Close();
            }

            return ConnectionStatus();
        }

        public static string SendTestMessage(string message, string contactNumber)
        {
            var pdu = new SmsSubmitPdu(message, contactNumber);

            comm.SendMessage(pdu);

            return string.Format(Messages.GSMMessageSuccess, contactNumber, pdu.ToString());
        }
    }
}
