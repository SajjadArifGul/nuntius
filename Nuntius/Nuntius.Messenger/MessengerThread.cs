﻿using Nuntius.Globals;
using Nuntius.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Nuntius.Messenger
{
    public class MessengerThread
    {
        public static void Execute()
        {

            int normalSleepTimeInMilliSeconds = 60000;

            do
            {
                try
                {
                    //SendMessages();

                    StartPendingBroadcasts();
                }
                catch
                {
                    System.Threading.Thread.Sleep(normalSleepTimeInMilliSeconds / 10);

                    continue;
                }

                System.Threading.Thread.Sleep(normalSleepTimeInMilliSeconds);

            } while (true);
        }

        private static async void StartPendingBroadcasts()
        {
            try
            {
                var pendingBroadcasts = await BroadcastsService.Instance.GetPendingBroadcasts();

                foreach (var broadcast in pendingBroadcasts)
                {
                    try
                    {
                        broadcast.StartTime = DateTime.Now;
                        broadcast.Status = (int)BroadcastStatus.Processing;

                        if (await BroadcastsService.Instance.UpdateBroadcast(broadcast))
                        {
                            foreach (var message in broadcast.Messages.Where(x => x.Status == (int)MessageStatus.Pending))
                            {
                                try
                                {
                                    if (message.Status == (int)MessageStatus.Pending) //this check is added because maybe after the list was initially taken, the message has been cancelled etc
                                    {
                                        //send message here
                                        //Thread.Sleep(1000); //dummy. As if message is being sent.

                                        //also keep in mind user setting for Wait Time while sending messages

                                        message.Status = (int)MessageStatus.Sent;
                                        message.SentTime = DateTime.Now;
                                        message.Result = ""; //Maybe update some error or other stuff from broadcaster

                                        //await MessagesService.Instance.UpdateMessage(message);
                                    }
                                }
                                catch
                                {
                                    //continue or set message status to some errror

                                    //message.Result = 
                                }
                            }

                            broadcast.Status = (int)BroadcastStatus.Completed;
                            broadcast.CompletedTime = DateTime.Now;

                            await BroadcastsService.Instance.UpdateBroadcast(broadcast);
                        }
                    }
                    catch
                    {
                        //continue
                    }
                }

            }
            catch
            {
                //continue for threads to work properly
            }
        }

        private static async void SendMessages()
        {
            var pendingMessages = await MessagesService.Instance.GetPendingMessages();

            if (pendingMessages != null && pendingMessages.Count > 0)
            {
                foreach (var message in pendingMessages)
                {
                    if (message.Status == (int)MessageStatus.Pending) //this check is added because maybe after the list was initially taken, the message has been cancelled etc
                    {
                        //send message here
                        Thread.Sleep(1000); //dummy. As if message is being sent.

                        //also keep in mind user setting for Wait Time while sending messages

                        message.Status = (int)MessageStatus.Sent;
                        message.SentTime = DateTime.Now;
                        message.Result = ""; //Maybe update some error or other stuff from broadcaster

                        await MessagesService.Instance.UpdateMessage(message);
                    }
                }
            }
        }
    }
}
