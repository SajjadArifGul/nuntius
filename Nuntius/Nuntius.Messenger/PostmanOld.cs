﻿using Nuntius.Entities;
using Nuntius.Globals;
using Nuntius.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Nuntius.Messenger
{
    public abstract class PostmanOld
    {
        static int normalSleepTimeInMilliSeconds = 3000;
        public static int broadcastID = 0;
        public static BroadcasterEnums PreferredBroadcaster = BroadcasterEnums.GSM;

        static int doCount = 1;

        /// <summary>
        /// Execute just once.  Should be used with BroadcastID
        /// </summary>
        public async static void ExecuteOnce()
        {
            LoggerService.Instance.LogMessage(string.Format("Calling - StartPendingBroadcasts only once."));

            var result = await StartPendingBroadcasts();

            LoggerService.Instance.LogMessage(string.Format("Called - StartPendingBroadcasts Once with result = {0}.", result));

            System.Threading.Thread.Sleep(normalSleepTimeInMilliSeconds);

            LoggerService.Instance.LogMessage("Slept Once - normalSleepTimeInMilliSeconds.");
        }

        /// <summary>
        /// Continuously Executes
        /// </summary>
        public async static void Execute()
        {
            doCount = 1;

            do
            {
                LoggerService.Instance.LogMessage(string.Format("Calling - StartPendingBroadcasts {0} time.", doCount));

                var result = await StartPendingBroadcasts();

                LoggerService.Instance.LogMessage(string.Format("Called - StartPendingBroadcasts with result = {0}.", result));

                System.Threading.Thread.Sleep(normalSleepTimeInMilliSeconds);

                LoggerService.Instance.LogMessage("Slept - normalSleepTimeInMilliSeconds.");

                doCount++;
            } while (true);
        }

        private static async Task<bool> StartPendingBroadcasts()
        {
            try
            {
                var pendingBroadcasts = new List<Broadcast>();

                LoggerService.Instance.LogMessage("Before awaiting pendingBroadcasts.");

                if(broadcastID != 0)
                {
                    var broadcast = await BroadcastsService.Instance.GetBroadcastByID(broadcastID);

                    if (broadcast != null) pendingBroadcasts.Add(broadcast);
                }
                else
                {
                    pendingBroadcasts = await BroadcastsService.Instance.GetPendingBroadcasts();
                }

                LoggerService.Instance.LogMessage(string.Format("Result pendingBroadcasts found {0} broadcasts.", pendingBroadcasts.Count));

                foreach (var broadcast in pendingBroadcasts)
                {
                    try
                    {
                        LoggerService.Instance.LogMessage(string.Format("Preparing Broadcast with ID: {0}", broadcast.ID));

                        broadcast.StartTime = DateTime.Now;
                        broadcast.Status = (int)BroadcastStatus.Processing;

                        if (await BroadcastsService.Instance.UpdateBroadcast(broadcast))
                        {
                            var pendingBroadcastMessages = broadcast.Messages.Where(x => x.Status == (int)MessageStatus.Pending).ToList();

                            LoggerService.Instance.LogMessage(string.Format("Broadcast with ID: {0} Status Updated to Processing. Found {1} Pending Messages. Starting Message Loop", broadcast.ID, pendingBroadcastMessages.Count));

                            foreach (var message in pendingBroadcastMessages)
                            {
                                LoggerService.Instance.LogMessage(string.Format("Entered Message Sending Loop with Message ID: {0}.", message.ID));

                                try
                                {
                                    if (message.Status == (int)MessageStatus.Pending) //this check is added because maybe after the list was initially taken, the message has been cancelled etc
                                    {
                                        //Thread.Sleep(2000);

                                        //also keep in mind user setting for Wait Time while sending messages
                                        var messageResults = string.Empty;

                                        if(PreferredBroadcaster == BroadcasterEnums.Twilio)
                                        {
                                            foreach (var number in message.Contact.Numbers)
                                            {
                                                messageResults += TwilioMessenger.SendMessage(broadcast.Text, number.Value);
                                            }
                                        }
                                        else
                                        {
                                            foreach (var number in message.Contact.Numbers)
                                            {
                                                messageResults += GSMMessenger.SendMessage(broadcast.Text, number.Value);
                                            }
                                        }

                                        message.Status = (int)MessageStatus.Sent;
                                        message.SentTime = DateTime.Now;
                                        message.Result = messageResults;
                                        
                                        LoggerService.Instance.LogMessage(string.Format("Message with ID: {0} sent successfully.", message.ID));
                                    }
                                    else
                                    {
                                        LoggerService.Instance.LogMessage(string.Format("Message with ID: {0} is not Status as Pending.", message.ID));
                                    }
                                }
                                catch (Exception ex)
                                {
                                    message.Status = (int)MessageStatus.Failed;
                                    message.SentTime = DateTime.Now;
                                    message.Result = ex.Message;

                                    LoggerService.Instance.LogMessage(string.Format("Error Message Sending with Message ID: {3} {2}{0} {2}{1} {2}", ex.Message, ex.StackTrace, Environment.NewLine, message.ID));                                    
                                }
                            }

                            broadcast.Status = (int)BroadcastStatus.Completed;
                            broadcast.CompletedTime = DateTime.Now;

                            var updateResult = await BroadcastsService.Instance.UpdateBroadcast(broadcast);
                            
                            LoggerService.Instance.LogMessage(string.Format("Broadcast with ID: {0} Status Updated to Completed is {1}.", broadcast.ID, updateResult));
                        }
                        else
                        {
                            LoggerService.Instance.LogMessage(string.Format("Broadcast with ID: {0} not updated. Out of If.", broadcast.ID));
                        }
                    }
                    catch (Exception ex)
                    {
                        //continue
                        LoggerService.Instance.LogMessage(string.Format("ERROR - Each Broadcast {3} Loop {2}{0} {2}{1} {2}", ex.Message, ex.StackTrace, Environment.NewLine, broadcast.ID));
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                //continue for threads to work properly
                LoggerService.Instance.LogMessage(string.Format("ERROR Main StartPendingBroadcasts {2}{0} {2}{1} {2}", ex.Message, ex.StackTrace, Environment.NewLine));
                return false;
            }
        }
    }
}
