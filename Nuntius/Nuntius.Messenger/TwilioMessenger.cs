﻿using Nuntius.Globals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Twilio;
using Twilio.Converters;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace Nuntius.Messenger
{
    public static class TwilioMessenger
    {
        // Find your Account Sid and Token at twilio.com/console
        public static string AccountSid;
        public static string AuthToken;
        public static string FromNumber;

        public static void Setup(string accountSid, string authToken, string fromNumber)
        {
            AccountSid = accountSid;
            AuthToken = authToken;
            FromNumber = fromNumber;
        }

        public static string SendMessage(string message, string contactNumber)
        {
            try
            {
                TwilioClient.Init(AccountSid, AuthToken);

                var tMessage = MessageResource.Create(
                    to: new PhoneNumber(contactNumber),
                    from: new PhoneNumber(FromNumber),
                    body: message
                );

                return string.Format(Messages.GSMMessageSuccess, contactNumber, tMessage.Sid);
            }
            catch (Exception ex)
            {
                return string.Format(Messages.TwilioMessageError, contactNumber, ex.Message);
            }
        }

        public static string SendTestMessage(string message, string contactNumber)
        {
            TwilioClient.Init(AccountSid, AuthToken);

            var tMessage = MessageResource.Create(
                to: new PhoneNumber(contactNumber),
                from: new PhoneNumber(FromNumber),
                body: message
            );

            return string.Format(Messages.GSMMessageSuccess, contactNumber, tMessage.Sid);
        }
    }
}
