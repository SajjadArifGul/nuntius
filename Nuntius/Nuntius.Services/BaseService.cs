﻿using Nuntius.Data;
using Nuntius.Globals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nuntius.Services
{
    public abstract class BaseService : IDisposable
    {
        private NuntiusContext _context { get; set; }
        public NuntiusContext context {
            get
            {
                if (_context == null)
                {
                    _context = new NuntiusContext();

                    LoggerService.Instance.LogMessage(string.Format("New Context {0}", _context.ConvertToString()));
                }

                return _context;
            }
        }

        public void Dispose()
        {
            if (_context != null)
            {
                _context.Dispose();
                _context = null;
            }

            LoggerService.Instance.LogMessage(string.Format("Disposed Context {0}", _context.ConvertToString()));
        }
    }
}
