﻿using Nuntius.Data;
using Nuntius.Entities;
using Nuntius.Globals;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Nuntius.Services
{
    public class BroadcastersService : BaseService
    {
        #region Define as Singleton
        private static BroadcastersService _Instance;

        public static BroadcastersService Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new BroadcastersService();
                }

                return (_Instance);
            }
        }

        private BroadcastersService()
        {
        }
        #endregion
        
        public Task<List<Broadcaster>> GetBroadcasters()
        {
            return Task.Factory.StartNew(() =>
            {
                return context.Broadcasters.ToList();
            });
        }

        public Task<Broadcaster> GetBroadcasterByID(int broadcastID)
        {
            return Task.Run(() =>
            {
                return context.Broadcasters.Find(broadcastID);
            });
        }

        public Task<bool> AddNewBroadcaster(Broadcaster broadcaster)
        {
            return Task.Run(() =>
            {
                context.Broadcasters.Add(broadcaster);

                return context.SaveChanges() > 0;
            });
        }

        public Task<bool> AddNewBroadcasters(List<Broadcaster> broadcasters)
        {
            return Task.Run(() =>
            {
                context.Broadcasters.AddRange(broadcasters);

                return context.SaveChanges() > 0;
            });
        }
    }
}
