﻿using Nuntius.Data;
using Nuntius.Entities;
using Nuntius.Globals;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Nuntius.Services
{
    public class BroadcastsService : BaseService
    {
        #region Define as Singleton
        private static BroadcastsService _Instance;

        public static BroadcastsService Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new BroadcastsService();
                }

                return (_Instance);
            }
        }

        private BroadcastsService()
        {
        }
        #endregion
        
        public Task<List<Broadcast>> SearchBroadcasts(string searchTerm, int? pageNo = 0, int? noOfRecords = 0)
        {
            return Task.Run(() =>
            {
                Dispose();

                IQueryable<Broadcast> broadcasts;

                if (string.IsNullOrEmpty(searchTerm))
                {
                    broadcasts = context.Broadcasts
                                      .OrderByDescending(x => x.ID);
                }
                else
                {
                    broadcasts = context.Broadcasts
                                      .Where(x => x.Text.ToLower().Contains(searchTerm.ToLower()))
                                      .OrderByDescending(x => x.ID);
                }

                if (pageNo.HasValue && pageNo.Value > 0 && noOfRecords.HasValue && noOfRecords.Value > 0)
                {
                    broadcasts = broadcasts.Skip((pageNo.Value - 1) * noOfRecords.Value).Take(noOfRecords.Value);
                }

                return broadcasts.Include(x => x.Messages).ToList();
            });
        }

        public Task<List<Broadcast>> GetPendingBroadcasts()
        {
            return Task.Run(() =>
            {
                return context.Broadcasts.Where(x => x.Status == (int)BroadcastStatus.Pending)
                                             .Include(x => x.Messages)
                                             .Include("Messages.Contact")
                                             .Include("Messages.Contact.Numbers")
                                             .OrderBy(x => x.StartTime)
                                             .ToList();
            });
        }

        public Task<List<Broadcast>> GetPendingBroadcastsForService()
        {
            return Task.Run(() =>
            {
                using (var database = new NuntiusContext())
                {
                    return database.Broadcasts.Where(x => x.Status == (int)BroadcastStatus.Pending)
                                                 .Include(x => x.Messages)
                                                 .Include("Messages.Contact")
                                                 .Include("Messages.Contact.Numbers")
                                                 .OrderBy(x => x.StartTime)
                                                 .ToList();
                }
            });
        }

        public Task<int> GetBroadcastsCount(string searchTerm)
        {
            return Task.Run(() =>
            {
                IQueryable<Broadcast> broadcasts;

                if (string.IsNullOrEmpty(searchTerm))
                {
                    broadcasts = context.Broadcasts;
                }
                else
                {
                    broadcasts = context.Broadcasts.Where(x => x.Text.ToLower().Contains(searchTerm.ToLower()));
                }

                return broadcasts.Count();
            });
        }
        
        public Task<Broadcast> GetBroadcastByID(int broadcastID)
        {
            return Task.Run(() =>
            {
                return context.Broadcasts.Where(x => x.ID == broadcastID)
                                         .Include(x => x.Messages)
                                         .Include("Messages.Contact")
                                         .Include("Messages.Contact.Numbers")
                                         .FirstOrDefault();
            });
        }

        public Task<bool> AddNewBroadcast(Broadcast broadcast)
        {
            return Task.Run(() =>
            {
                context.Broadcasts.Add(broadcast);

                return context.SaveChanges() > 0;
            });
        }

        public Task<bool> AddNewBroadcasts(List<Broadcast> broadcasts)
        {
            return Task.Run(() =>
            {
                context.Broadcasts.AddRange(broadcasts);

                return context.SaveChanges() > 0;
            });
        }

        public Task<bool> UpdateBroadcast(Broadcast broadcast)
        {
            return Task.Run(() =>
            {
                context.Entry(broadcast).State = EntityState.Modified;

                return context.SaveChanges() > 0;
            });
        }

        public Task<bool> DeleteBroadcast(Broadcast broadcast)
        {
            return Task.Run(() =>
            {
                broadcast.Messages.Where(x => x.ID > 0).ToList().ForEach(bm => context.Entry(bm).State = EntityState.Deleted);

                context.Entry(broadcast).State = EntityState.Deleted;
                
                return context.SaveChanges() > 0;
            });
        }
    }
}
