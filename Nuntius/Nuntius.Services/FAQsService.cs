﻿using Nuntius.Data;
using Nuntius.Entities;
using Nuntius.Globals;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Nuntius.Services
{
    public class FAQsService : BaseService
    {
        #region Define as Singleton
        private static FAQsService _Instance;

        public static FAQsService Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new FAQsService();
                }

                return (_Instance);
            }
        }

        private FAQsService()
        {
        }
        #endregion
        
        public Task<List<FAQ>> GetFAQs()
        {
            return Task.Factory.StartNew(() =>
            {
                return context.FAQs.ToList();
            });
        }

        public Task<FAQ> GetFAQByID(int faqID)
        {
            return Task.Run(() =>
            {
                return context.FAQs.Find(faqID);
            });
        }

        public Task<bool> AddNewFAQ(FAQ faq)
        {
            return Task.Run(() =>
            {
                context.FAQs.Add(faq);

                return context.SaveChanges() > 0;
            });
        }

        public Task<bool> AddNewFAQs(List<FAQ> faqs)
        {
            return Task.Run(() =>
            {
                context.FAQs.AddRange(faqs);

                return context.SaveChanges() > 0;
            });
        }
    }
}
