﻿using Nuntius.Data;
using Nuntius.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nuntius.Services
{
    public class GroupsService : BaseService
    {
        #region Define as Singleton
        private static GroupsService _Instance;

        public static GroupsService Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new GroupsService();
                }

                return (_Instance);
            }
        }

        private GroupsService()
        {
        }
        #endregion

        public Task<List<Group>> SearchGroups(string searchTerm, int? pageNo = 0, int? noOfRecords = 0)
        {
            return Task.Run(() =>
            {
                Dispose();

                IQueryable<Group> groups;

                if (string.IsNullOrEmpty(searchTerm))
                {
                    groups = context.Groups
                                    .OrderBy(x => x.Name);
                }
                else
                {
                    groups = context.Groups
                                    .Where(x => x.Name.ToLower().Contains(searchTerm.ToLower()))
                                    .OrderBy(x => x.Name);
                }

                if (pageNo.HasValue && pageNo.Value > 0 && noOfRecords.HasValue && noOfRecords.Value > 0)
                {
                    groups = groups.Skip((pageNo.Value - 1) * noOfRecords.Value).Take(noOfRecords.Value);
                }

                return groups.Include(x => x.GroupContacts).ToList();
            });
        }

        public Task<int> GetGroupsCount(string searchTerm)
        {
            return Task.Run(() =>
            {
                IQueryable<Group> groups;

                if (string.IsNullOrEmpty(searchTerm))
                {
                    groups = context.Groups;
                }
                else
                {
                    groups = context.Groups.Where(x => x.Name.ToLower().Contains(searchTerm.ToLower()));
                }

                return groups.Count();
            });
        }

        public Task<Group> GetGroupByID(int groupID)
        {
            return Task.Run(() =>
            {
                return context.Groups.Where(x => x.ID == groupID)
                                     .Include(x => x.GroupContacts)
                                     .Include("GroupContacts.Contact")
                                     .Include("GroupContacts.Group")
                                     .FirstOrDefault();
            });
        }

        public Task<bool> AddNewGroup(Group group)
        {
            return Task.Run(() =>
            {
                context.Groups.Add(group);

                return context.SaveChanges() > 0;
            });
        }

        public Task<bool> UpdateGroup(Group group)
        {
            return Task.Run(() =>
            {
                group.GroupContacts.Where(x => x.ID > 0).ToList().ForEach(gc => context.Entry(gc).State = EntityState.Deleted);
                group.GroupContacts.Where(x => x.ID == 0).ToList().ForEach(gc => context.Entry(gc).State = EntityState.Added);
                context.Entry(group).State = EntityState.Modified;

                return context.SaveChanges() > 0;                
            });
        }

        public Task<bool> DeleteGroup(Group group)
        {
            return Task.Run(() =>
            {
                group.GroupContacts.Where(x => x.ID > 0).ToList().ForEach(gc => context.Entry(gc).State = EntityState.Deleted);
                context.Entry(group).State = EntityState.Deleted;

                return context.SaveChanges() > 0;
            });
        }
    }
}
