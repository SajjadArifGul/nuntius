﻿using Nuntius.Data;
using Nuntius.Entities;
using Nuntius.Globals;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Nuntius.Services
{
    public class LoggerService : BaseService
    {
        #region Define as Singleton
        private static LoggerService _Instance;

        public static LoggerService Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new LoggerService();
                }

                return (_Instance);
            }
        }

        private LoggerService()
        {
        }
        #endregion

        string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\log.txt";

        public void LogMessage(string message)
        {
            try
            {
                message = string.Format("{0} ~ {1}{2}", message, DateTime.Now, Environment.NewLine);

                if (!File.Exists(filepath))
                {
                    using (StreamWriter sw = File.CreateText(filepath))
                    {
                        sw.WriteLine(message);
                    }
                }
                else
                {
                    using (StreamWriter sw = File.AppendText(filepath))
                    {
                        sw.WriteLine(message);
                    }
                }
            }
            catch 
            {
                //let it go. We can't even write its log.
            }
        }
    }
}
