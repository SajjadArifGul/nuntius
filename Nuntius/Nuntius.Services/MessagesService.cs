﻿using Nuntius.Data;
using Nuntius.Entities;
using Nuntius.Globals;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Nuntius.Services
{
    public class MessagesService : BaseService
    {
        #region Define as Singleton
        private static MessagesService _Instance;

        public static MessagesService Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new MessagesService();
                }

                return (_Instance);
            }
        }

        private MessagesService()
        {
        }
        #endregion

        public Task<List<Message>> GetPendingMessages()
        {
            return Task.Run(() =>
            {
                return context.Message.Where(x=>x.Status == (int)MessageStatus.Pending)
                                    .Include(x => x.Contact)
                                    .Include("Contact.Numbers").ToList();
            });
        }

        public Task<List<Contact>> SearchContacts(string searchTerm, int? pageNo = 0, int? noOfRecords = 0)
        {
            return Task.Run(() =>
            {
                Dispose();

                IQueryable<Contact> contacts;

                if (string.IsNullOrEmpty(searchTerm))
                {
                    contacts = context.Contacts
                                      .OrderBy(x => x.Name);
                }
                else
                {
                    contacts = context.Contacts
                                      .Where(x => x.Name.ToLower().Contains(searchTerm.ToLower()))
                                      .OrderBy(x => x.Name);
                }

                if (pageNo.HasValue && pageNo.Value > 0 && noOfRecords.HasValue && noOfRecords.Value > 0)
                {
                    contacts = contacts.Skip((pageNo.Value - 1) * noOfRecords.Value).Take(noOfRecords.Value);
                }

                return contacts.Include(x => x.Numbers).ToList();
            });
        }
        
        public Task<int> GetContactsCount(string searchTerm)
        {
            return Task.Run(() =>
            {
                IQueryable<Contact> contacts;

                if (string.IsNullOrEmpty(searchTerm))
                {
                    contacts = context.Contacts;
                }
                else
                {
                    contacts = context.Contacts.Where(x => x.Name.ToLower().Contains(searchTerm.ToLower()));
                }

                return contacts.Count();
            });
        }
        
        public Task<Contact> GetContactByID(int contactID)
        {
            return Task.Run(() =>
            {
                return context.Contacts.Where(x => x.ID == contactID).Include(x => x.Numbers).Include(x => x.GroupContacts).FirstOrDefault();
            });
        }

        public Task<bool> AddNewContact(Contact contact)
        {
            return Task.Run(() =>
            {
                context.Contacts.Add(contact);

                return context.SaveChanges() > 0;
            });
        }

        public Task<bool> AddNewContacts(List<Contact> contacts)
        {
            return Task.Run(() =>
            {
                context.Contacts.AddRange(contacts);

                return context.SaveChanges() > 0;
            });
        }

        public Task<bool> UpdateMessage(Message message)
        {
            return Task.Run(() =>
            {
                context.Entry(message).State = EntityState.Modified;

                return context.SaveChanges() > 0;
            });
        }

        public Task<bool> DeleteContact(Contact contact)
        {
            return Task.Run(() =>
            {
                contact.GroupContacts.Where(x => x.ID > 0).ToList().ForEach(gc => context.Entry(gc).State = EntityState.Deleted);

                context.Entry(contact).State = EntityState.Deleted;
                
                return context.SaveChanges() > 0;
            });
        }

        public Task<List<Contact>> TextToContacts(string[] contactLines)
        {
            return Task.Run(() =>
            {
                List<Contact> contacts = null;

                if (contactLines != null && contactLines.Length > 0)
                {
                    contacts = new List<Contact>();

                    foreach (var contactLine in contactLines)
                    {
                        if (!string.IsNullOrEmpty(contactLine))
                        {
                            try
                            {
                                var contact = new Contact();

                                if (contactLine.Contains("#"))
                                {
                                    contact.Name = contactLine.GetSubstringText(string.Empty, "#");
                                    contact.Numbers = contactLine.GetSubstringText("#", string.Empty).TextToNumbers();
                                }
                                else
                                {
                                    contact.Numbers = contactLine.TextToNumbers();
                                }

                                contacts.Add(contact);
                            }
                            catch
                            {
                                //do nothing just ignore this contact. move to the others
                            }
                        }
                    }
                }

                return contacts;
            });            
        }
        
        public Task<string> ContactsToText(List<Contact> contacts)
        {
            return Task.Run(() =>
            {
                if (contacts != null && contacts.Count > 0)
                {
                    var contactsText = string.Empty;

                    foreach (var contact in contacts)
                    {
                        var contactLine = string.Format("{0}#{1}{2}", contact.Name, contact.Numbers.JoinNumbers(","), Environment.NewLine);

                        contactsText = contactsText + contactLine;
                    }

                    return contactsText;
                }

                return string.Empty;
            });
        }
    }
}
