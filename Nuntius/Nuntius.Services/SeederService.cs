﻿using Nuntius.Data;
using Nuntius.Entities;
using Nuntius.Globals;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Nuntius.Services
{
    public class SeederService : BaseService
    {
        #region Define as Singleton
        private static SeederService _Instance;

        public static SeederService Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new SeederService();
                }

                return (_Instance);
            }
        }

        private SeederService()
        {
        }
        #endregion

        public void Seed()
        {
            Task.Run(() =>
            {
                using (var context = new NuntiusContext())
                {
                    AddNuntiusDefaults(context);
                    AddBroadcasters(context);
                    SetSMSDefaultConfigurations(context);
                    SetGSMCommDefaultConfigurations(context);
                    SetTwilioDefaultConfigurations(context);

                    context.SaveChanges();
                }
            });            
        }

        private void AddNuntiusDefaults(NuntiusContext context)
        {
            var NuntiusVersion = context.Configurations.Find((int)ConfigurationEnums.NuntiusVersion);
            if (NuntiusVersion == null)
            {
                NuntiusVersion = new Configuration() { ID = (int)ConfigurationEnums.NuntiusVersion, Value = "2" };

                NuntiusVersion = context.Configurations.Add(NuntiusVersion);
            }

            var AboutDetails = context.Configurations.Find((int)ConfigurationEnums.AboutDetails);
            if (AboutDetails == null)
            {
                AboutDetails = new Configuration() { ID = (int)ConfigurationEnums.AboutDetails,
                                                     Value = @"Hi, I am Sajjad Arif Gul and this is my digital notebook where I share some of my day to day short stories. Sometimes they might be very short stories. I recommend not to blink. I live in Karachi, Pakistan. I was born on December 14, 1993 (if you want to send me birthday gifts! I love em). I did my primary & secondary education from EAB Pakistan Navy School and went to Govt. Degree College Stadium Road Karachi for Intermediate Education. I received my Bachelors Degree in Software Engineering from Bahria University. Though it sounds weird when I tell people I’m a software engineer, but I can’t imagine doing anything else. I sit before a computer in 1999 for the first time when my dad gift me & my brother on good grades in exams. I only played games & watched movies on it. Well computers and its games always amazed me how they works. Who knew I’ll be a software engineer one day dealing with these computers & their softwares."
                };

                AboutDetails = context.Configurations.Add(AboutDetails);
            }

            var SinglePageRecordsCount = context.Configurations.Find((int)ConfigurationEnums.SinglePageRecordsCount);
            if (SinglePageRecordsCount == null)
            {
                SinglePageRecordsCount = new Configuration() { ID = (int)ConfigurationEnums.SinglePageRecordsCount, Value = "10" };

                SinglePageRecordsCount = context.Configurations.Add(SinglePageRecordsCount);
            }
        }

        private void AddBroadcasters(NuntiusContext context)
        {
            var GSMBroadcaster = context.Broadcasters.Find((int)BroadcasterEnums.GSM);
            if (GSMBroadcaster == null)
            {
                GSMBroadcaster = new Entities.Broadcaster() { ID = (int)BroadcasterEnums.GSM, Name = "GSM" };

                GSMBroadcaster = context.Broadcasters.Add(GSMBroadcaster);
            }

            var TwilioBroadcaster = context.Broadcasters.Find((int)BroadcasterEnums.Twilio);
            if (TwilioBroadcaster == null)
            {
                TwilioBroadcaster = new Entities.Broadcaster() { ID = (int)BroadcasterEnums.Twilio, Name = "Twilio" };

                TwilioBroadcaster = context.Broadcasters.Add(TwilioBroadcaster);
            }
        }

        private void SetSMSDefaultConfigurations(NuntiusContext context)
        {
            var SMSTimeConfig = context.Configurations.Find((int)ConfigurationEnums.SMSTimeConfig);
            if (SMSTimeConfig == null)
            {
                SMSTimeConfig = new Configuration() { ID = (int)ConfigurationEnums.SMSTimeConfig, Value = Defaults.SMSTime.ToString() };

                SMSTimeConfig = context.Configurations.Add(SMSTimeConfig);
            }

            var SMSMessageCountConfig = context.Configurations.Find((int)ConfigurationEnums.SMSMessageCountConfig);
            if (SMSMessageCountConfig == null)
            {
                SMSMessageCountConfig = new Configuration() { ID = (int)ConfigurationEnums.SMSMessageCountConfig, Value = Defaults.SMSMessageCount.ToString() };

                SMSMessageCountConfig = context.Configurations.Add(SMSMessageCountConfig);
            }

            var PrefferedBroadcaster = context.Configurations.Find((int)ConfigurationEnums.PrefferedBroadcaster);
            if (PrefferedBroadcaster == null)
            {
                PrefferedBroadcaster = new Configuration() { ID = (int)ConfigurationEnums.PrefferedBroadcaster, Value = Defaults.PrefferredBroadcaster.ToString() };

                PrefferedBroadcaster = context.Configurations.Add(PrefferedBroadcaster);
            }
        }

        private void SetGSMCommDefaultConfigurations(NuntiusContext context)
        {
            var GSMBaudRateConfig = context.Configurations.Find((int)ConfigurationEnums.GSMBaudRateConfig);
            if (GSMBaudRateConfig == null)
            {
                GSMBaudRateConfig = new Configuration() { ID = (int)ConfigurationEnums.GSMBaudRateConfig, Value = Defaults.GSMBaudRate.ToString() };

                GSMBaudRateConfig = context.Configurations.Add(GSMBaudRateConfig);
            }

            var GSMTimeoutConfig = context.Configurations.Find((int)ConfigurationEnums.GSMTimeoutConfig);
            if (GSMTimeoutConfig == null)
            {
                GSMTimeoutConfig = new Configuration() { ID = (int)ConfigurationEnums.GSMTimeoutConfig, Value = Defaults.GSMTimeout.ToString() };

                GSMTimeoutConfig = context.Configurations.Add(GSMTimeoutConfig);
            }
        }

        private void SetTwilioDefaultConfigurations(NuntiusContext context)
        {
            var TwilioAccountSid = context.Configurations.Find((int)ConfigurationEnums.TwilioAccountSid);
            if (TwilioAccountSid == null)
            {
                TwilioAccountSid = new Configuration() { ID = (int)ConfigurationEnums.TwilioAccountSid, Value = Defaults.TwilioAccountSid.ToString() };

                TwilioAccountSid = context.Configurations.Add(TwilioAccountSid);
            }

            var TwilioAuthToken = context.Configurations.Find((int)ConfigurationEnums.TwilioAuthToken);
            if (TwilioAuthToken == null)
            {
                TwilioAuthToken = new Configuration() { ID = (int)ConfigurationEnums.TwilioAuthToken, Value = Defaults.TwilioAuthToken.ToString() };

                TwilioAuthToken = context.Configurations.Add(TwilioAuthToken);
            }

            var TwilioFromNumber = context.Configurations.Find((int)ConfigurationEnums.TwilioFromNumber);
            if (TwilioFromNumber == null)
            {
                TwilioFromNumber = new Configuration() { ID = (int)ConfigurationEnums.TwilioFromNumber, Value = Defaults.TwilioFromNumber.ToString() };

                TwilioFromNumber = context.Configurations.Add(TwilioFromNumber);
            }
        }
    }
}
