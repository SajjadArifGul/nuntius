﻿using Nuntius.Messenger;
using Nuntius.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace Nuntius.WindowsService
{
    partial class NuntiusService : ServiceBase
    {
        //readonly System.Threading.Thread messengerThread = new System.Threading.Thread(Postman.Execute);
        readonly System.Threading.Thread messengerThread = null;

        public NuntiusService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                LoggerService.Instance.LogMessage("Starting MessengerThread");
                messengerThread.Start();
                LoggerService.Instance.LogMessage("Started MessengerThread");
            }
            catch (Exception ex)
            {
                LoggerService.Instance.LogMessage(String.Format("Starting MessengerThread EXCEPTION {2}{0}{2}{1}{2}", ex.Message, ex.StackTrace, Environment.NewLine));
            }
        }

        [Conditional("DEBUG_SERVICE")]
        private static void DebugMode()
        {
            Debugger.Break();
        }

        protected override void OnStop()
        {
            try
            {
                LoggerService.Instance.LogMessage("Aborting MessengerThread");
                messengerThread.Abort();
                LoggerService.Instance.LogMessage("Aborted MessengerThread");
            }
            catch (Exception ex)
            {
                LoggerService.Instance.LogMessage(String.Format("Aborting MessengerThread EXCEPTION {2}{0}{2}{1}{2}", ex.Message, ex.StackTrace, Environment.NewLine));
            }
        }
    }
}
